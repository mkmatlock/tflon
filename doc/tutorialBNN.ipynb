{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "# Tutorial: Neural Network"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Overview\n",
    "\n",
    "In this section, we are building a neural network with minor tweaks to the ridge logistic regressor in the previous section.\n",
    "\n",
    "## Architecture"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We're going to add a simple fully connected (dense) layer between the input and the target neuron, which will allow our predictor to be more able to represent the data. Here's a diagram of our desired model:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "![](NN.jpg)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "In tflon, it's as simple as adding \n",
    "```python\n",
    "tflon.toolkit.Dense(5, activation=tf.tanh) \n",
    "```\n",
    "Giving us: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "```python\n",
    "class NNModel(tflon.model.Model):\n",
    "    def _model(self):\n",
    "        I = self.add_input('desc', shape=[None, 30])\n",
    "        T = self.add_target('targ', shape=[None, 1])\n",
    "\n",
    "        net = tflon.toolkit.WindowInput() |\\\n",
    "              tflon.toolkit.Dense(5, activation=tf.tanh) |\\\n",
    "              tflon.toolkit.Dense(1)\n",
    "        out = net(I)\n",
    "\n",
    "        self.add_output( 'pred', tf.nn.sigmoid(out) )\n",
    "        self.add_loss( 'xent', tflon.toolkit.xent_uniform_sum(T, out) )\n",
    "        self.add_loss( 'l2', tflon.toolkit.l2_penalty(self.weights) )\n",
    "        self.add_metric( 'auc', tflon.toolkit.auc(T, out) )\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Variants\n",
    "\n",
    "### Multiple output regression\n",
    "\n",
    "For many Neural Networks we would like to predict multiple targets real valued targets. This can be done convienently using ```Dense``` and modifying ```add_target```. Since it's a real value regressor, we will used the common least mean squared error instead of crossentropy. \n",
    "\n",
    "```python\n",
    "class MNNModel(tflon.model.Model):\n",
    "    def _model(self):\n",
    "        I = self.add_input('desc', shape=[None, 29])\n",
    "        T = self.add_target('targ', shape=[None, 2])\n",
    "\n",
    "        net = tflon.toolkit.WindowInput() |\\\n",
    "              tflon.toolkit.Dense(5, activation=tf.tanh) |\\\n",
    "              tflon.toolkit.Dense(2)\n",
    "        out = net(I)\n",
    "\n",
    "        self.add_output( 'pred', tf.nn.sigmoid(out) )\n",
    "        self.add_loss('loss', tf.reduce_sum(tf.square(pred - out)))\n",
    "        self.add_loss( 'l2', tflon.toolkit.l2_penalty(self.weights) )\n",
    "        self.add_metric( 'auc', tflon.toolkit.auc(T, out) )\n",
    "```\n",
    "\n",
    "For use with the breast cancer data, the following line in the preprocessing will also need to be modified to set two columns for the target instead of one.\n",
    "\n",
    "```python\n",
    "targ, desc = tflon.data.Table(df).split([2])\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Generalization\n",
    "\n",
    "The goal in building deep learning models is to train the network to best respond to unobserved data. Thus, it's important to construct a model that doesn't overfit or 'memorizes' the training data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "### Validation and Test Set\n",
    "\n",
    "**Holdout:** It's best practice to reserve a holdout set partitioned before any parameter tuning. This ensures that at the end this dataset can be used as an unbiased estimate of the generalizaiton error. We refer to this set as the holdout/test set and the rest the training set. \n",
    "\n",
    "As expected, this can easily be achieved by adding the following line after we define our feed. We holdout 10% for testing.\n",
    "```python\n",
    "feed = tflon.data.TableFeed({'desc':desc, 'targ':targ})\n",
    "IDs = np.random.randint(569 -1, shape=569//10)\n",
    "testing =  feed.holdout(IDs) \n",
    "```\n",
    "Then, we can evaluate the result. `test_result` is an array of the model's predictions while `test_AUC` provides the AUC.\n",
    "\n",
    "```python\n",
    "with tf.Session():\n",
    "    LR.fit( feed, trainer, restarts=2 )\n",
    "    test_result = NN.infer(testing,'pred')\n",
    "    test_AUC = NN.evaluate(testing)['AUC']\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "**Crossvalidation:** Simply put crossvalidation splits the data into fragments using each fragment as validation once. This allows us to use all the data in the **training set** for validation when evaluating the effect of different parameter settings. We will not show it explicitly, but effectively we implement this by applying holdout to different subsets."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true,
    "nbsphinx": "hidden"
   },
   "source": [
    "### Dropout"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true,
    "nbsphinx": "hidden"
   },
   "source": [
    "Dropout is a common technique for abaiting overfitting in essence preventing the network from simply memorizing the training dataset. Effectively, at each pass the network restrict certain neurons from being updated so that the network is forced to learn fewer more meaningful connections as opposed to many noisy connections."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "nbsphinx": "hidden"
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2018-07-17 12:35:33.606892: I Step  20 /  100, Q:  100, Loss: 7.905e+01\n",
      "2018-07-17 12:35:33.730180: I Step  40 /  100, Q:  100, Loss: 7.637e+01\n",
      "2018-07-17 12:35:33.850197: I Step  60 /  100, Q:  100, Loss: 7.589e+01\n",
      "2018-07-17 12:35:33.968150: I Step  80 /  100, Q:  100, Loss: 7.496e+01\n",
      "2018-07-17 12:35:34.088113: I Step  100 /  100, Q:  100, Loss: 7.488e+01\n",
      "2018-07-17 12:35:34.231735: I Step  20 /  100, Q:  100, Loss: 7.902e+01\n",
      "2018-07-17 12:35:34.343186: I Step  40 /  100, Q:  100, Loss: 7.495e+01\n",
      "2018-07-17 12:35:34.470532: I Step  60 /  100, Q:  100, Loss: 7.427e+01\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "AUC: 0.9958908\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2018-07-17 12:35:34.581586: I Step  80 /  100, Q:  100, Loss: 7.405e+01\n",
      "2018-07-17 12:35:34.687690: I Step  100 /  100, Q:  100, Loss: 7.393e+01\n",
      "2018-07-17 12:35:34.707378: I Found a solution with loss 7.39e+01\n"
     ]
    }
   ],
   "source": [
    "\n",
    "import pandas as pd\n",
    "import tensorflow as tf\n",
    "import tflon \n",
    "tflon.system.reset()\n",
    "class LRModel(tflon.model.Model):\n",
    "    def _model(self):\n",
    "        I = self.add_input('desc', shape=[None, 30])\n",
    "        T = self.add_target('targ', shape=[None, 1])\n",
    "\n",
    "        net = tflon.toolkit.WindowInput() | tflon.toolkit.Dense(5, activation=tf.tanh) | tflon.toolkit.Dense(1)\n",
    "        L = net(I)\n",
    "\n",
    "        self.add_output( 'pred', tf.nn.sigmoid(L) )\n",
    "        self.add_loss( 'xent', tflon.toolkit.xent_uniform_sum(T, L) )\n",
    "        self.add_loss( 'L2', tflon.toolkit.l2_penalty(self.weights))\n",
    "        self.add_metric( 'auc', tflon.toolkit.auc(T, L) )\n",
    "        \n",
    "df = pd.read_csv(\"~/wdbc.data\" , header=None)\n",
    "df[1] = df[1].apply(lambda x : 1 if (x == 'M') else 0) \n",
    "df = df.iloc[:,1:]\n",
    "targ, desc = tflon.data.Table(df).split([1])\n",
    "feed = tflon.data.TableFeed({'desc':desc, 'targ':targ})\n",
    "\n",
    "LR = LRModel()\n",
    "\n",
    "trainer = tflon.train.OpenOptTrainer( iterations=100)\n",
    "\n",
    "with tf.Session():\n",
    "    LR.fit( feed, trainer, restarts=2 )\n",
    "    metrics = LR.evaluate(feed)\n",
    "    print \"AUC:\", metrics['auc']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "tflon.system.reset()\n",
    "# Wait how does the system calculate the AUC since it's not hold out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "source": [
    "AUC: 0.9991015"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "nbsphinx": "hidden"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     1       2\n",
      "0    1  17.990\n",
      "1    1  20.570\n",
      "2    1  19.690\n",
      "3    1  11.420\n",
      "4    1  20.290\n",
      "5    1  12.450\n",
      "6    1  18.250\n",
      "7    1  13.710\n",
      "8    1  13.000\n",
      "9    1  12.460\n",
      "10   1  16.020\n",
      "11   1  15.780\n",
      "12   1  19.170\n",
      "13   1  15.850\n",
      "14   1  13.730\n",
      "15   1  14.540\n",
      "16   1  14.680\n",
      "17   1  16.130\n",
      "18   1  19.810\n",
      "19   0  13.540\n",
      "20   0  13.080\n",
      "21   0   9.504\n",
      "22   1  15.340\n",
      "23   1  21.160\n",
      "24   1  16.650\n",
      "25   1  17.140\n",
      "26   1  14.580\n",
      "27   1  18.610\n",
      "28   1  15.300\n",
      "29   1  17.570\n",
      "..  ..     ...\n",
      "539  0   7.691\n",
      "540  0  11.540\n",
      "541  0  14.470\n",
      "542  0  14.740\n",
      "543  0  13.210\n",
      "544  0  13.870\n",
      "545  0  13.620\n",
      "546  0  10.320\n",
      "547  0  10.260\n",
      "548  0   9.683\n",
      "549  0  10.820\n",
      "550  0  10.860\n",
      "551  0  11.130\n",
      "552  0  12.770\n",
      "553  0   9.333\n",
      "554  0  12.880\n",
      "555  0  10.290\n",
      "556  0  10.160\n",
      "557  0   9.423\n",
      "558  0  14.590\n",
      "559  0  11.510\n",
      "560  0  14.050\n",
      "561  0  11.200\n",
      "562  1  15.220\n",
      "563  1  20.920\n",
      "564  1  21.560\n",
      "565  1  20.130\n",
      "566  1  16.600\n",
      "567  1  20.600\n",
      "568  0   7.760\n",
      "\n",
      "[569 rows x 2 columns]\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2018-07-17 12:44:52.790047: I Step  20 /  100, Q:  100, Loss: -1.979e+11\n",
      "2018-07-17 12:44:52.902815: I Step  40 /  100, Q:  100, Loss: -4.750e+19\n",
      "2018-07-17 12:44:53.088265: I Step  20 /  100, Q:  100, Loss: -1.157e+12\n",
      "2018-07-17 12:44:53.202625: I Step  40 /  100, Q:  100, Loss: -7.046e+19\n",
      "2018-07-17 12:44:53.323187: I Step  60 /  100, Q:  100, Loss: -1.192e+23\n",
      "2018-07-17 12:44:53.345933: I Found a solution with loss -1.19e+23\n"
     ]
    },
    {
     "ename": "InvalidArgumentError",
     "evalue": "exceptions.ValueError: continuous format is not supported\n\t [[Node: MNNModel/Tower/PyFunc_2 = PyFunc[Tin=[DT_FLOAT, DT_FLOAT], Tout=[DT_FLOAT], token=\"pyfunc_30\", _device=\"/job:localhost/replica:0/task:0/device:CPU:0\"](MNNModel/Tower/Accumulator/read, MNNModel/Tower/Accumulator_1/read)]]\n\nCaused by op u'MNNModel/Tower/PyFunc_2', defined at:\n  File \"/usr/lib/python2.7/runpy.py\", line 162, in _run_module_as_main\n    \"__main__\", fname, loader, pkg_name)\n  File \"/usr/lib/python2.7/runpy.py\", line 72, in _run_code\n    exec code in run_globals\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/__main__.py\", line 3, in <module>\n    app.launch_new_instance()\n  File \"/usr/local/lib/python2.7/dist-packages/traitlets/config/application.py\", line 658, in launch_instance\n    app.start()\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/kernelapp.py\", line 474, in start\n    ioloop.IOLoop.instance().start()\n  File \"/usr/local/lib/python2.7/dist-packages/zmq/eventloop/ioloop.py\", line 177, in start\n    super(ZMQIOLoop, self).start()\n  File \"/usr/local/lib/python2.7/dist-packages/tornado/ioloop.py\", line 887, in start\n    handler_func(fd_obj, events)\n  File \"/usr/local/lib/python2.7/dist-packages/tornado/stack_context.py\", line 275, in null_wrapper\n    return fn(*args, **kwargs)\n  File \"/usr/local/lib/python2.7/dist-packages/zmq/eventloop/zmqstream.py\", line 440, in _handle_events\n    self._handle_recv()\n  File \"/usr/local/lib/python2.7/dist-packages/zmq/eventloop/zmqstream.py\", line 472, in _handle_recv\n    self._run_callback(callback, msg)\n  File \"/usr/local/lib/python2.7/dist-packages/zmq/eventloop/zmqstream.py\", line 414, in _run_callback\n    callback(*args, **kwargs)\n  File \"/usr/local/lib/python2.7/dist-packages/tornado/stack_context.py\", line 275, in null_wrapper\n    return fn(*args, **kwargs)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/kernelbase.py\", line 276, in dispatcher\n    return self.dispatch_shell(stream, msg)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/kernelbase.py\", line 228, in dispatch_shell\n    handler(stream, idents, msg)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/kernelbase.py\", line 390, in execute_request\n    user_expressions, allow_stdin)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/ipkernel.py\", line 196, in do_execute\n    res = shell.run_cell(code, store_history=store_history, silent=silent)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/zmqshell.py\", line 501, in run_cell\n    return super(ZMQInteractiveShell, self).run_cell(*args, **kwargs)\n  File \"/usr/local/lib/python2.7/dist-packages/IPython/core/interactiveshell.py\", line 2717, in run_cell\n    interactivity=interactivity, compiler=compiler, result=result)\n  File \"/usr/local/lib/python2.7/dist-packages/IPython/core/interactiveshell.py\", line 2821, in run_ast_nodes\n    if self.run_code(code, result):\n  File \"/usr/local/lib/python2.7/dist-packages/IPython/core/interactiveshell.py\", line 2881, in run_code\n    exec(code_obj, self.user_global_ns, self.user_ns)\n  File \"<ipython-input-19-e51730e29fae>\", line 25, in <module>\n    LR = MNNModel()\n  File \"tflon/model/model.py\", line 45, in __init__\n    self._build( params )\n  File \"tflon/model/model.py\", line 105, in _build\n    self._tower.populate( lambda: self._model() )\n  File \"tflon/model/tower.py\", line 231, in populate\n    build_fn()\n  File \"tflon/model/model.py\", line 105, in <lambda>\n    self._tower.populate( lambda: self._model() )\n  File \"<ipython-input-19-e51730e29fae>\", line 16, in _model\n    self.add_metric( 'auc', tflon.toolkit.auc(T, L) )\n  File \"tflon/toolkit/metrics.py\", line 138, in auc\n    return tf.py_func(lambda t, l: _auc_impl(t, l, axis, nans), [T_acc, L_acc], L.dtype), tf.group(T_up, L_up)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/ops/script_ops.py\", line 317, in py_func\n    func=func, inp=inp, Tout=Tout, stateful=stateful, eager=False, name=name)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/ops/script_ops.py\", line 225, in _internal_py_func\n    input=inp, token=token, Tout=Tout, name=name)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/ops/gen_script_ops.py\", line 93, in _py_func\n    \"PyFunc\", input=input, token=token, Tout=Tout, name=name)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/framework/op_def_library.py\", line 787, in _apply_op_helper\n    op_def=op_def)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/framework/ops.py\", line 3271, in create_op\n    op_def=op_def)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/framework/ops.py\", line 1650, in __init__\n    self._traceback = self._graph._extract_stack()  # pylint: disable=protected-access\n\nInvalidArgumentError (see above for traceback): exceptions.ValueError: continuous format is not supported\n\t [[Node: MNNModel/Tower/PyFunc_2 = PyFunc[Tin=[DT_FLOAT, DT_FLOAT], Tout=[DT_FLOAT], token=\"pyfunc_30\", _device=\"/job:localhost/replica:0/task:0/device:CPU:0\"](MNNModel/Tower/Accumulator/read, MNNModel/Tower/Accumulator_1/read)]]\n",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m\u001b[0m",
      "\u001b[0;31mInvalidArgumentError\u001b[0mTraceback (most recent call last)",
      "\u001b[0;32m<ipython-input-19-e51730e29fae>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     29\u001b[0m \u001b[0;32mwith\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mSession\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     30\u001b[0m     \u001b[0mLR\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfit\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mfeed\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mtrainer\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mrestarts\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;36m2\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 31\u001b[0;31m     \u001b[0mmetrics\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mLR\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mevaluate\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mfeed\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     32\u001b[0m     \u001b[0;32mprint\u001b[0m \u001b[0;34m\"AUC:\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mmetrics\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m'auc'\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/home/jwong/projects/tflon/doc/tutorial/tflon/model/model.pyc\u001b[0m in \u001b[0;36mevaluate\u001b[0;34m(self, source)\u001b[0m\n\u001b[1;32m    215\u001b[0m             \u001b[0mS\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mrun\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mupdate_ops\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mfeed_dict\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mfeed\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mbatch\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    216\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 217\u001b[0;31m         \u001b[0mvalues\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mS\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mrun\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mvalue_ops\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    218\u001b[0m         \u001b[0;32mreturn\u001b[0m \u001b[0mdict\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mzip\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mnames\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mvalues\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    219\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.pyc\u001b[0m in \u001b[0;36mrun\u001b[0;34m(self, fetches, feed_dict, options, run_metadata)\u001b[0m\n\u001b[1;32m    903\u001b[0m     \u001b[0;32mtry\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    904\u001b[0m       result = self._run(None, fetches, feed_dict, options_ptr,\n\u001b[0;32m--> 905\u001b[0;31m                          run_metadata_ptr)\n\u001b[0m\u001b[1;32m    906\u001b[0m       \u001b[0;32mif\u001b[0m \u001b[0mrun_metadata\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    907\u001b[0m         \u001b[0mproto_data\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtf_session\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mTF_GetBuffer\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mrun_metadata_ptr\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.pyc\u001b[0m in \u001b[0;36m_run\u001b[0;34m(self, handle, fetches, feed_dict, options, run_metadata)\u001b[0m\n\u001b[1;32m   1135\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0mfinal_fetches\u001b[0m \u001b[0;32mor\u001b[0m \u001b[0mfinal_targets\u001b[0m \u001b[0;32mor\u001b[0m \u001b[0;34m(\u001b[0m\u001b[0mhandle\u001b[0m \u001b[0;32mand\u001b[0m \u001b[0mfeed_dict_tensor\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1136\u001b[0m       results = self._do_run(handle, final_targets, final_fetches,\n\u001b[0;32m-> 1137\u001b[0;31m                              feed_dict_tensor, options, run_metadata)\n\u001b[0m\u001b[1;32m   1138\u001b[0m     \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1139\u001b[0m       \u001b[0mresults\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;34m[\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.pyc\u001b[0m in \u001b[0;36m_do_run\u001b[0;34m(self, handle, target_list, fetch_list, feed_dict, options, run_metadata)\u001b[0m\n\u001b[1;32m   1353\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0mhandle\u001b[0m \u001b[0;32mis\u001b[0m \u001b[0mNone\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1354\u001b[0m       return self._do_call(_run_fn, self._session, feeds, fetches, targets,\n\u001b[0;32m-> 1355\u001b[0;31m                            options, run_metadata)\n\u001b[0m\u001b[1;32m   1356\u001b[0m     \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1357\u001b[0m       \u001b[0;32mreturn\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_do_call\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0m_prun_fn\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_session\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mhandle\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mfeeds\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mfetches\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/usr/local/lib/python2.7/dist-packages/tensorflow/python/client/session.pyc\u001b[0m in \u001b[0;36m_do_call\u001b[0;34m(self, fn, *args)\u001b[0m\n\u001b[1;32m   1372\u001b[0m         \u001b[0;32mexcept\u001b[0m \u001b[0mKeyError\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1373\u001b[0m           \u001b[0;32mpass\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m-> 1374\u001b[0;31m       \u001b[0;32mraise\u001b[0m \u001b[0mtype\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0me\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mnode_def\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mop\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mmessage\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m   1375\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m   1376\u001b[0m   \u001b[0;32mdef\u001b[0m \u001b[0m_extend_graph\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mInvalidArgumentError\u001b[0m: exceptions.ValueError: continuous format is not supported\n\t [[Node: MNNModel/Tower/PyFunc_2 = PyFunc[Tin=[DT_FLOAT, DT_FLOAT], Tout=[DT_FLOAT], token=\"pyfunc_30\", _device=\"/job:localhost/replica:0/task:0/device:CPU:0\"](MNNModel/Tower/Accumulator/read, MNNModel/Tower/Accumulator_1/read)]]\n\nCaused by op u'MNNModel/Tower/PyFunc_2', defined at:\n  File \"/usr/lib/python2.7/runpy.py\", line 162, in _run_module_as_main\n    \"__main__\", fname, loader, pkg_name)\n  File \"/usr/lib/python2.7/runpy.py\", line 72, in _run_code\n    exec code in run_globals\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/__main__.py\", line 3, in <module>\n    app.launch_new_instance()\n  File \"/usr/local/lib/python2.7/dist-packages/traitlets/config/application.py\", line 658, in launch_instance\n    app.start()\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/kernelapp.py\", line 474, in start\n    ioloop.IOLoop.instance().start()\n  File \"/usr/local/lib/python2.7/dist-packages/zmq/eventloop/ioloop.py\", line 177, in start\n    super(ZMQIOLoop, self).start()\n  File \"/usr/local/lib/python2.7/dist-packages/tornado/ioloop.py\", line 887, in start\n    handler_func(fd_obj, events)\n  File \"/usr/local/lib/python2.7/dist-packages/tornado/stack_context.py\", line 275, in null_wrapper\n    return fn(*args, **kwargs)\n  File \"/usr/local/lib/python2.7/dist-packages/zmq/eventloop/zmqstream.py\", line 440, in _handle_events\n    self._handle_recv()\n  File \"/usr/local/lib/python2.7/dist-packages/zmq/eventloop/zmqstream.py\", line 472, in _handle_recv\n    self._run_callback(callback, msg)\n  File \"/usr/local/lib/python2.7/dist-packages/zmq/eventloop/zmqstream.py\", line 414, in _run_callback\n    callback(*args, **kwargs)\n  File \"/usr/local/lib/python2.7/dist-packages/tornado/stack_context.py\", line 275, in null_wrapper\n    return fn(*args, **kwargs)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/kernelbase.py\", line 276, in dispatcher\n    return self.dispatch_shell(stream, msg)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/kernelbase.py\", line 228, in dispatch_shell\n    handler(stream, idents, msg)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/kernelbase.py\", line 390, in execute_request\n    user_expressions, allow_stdin)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/ipkernel.py\", line 196, in do_execute\n    res = shell.run_cell(code, store_history=store_history, silent=silent)\n  File \"/usr/local/lib/python2.7/dist-packages/ipykernel/zmqshell.py\", line 501, in run_cell\n    return super(ZMQInteractiveShell, self).run_cell(*args, **kwargs)\n  File \"/usr/local/lib/python2.7/dist-packages/IPython/core/interactiveshell.py\", line 2717, in run_cell\n    interactivity=interactivity, compiler=compiler, result=result)\n  File \"/usr/local/lib/python2.7/dist-packages/IPython/core/interactiveshell.py\", line 2821, in run_ast_nodes\n    if self.run_code(code, result):\n  File \"/usr/local/lib/python2.7/dist-packages/IPython/core/interactiveshell.py\", line 2881, in run_code\n    exec(code_obj, self.user_global_ns, self.user_ns)\n  File \"<ipython-input-19-e51730e29fae>\", line 25, in <module>\n    LR = MNNModel()\n  File \"tflon/model/model.py\", line 45, in __init__\n    self._build( params )\n  File \"tflon/model/model.py\", line 105, in _build\n    self._tower.populate( lambda: self._model() )\n  File \"tflon/model/tower.py\", line 231, in populate\n    build_fn()\n  File \"tflon/model/model.py\", line 105, in <lambda>\n    self._tower.populate( lambda: self._model() )\n  File \"<ipython-input-19-e51730e29fae>\", line 16, in _model\n    self.add_metric( 'auc', tflon.toolkit.auc(T, L) )\n  File \"tflon/toolkit/metrics.py\", line 138, in auc\n    return tf.py_func(lambda t, l: _auc_impl(t, l, axis, nans), [T_acc, L_acc], L.dtype), tf.group(T_up, L_up)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/ops/script_ops.py\", line 317, in py_func\n    func=func, inp=inp, Tout=Tout, stateful=stateful, eager=False, name=name)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/ops/script_ops.py\", line 225, in _internal_py_func\n    input=inp, token=token, Tout=Tout, name=name)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/ops/gen_script_ops.py\", line 93, in _py_func\n    \"PyFunc\", input=input, token=token, Tout=Tout, name=name)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/framework/op_def_library.py\", line 787, in _apply_op_helper\n    op_def=op_def)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/framework/ops.py\", line 3271, in create_op\n    op_def=op_def)\n  File \"/usr/local/lib/python2.7/dist-packages/tensorflow/python/framework/ops.py\", line 1650, in __init__\n    self._traceback = self._graph._extract_stack()  # pylint: disable=protected-access\n\nInvalidArgumentError (see above for traceback): exceptions.ValueError: continuous format is not supported\n\t [[Node: MNNModel/Tower/PyFunc_2 = PyFunc[Tin=[DT_FLOAT, DT_FLOAT], Tout=[DT_FLOAT], token=\"pyfunc_30\", _device=\"/job:localhost/replica:0/task:0/device:CPU:0\"](MNNModel/Tower/Accumulator/read, MNNModel/Tower/Accumulator_1/read)]]\n"
     ]
    }
   ],
   "source": [
    "import pandas as pd\n",
    "import tensorflow as tf\n",
    "import tflon \n",
    "tflon.system.reset()\n",
    "class MNNModel(tflon.model.Model):\n",
    "    def _model(self):\n",
    "        I = self.add_input('desc', shape=[None, 29])\n",
    "        T = self.add_target('targ', shape=[None, 2])\n",
    "\n",
    "        net = tflon.toolkit.WindowInput() |\\\n",
    "              tflon.toolkit.Dense(5, activation=tf.tanh) |\\\n",
    "              tflon.toolkit.Dense(2)\n",
    "        L = net(I)\n",
    "\n",
    "        self.add_output( 'pred', tf.nn.sigmoid(L) )\n",
    "        self.add_loss( 'xent', tflon.toolkit.xent_uniform_sum(T, L) )\n",
    "        self.add_loss( 'l2', tflon.toolkit.l2_penalty(self.weights) )\n",
    "        self.add_metric( 'auc', tflon.toolkit.auc(T, L) )\n",
    "        \n",
    "df = pd.read_csv(\"~/wdbc.data\" , header=None)\n",
    "df[1] = df[1].apply(lambda x : 1 if (x == 'M') else 0) \n",
    "df = df.iloc[:,1:]\n",
    "targ, desc = tflon.data.Table(df).split([2])\n",
    "print targ.data\n",
    "feed = tflon.data.TableFeed({'desc':desc, 'targ':targ})\n",
    "\n",
    "LR = MNNModel()\n",
    "\n",
    "trainer = tflon.train.OpenOptTrainer( iterations=100)\n",
    "\n",
    "with tf.Session():\n",
    "    LR.fit( feed, trainer, restarts=2 )\n",
    "    metrics = LR.evaluate(feed)\n",
    "    print \"AUC:\", metrics['auc']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "nbspinx": "hidden"
   },
   "outputs": [],
   "source": [
    "\n",
    "import pandas as pd\n",
    "import tensorflow as tf\n",
    "import tflon \n",
    "tflon.system.reset()\n",
    "class LRModel(tflon.model.Model):\n",
    "    def _model(self):\n",
    "        I = self.add_input('desc', shape=[None, 29])\n",
    "        T = self.add_target('targ', shape=[None, 1])\n",
    "\n",
    "        net = tflon.toolkit.WindowInput() |\\\n",
    "                tflon.toolkit.Apply(lambda x: tf.nn.convolution())|\\\n",
    "        tflon.toolkit.Dense(5, activation=tf.tanh) | tflon.toolkit.Dense(1)\n",
    "        L = net(I)\n",
    "\n",
    "        self.add_output( 'pred', L )\n",
    "        self.add_loss('loss', tf.reduce_sum(tf.square(pred - out)))\n",
    "        self.add_loss( 'L2', tflon.toolkit.l2_penalty(self.weights))\n",
    "        self.add_metric( 'auc', tflon.toolkit.auc(T, L) )\n",
    "        \n",
    "df = pd.read_csv(\"~/wdbc.data\" , header=None)\n",
    "df[1] = df[1].apply(lambda x : 1 if (x == 'M') else 0) \n",
    "df = df.iloc[:,2:]\n",
    "targ, desc = tflon.data.Table(df).split([1])\n",
    "feed = tflon.data.TableFeed({'desc':desc, 'targ':targ})\n",
    "\n",
    "LR = LRModel()\n",
    "\n",
    "trainer = tflon.train.OpenOptTrainer( iterations=100)\n",
    "\n",
    "with tf.Session():\n",
    "    LR.fit( feed, trainer, restarts=2 )\n",
    "    metrics = LR.evaluate(feed)\n",
    "    print \"AUC:\", metrics['auc']"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
