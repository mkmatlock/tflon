Images
======

This module currently supports implementing the wave graph architecture for processing image data

Image grid graph API
--------------------
.. automodule:: tflon.image.graph
    :members:
