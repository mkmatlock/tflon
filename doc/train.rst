Trainers
========


Trainers
--------
.. automodule:: tflon.train.trainer
    :members:

Optimizers
----------
.. automodule:: tflon.train.optimizer
    :members:

Curriculum
----------
.. automodule:: tflon.train.sampling
    :members:
