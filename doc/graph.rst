Graphs
======

Graph base class
----------------
.. automodule:: tflon.graph.graph
    :members:

Graph data pre-processors
-------------------------
.. automodule:: tflon.graph.data
    :members:


Graph operations toolkit
------------------------
.. automodule:: tflon.graph.toolkit
    :members:

Graph recurrent networks (Wave)
-------------------------------
.. automodule:: tflon.graph.grnn
    :members:

Weave convolutional networks
----------------------------
.. automodule:: tflon.graph.weave
    :members:
