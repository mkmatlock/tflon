{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "# Tutorial: Distributed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Overview\n",
    "\n",
    "In this section, we are building a distributed neural network. The key part that needs to be handled from the user perspective is how the data will be divided when passed to different computing units. \n",
    "\n",
    "The underlying distributed API is a wrapper over Horovod the open source component of Uber's Michelangelo. https://github.com/uber/horovod Michealangelo was designed to deploy and scale distributed deep learning projects  over multiple GPUs in a network. \n",
    "\n",
    "The remainder of the distributed book-keeping is managed with the MPI package http://mpi4py.scipy.org/docs/. The beauty of MPI is that it handles both multithreaded and multiserver synchronization ( for the purposes here we'll call both distributed even though the term is traditionally applied to just the latter). \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Schema\n",
    "\n",
    "In short, the ```_schema``` method in model describes where the data is stored and how the retrieve it. This means as it is set up right now, we need to create a folder with the data broken up into different files (shards). Then define a corresponding ```_schema``` which overrides the default. \n",
    "\n",
    "### Shard Example\n",
    "\n",
    "Say we originally have the following directory structure:\n",
    "\n",
    "```\n",
    "data\n",
    "-->full_data.tsv\n",
    "```\n",
    "\n",
    "Then, we can divide the full_data.tsv into  by using unix ```head``` and ```tail```:\n",
    "\n",
    "``` bash\n",
    "head -100 data/full_data.tsv > data/shard0/shard0.tsv\n",
    "tail -n +101 > data/shard1/shard1.tsv\n",
    "```\n",
    "\n",
    "This gives us the following directory structure:\n",
    "\n",
    "```\n",
    "data\n",
    "--->full_data.tsv\n",
    "--->shard0\n",
    "---/--->shard0.tsv\n",
    "--->shard1\n",
    "---/--->shard1.tsv\n",
    "```\n",
    "\n",
    "This lets us train simultaneously on shard0 and shard1\n",
    "\n",
    "In our example, we define ```tsv_reader```, which accesses the data with pandas and provide for the descriptors and target the following three arguments: file name, table class, and helper function in our case ```tsv_reader```.\n",
    "\n",
    "```python \n",
    "    def _schema(self):\n",
    "        tsv_reader = lambda fpath: pd.read_csv(fpath, sep='\\t', \n",
    "                                   dtype={'ID':str}).set_index('ID')\n",
    "        return {'desc': ('descriptors.tsv', tflon.data.Table, tsv_reader),\n",
    "                'targ': ('targets.tsv', tflon.data.Table, tsv_reader)}\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Distributed Feeds and Distributed Trainer\n",
    "\n",
    "Thankfully, most of hardwork has already been done. It simply suffices to interface the functionality from tflon.distributed\n",
    "\n",
    "### Setup\n",
    "\n",
    "First, we need to initialize horovod and gpu resources with a 1000ms timeout if training gets stalled: \n",
    "``` python\n",
    "    tflon.data.TensorQueue.DEFAULT_TIMEOUT=1000\n",
    "    config = tflon.distributed.init_distributed_resources()\n",
    "```\n",
    "\n",
    "### Trainer\n",
    "\n",
    "We use the built-in ```DistributedTrainer``` which interfaces just like the ordinary trainer, OpenOptTrainer.\n",
    " According to the creators, [Adam Optimization ](https://arxiv.org/pdf/1412.6980.pdf) takes advantage of \n",
    "\n",
    "    \"AdaGrad (Duchi et al., 2011), which works well with sparse gradients,\n",
    "    and RMSProp (Tieleman & Hinton, 2012), which works well in on-line and non-stationary\n",
    "    settings\"\n",
    "\n",
    "This explicitly is implemented as\n",
    "```python\n",
    "        trainer = tflon.distributed.DistributedTrainer( tf.train.AdamOptimizer(1e-3),\n",
    "                    iterations=1000 )\n",
    "```\n",
    "\n",
    "### Feed\n",
    "\n",
    "For defining the feed, we use ```make_distributed_table_feed```. The first argument is the path to the directory containing the shards(subdirectories with partitions of the data). The second argument denotes the scheme. The master_table argument is simply indicating which key in the table to use as the index. By default, the feed will divide up the shards evenly between available processors.\n",
    "``` python\n",
    "    feed = tflon.distributed.make_distributed_table_feed( \n",
    "            resource_filename('tflon_test.data', 'distributed'), \n",
    "            NN.schema, master_table='desc', partition_strategy='all' )\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Executing the model\n",
    "\n",
    "Since the distributed table is implemented with MPI, you will need to run it with mpirun and in our case two threads:\n",
    "\n",
    "``` \n",
    "mpirun -np 2 python ./distributed.py\n",
    "```\n",
    "\n",
    "The table will not work properly and throw an error if mpi threads have no data, so the number of threads requested with the `np` option must be less than the number of shards\n",
    "\n",
    "### Warning\n",
    "Due to the way MPI is designed, the `is_master` with anything that involved distributed data. For example, anything during the training phase. Only use `is_master` post-training. In our example, we use it for the evaluation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Example\n",
    "Below is an example of putting all these changes together:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "```python\n",
    "import tflon\n",
    "import tensorflow as tf\n",
    "import pandas as pd\n",
    "from pkg_resources import resource_filename\n",
    "\n",
    "class NeuralNet(tflon.model.Model):\n",
    "    def _schema(self):\n",
    "        tsv_reader = lambda fpath: pd.read_csv(fpath, sep='\\t', \n",
    "                                    dtype={'ID':str}).set_index('ID')\n",
    "        return {'desc': ('descriptors.tsv', tflon.data.Table, tsv_reader),\n",
    "                'targ': ('targets.tsv', tflon.data.Table, tsv_reader)}\n",
    "\n",
    "    def _model(self):\n",
    "        I = self.add_input('desc', shape=[None, 210])\n",
    "        T = self.add_target('targ', shape=[None, 1])\n",
    "        net = tflon.toolkit.WindowInput() |\\\n",
    "              tflon.toolkit.Dense(20, activation=tf.tanh) |\\\n",
    "              tflon.toolkit.Dense(5, activation=tf.tanh) |\\\n",
    "              tflon.toolkit.Dense(1)\n",
    "        L = net(I)\n",
    "\n",
    "        self.add_output( \"pred\", tf.nn.sigmoid(L) )\n",
    "        self.add_loss( \"xent\", tflon.toolkit.xent_uniform_sum(T, L) )\n",
    "        self.add_loss( \"l2\", tflon.toolkit.l2_penalty(self.weights) )\n",
    "        self.add_metric( 'auc', tflon.toolkit.auc(T, L) )\n",
    "\n",
    "if __name__=='__main__':\n",
    "    tflon.data.TensorQueue.DEFAULT_TIMEOUT=1000\n",
    "    # Initialize horovod and setup gpu resources\n",
    "    config = tflon.distributed.init_distributed_resources()\n",
    "\n",
    "    graph = tf.Graph()\n",
    "    with graph.as_default():\n",
    "        # Add a model instance\n",
    "        NN = NeuralNet(use_gpu=True)\n",
    "\n",
    "        # Create the distributed trainer\n",
    "        trainer = tflon.distributed.DistributedTrainer( tf.train.AdamOptimizer(1e-3),\n",
    "                    iterations=1000 )\n",
    "\n",
    "    # Create the data feed, use the same feed for all process instances\n",
    "    # tflon.distributed.DistributedTable adds MPI synchronization to the Table API min and max ops\n",
    "    # Usually, different data would be loaded on each process (see tflon.distributed.make_distributed_table_feed)\n",
    "    feed = tflon.distributed.make_distributed_table_feed( \n",
    "            resource_filename('tflon_test.data', 'distributed'), \n",
    "            NN.schema, master_table='desc', partition_strategy='all' )\n",
    "\n",
    "    with tf.Session(graph=graph, config=config):\n",
    "        # Train with minibatch size 100\n",
    "        NN.fit( feed.shuffle(batch_size=100), trainer, restarts=2, source_tables=feed )\n",
    "\n",
    "        # Perform inference on the master process\n",
    "        if tflon.distributed.is_master():\n",
    "            metrics = NN.evaluate( feed )\n",
    "            print \"AUC:\", metrics['auc']\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "nbsphinx": "hidden"
   },
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "'module' object has no attribute 'tanh'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0mTraceback (most recent call last)",
      "\u001b[0;32m<ipython-input-3-77bbe4ee001f>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     23\u001b[0m \u001b[0mfeed\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtflon\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mdata\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mTableFeed\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m{\u001b[0m\u001b[0;34m'desc'\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0mdesc\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m'targ'\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0mtarg\u001b[0m\u001b[0;34m}\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     24\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 25\u001b[0;31m \u001b[0mLR\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mLRModel\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     26\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     27\u001b[0m \u001b[0mtrainer\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtflon\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mtrain\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mOpenOptTrainer\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0miterations\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;36m100\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/home/jwong/projects/tflon/doc/tutorial/tflon/model/model.pyc\u001b[0m in \u001b[0;36m__init__\u001b[0;34m(self, **kwargs)\u001b[0m\n\u001b[1;32m     43\u001b[0m         \u001b[0;32mfor\u001b[0m \u001b[0mk\u001b[0m \u001b[0;32min\u001b[0m \u001b[0mkwargs\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     44\u001b[0m             \u001b[0mparams\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mk\u001b[0m\u001b[0;34m]\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mkwargs\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mk\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 45\u001b[0;31m         \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_build\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mparams\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     46\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     47\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0m_parameters\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/home/jwong/projects/tflon/doc/tutorial/tflon/model/model.pyc\u001b[0m in \u001b[0;36m_build\u001b[0;34m(self, params)\u001b[0m\n\u001b[1;32m    103\u001b[0m         \u001b[0;32mwith\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvariable_scope\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_scope\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    104\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_tower\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mTower\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mparams\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mparams\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0muse_gpu\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_use_gpu\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 105\u001b[0;31m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_tower\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpopulate\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0;32mlambda\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_model\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    106\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_init_op\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvariables_initializer\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvariables\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    107\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/home/jwong/projects/tflon/doc/tutorial/tflon/model/tower.pyc\u001b[0m in \u001b[0;36mpopulate\u001b[0;34m(self, build_fn)\u001b[0m\n\u001b[1;32m    223\u001b[0m         \"\"\"\n\u001b[1;32m    224\u001b[0m         \u001b[0;32mwith\u001b[0m \u001b[0mbuild_tower\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mname_scope\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'Tower'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mdevice\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_op_device\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 225\u001b[0;31m             \u001b[0mbuild_fn\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    226\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_loss_op\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0madd_n\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_losses\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvalues\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m)\u001b[0m \u001b[0;32mif\u001b[0m \u001b[0mlen\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_losses\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m>\u001b[0m\u001b[0;36m0\u001b[0m \u001b[0;32melse\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mno_op\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    227\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_perform_checks\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/home/jwong/projects/tflon/doc/tutorial/tflon/model/model.pyc\u001b[0m in \u001b[0;36m<lambda>\u001b[0;34m()\u001b[0m\n\u001b[1;32m    103\u001b[0m         \u001b[0;32mwith\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvariable_scope\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_scope\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    104\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_tower\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mTower\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mparams\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mparams\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0muse_gpu\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_use_gpu\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 105\u001b[0;31m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_tower\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mpopulate\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0;32mlambda\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_model\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    106\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_init_op\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtf\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvariables_initializer\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvariables\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    107\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-3-77bbe4ee001f>\u001b[0m in \u001b[0;36m_model\u001b[0;34m(self)\u001b[0m\n\u001b[1;32m      9\u001b[0m         \u001b[0mT\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0madd_target\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'targ'\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mshape\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mNone\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     10\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 11\u001b[0;31m         \u001b[0mnet\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtflon\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mtoolkit\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mWindowInput\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m|\u001b[0m \u001b[0mtflon\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mtoolkit\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mDense\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m5\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mactivation\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mtflon\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mtanh\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m|\u001b[0m \u001b[0mtflon\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mtoolkit\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mDense\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     12\u001b[0m         \u001b[0mL\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mnet\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mI\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     13\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mAttributeError\u001b[0m: 'module' object has no attribute 'tanh'"
     ]
    }
   ],
   "source": [
    "\n",
    "import pandas as pd\n",
    "import tensorflow as tf\n",
    "import tflon \n",
    "\n",
    "class LRModel(tflon.model.Model):\n",
    "    def _model(self):\n",
    "        I = self.add_input('desc', shape=[None, 30])\n",
    "        T = self.add_target('targ', shape=[None, 1])\n",
    "\n",
    "        net = tflon.toolkit.WindowInput() | tflon.toolkit.Dense(5, activation=tf.tanh) | tflon.toolkit.Dense(1)\n",
    "        L = net(I)\n",
    "\n",
    "        self.add_output( 'pred', tf.nn.sigmoid(L) )\n",
    "        self.add_loss( 'xent', tflon.toolkit.xent_uniform_sum(T, L) )\n",
    "        self.add_loss( 'L2', tflon.toolkit.l2_penalty(self.weights))\n",
    "        self.add_metric( 'auc', tflon.toolkit.auc(T, L) )\n",
    "        \n",
    "df = pd.read_csv(\"~/wdbc.data\" , header=None)\n",
    "df[1] = df[1].apply(lambda x : 1 if (x == 'M') else 0) \n",
    "df = df.iloc[:,1:]\n",
    "targ, desc = tflon.data.Table(df).split([1])\n",
    "feed = tflon.data.TableFeed({'desc':desc, 'targ':targ})\n",
    "\n",
    "LR = LRModel()\n",
    "\n",
    "trainer = tflon.train.OpenOptTrainer( iterations=100)\n",
    "\n",
    "with tf.Session():\n",
    "    LR.fit( feed, trainer, restarts=2 )\n",
    "    metrics = LR.evaluate(feed)\n",
    "    print \"AUC:\", metrics['auc']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true,
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "tflon.system.reset()\n",
    "# Wait how does the system calculate the AUC since it's not hold out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "source": [
    "AUC: 0.9991015"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
