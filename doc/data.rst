Data pipelines
==============

Table data types
----------------
.. automodule:: tflon.data.tables
    :members:

Parquet helper functions
------------------------
.. automodule:: tflon.data.parquet
    :members:

Data feeds
----------
.. automodule:: tflon.data.feeds
    :members:

Schema definitions
------------------
.. automodule:: tflon.data.schema
    :members:

Output transformations
----------------------
.. automodule:: tflon.data.output
    :members:
