# Tflon: Non-stick tensorflow

Tflon is an up-and-coming deep learning toolkit designed to streamline
tensorflow model and data pipeline construction while enabling complex,
dynamically structured operations on data to foster innovative deep
learning research.

Currently, tflon's most developed use case is executing computations on
graph-structure data, such as chemical data, using the wave architecture.(1,2)

Ultimately, tflon aims to provide three key features.

* A flexible data input model, which uses python multiprocessing to
  enable throughput without necessitating custom tensorflow extensions
  for handling complex data.

* A modular model construction framework, which supports complex,
  dynamically structured operations on data, and can be mixed with pure
  tensorflow, or alternative APIs such as keras or sonnet.

* Seamless, intuitive integration with other tools including: data
  storage (pyarrow, parquet), distributed execution (horovod, MPI),
  profiling (pyflame, tfprof), resource monitoring (psutil, GPUtil), and
  others.

Currently, we are still a long way off from achieving these goals. If
you are interested in contributing, please let us know!

## Contents

* [Disclaimer](#disclaimer)
* [Requirements](#requirements)
* [Installation](#installation)
* [Getting Started](#getting started)
* [Wave architecture](#wave graph deep-learning architecture)
* [References](#references)
* [License](#license)

## Disclaimer

This software is in pre-release development, and is provided on an
as-is basis, with no guarantees about future functionality or support.
This software is likely to undergo substantial changes in the near
future.  Please be advised that projects making use of this software may
require substantial changes to maintain compatibility with future
releases. We expect to arrive at a stable API soon.

In addition, be advised that this software is currently provided on a
restricted non-commercial license (see LICENSE). Future releases of
tflon will likely be provided under a permissive open source license,
depending upon interest.

## Requirements

Tflon is tested for python 2.7 on unix based systems (CentOS, Ubuntu,
MacOS). Some features (such as process monitoring and profiling)
will likely not work on Windows. Tflon core requirements are listed
below, all python package requirements are available in pypi. Additional
packages are required to use some features as noted.

### Required

* tensorflow
* sklearn
* pyarrow
* pandas
* numpy
* scipy
* click
* dill
* ujson
* psutil
* GPUtil

### Optional

* networkx (2.0 or later; for graph and chem modules)
* openopt  (for using scipy.minimize optimizers)
* tess     (for 3-D chemistry models)
* ppca     (for PCA with missing data)
* pyflame  (for the profiler)

## Installation

```
    hg clone https://bitbucket.org/mkmatlock/tflon
    cd tflon
    python setup.py build
    pip install .
```

## Getting Started

Read the tutorials and documentation at:
https://tflon.readthedocs.io/en/latest/

## Wave graph deep-learning architecture

The current primary use of tflon is processing graph structured data
with the wave architecture.(1,2) For complete implementation examples,
we have provided the `tflon.examples.tox21` and `tflon.examples.maze`
module. The module `tflon.examples.tox21` implements several graph
architectures on the tox21 dataset, a set of small-molecules annotated
with activity data from several toxicity related assays. The module
`tflon.exmaples.maze` trains a dynamic wave network to perform the image
maze task from our paper.(2)

## License

This software is currently under a restricted non-commercial license (see
LICENSE).

## References

1. Matlock, M. K., Dang, N. L., and Swamidass, S. J. (2018). 
   Learning a local variable model of aromatic and conjugated systems. 
   _ACS central science_, 4(1), 52-62.
2. Matlock, M. K., Datta, A., Dang, N. L., Jiang, K., and Swamidass, S.
   J. (2018). Deep learning long-range information in undirected graphs
   with wave networks. ArXiV preprint: https://arxiv.org/abs/1810.12153
