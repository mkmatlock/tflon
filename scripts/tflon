#!/usr/bin/python
from __future__ import print_function
import os
import click
import tflon
import tensorflow as tf
import tarfile


@click.group()
def cli():
    pass

@cli.group('model')
def _model():
    pass

@_model.command('create')
@click.argument('imp')
@click.argument('classname')
@click.argument('parameters')
@click.argument('output')
def model_create(imp, classname, parameters, output):
    import_vars = __import__(imp).__dict__
    lvars = locals().copy()
    lvars.update(globals())
    lvars['tf'] = tf
    lvars['tflon'] = tflon
    class_ = eval(classname, import_vars, lvars)
    parameters = eval(parameters, import_vars, lvars)

    model = class_(params=parameters)
    with tf.Session() as S:
        model.initialize()
        model.save(output)

@_model.command('info')
@click.argument('modelfile')
def model_info(modelfile):
    model = tflon.model.Model.load(modelfile)
    print("Weights\t%d" % (len(model)))
    print("Ops\t%d" % (len(tf.get_default_graph().get_operations())))
    for k in model.parameters:
        print("%s\t%s" % (k, str(model.parameters[k])))



@cli.group('log')
def _log():
    pass

@_log.command('progress')
@click.argument('eventfile')
def log_progress(eventfile):
    if os.path.isdir(eventfile):
        eventfile = tflon.summary.get_logfiles(eventfile)
    steps, _ = tflon.summary.search_summary(eventfile)
    print("Current step: %d" % (steps[-1]))



@cli.group('data')
def _data():
    pass

@_data.command('info')
@click.option('--archive', is_flag=True, default=False)
@click.argument('directory')
def data_info(archive, directory):
    shards = tflon.data.list_shards(directory, shard_format='archive' if archive else 'directory')

    records = 0
    for sfn in shards:
        if archive:
            tfile = tarfile.open(sfn, 'r')
            idxfile = tfile.extractfile('index')
            records += len(idxfile.readlines())
            tfile.close()
        else:
            idxfile = open(os.path.join(sfn, 'index'))
            records += len(idxfile.readlines())
            idxfile.close()

    print("%d shards" % (len(shards)))
    print("%d records" % (records))

@_data.command('featurize')
@click.option('-x', '--exclude', multiple=True)
@click.argument('modelfile')
@click.argument('inpdir')
@click.argument('outdir')
def data_featurize(exclude, modelfile, inpdir, outdir):
    pass

@_data.command('index')
@click.option('--ttype', default='tflon.data.Table')
@click.option('--reader', default='tflon.data.read_parquet')
@click.argument('shard')
@click.argument('table')
def data_index(ttype, reader, shard, table):
    ttype = eval(ttype)
    reader = eval(reader)
    schema = tflon.data.Schema({table:tflon.data.SchemaEntry(table, ttype, reader=reader, required=True)})
    data = schema.load(shard)

    for ix in data.index:
        print(ix)

if __name__=='__main__':
    cli()
