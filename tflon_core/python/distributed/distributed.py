from tflon.train import TFTrainer
from six import string_types
from tflon.data import Table, TableFeed, list_shards
from tflon import logging
import numpy as np
import tensorflow as tf
mpi=None
hvd=None
hvd_ops=None
comm=None
activated=False

def has_distributed_capability():
    try:
        import horovod.tensorflow as hvd
        import mpi4py
        from distutils.spawn import find_executable
        return find_executable("mpirun") is not None
    except:
        return False

def init_distributed_resources():
    """
        Initialize mpi and Horovod for distributed training. This should be
        called before any tflon or tensorflow calls.
    """
    global mpi, hvd, hvd_ops, comm, activated
    try:
        import horovod.tensorflow as _hvd
        import horovod.tensorflow.mpi_ops as _hvd_ops
        import mpi4py
        mpi4py.rc.initialize = False
        _hvd.init()
        assert _hvd.mpi_threads_supported(), "MPI was not built with thread support"

        from mpi4py import MPI as _mpi

        assert _hvd.size() == _mpi.COMM_WORLD.Get_size(), "Horovod detected process group size did not match MPI4py %d != %d" % (_hvd.size(), _mpi.COMM_WORLD.Get_size())

        mpi, hvd, hvd_ops = _mpi, _hvd, _hvd_ops
        comm = mpi.COMM_WORLD
        activated=True
    except:
        logging.error("Please install horovod for distributed training.")
        raise

def is_activated():
    global activated
    return activated

def get_rank():
    """
        Returns rank (the index for distributed thread)
    """
    return hvd.rank()

def get_local_rank():
    """
        Returns local rank (the index for distributed thread)
    """
    return hvd.local_rank()

def num_processes():
    return hvd.size()

def num_local_processes():
    return hvd.local_size()

def is_master():
    """
        Check whether the current process is the rank 0 mpi process.
    """
    return hvd.rank()==0

class DistributedTrainer(TFTrainer):
    """
        This trainer adds horovod DistributedOptimizer wrapper to a tensorflow optimizer, and
        handles broadcasting initialized model states.
    """
    def __init__(self, optimizer, iterations, **kwargs):
        optimizer = hvd.DistributedOptimizer( optimizer )
        super(DistributedTrainer, self).__init__(optimizer, iterations, **kwargs)

    def setup(self, model):
        super(DistributedTrainer, self).setup(model)
        self._bcast_op = tf.group(*[tf.assign(var, hvd_ops.broadcast(var, 0))
                                                for var in model.variables])

    def refresh(self):
        super(DistributedTrainer, self).refresh()
        S = tf.get_default_session()
        S.run(self._bcast_op)

class DistributedTable(Table):
    """
        Table wrapper implementing mpi Reduce ops for table data.
    """

    def __init__(self, table):
        self._table = table

    def serialize(self):
        return DistributedTable(self._table.serialize())

    def deserialize(self):
        return DistributedTable(self._table.deserialize())

    def totuple(self):
        return self._table.totuple()

    @property
    def data(self):
        return self._table.data

    def split(self, partitions):
        return [DistributedTable(t) for t in self._table.split(partitions)]

    def copy(self):
        return DistributedTable(self._table.copy())

    def drop(self, index, **kwargs):
        self._table.drop(index, **kwargs)

    def drop_duplicates(self):
        self._table.drop_duplicates()

    def min(self):
        part = self._table.min()
        mpi_type = mpi._typedict[part.dtype.char]
        total = np.zeros( part.shape ,dtype=part.dtype)
        comm.Allreduce([part,mpi_type], [total,mpi_type], op=mpi.MIN)
        return total

    def max(self):
        part = self._table.max()
        mpi_type = mpi._typedict[part.dtype.char]
        total = np.zeros(part.shape ,dtype=part.dtype)
        comm.Allreduce([part,mpi_type], [total,mpi_type], op=mpi.MAX)
        return total

    def count(self):
        part = self._table.count()
        mpi_type = mpi._typedict[part.dtype.char]
        total = np.zeros(part.shape, dtype=part.dtype)
        comm.Allreduce([part,mpi_type], [total,mpi_type], op=mpi.SUM)
        return total

    def sum(self):
        part = self._table.sum()
        mpi_type = mpi._typedict[part.dtype.char]
        total = np.zeros( part.shape ,dtype=part.dtype)
        comm.Allreduce([part,mpi_type], [total,mpi_type], op=mpi.SUM)
        return total

    def sqdiff(self, diff):
        part = self._table.sqdiff(diff)
        mpi_type = mpi._typedict[part.dtype.char]
        total = np.zeros( part.shape ,dtype=part.dtype)
        comm.Allreduce([part,mpi_type], [total,mpi_type], op=mpi.SUM)
        return total

    def moments(self, dof=0):
        N = self.count()
        S = self.sum()
        mu = S/N.astype(S.dtype)
        SS = self.sqdiff(mu)
        return mu, np.sqrt( SS/(N-dof) )

    def applymap(self, fn):
        return DistributedTable(self._table.applymap(fn))

    @property
    def index(self):
        return self._table.index

    @property
    def columns(self):
        return self._table.columns

    @property
    def shape(self):
        return self._table.get_shape()

    def get_shape(self):
        return self._table.get_shape()

    def filter_rows(self, index):
        return DistributedTable(self._table.filter_rows(index))

    def align(self, index, axis=0):
        self._table.align(index, axis=axis)

    def as_matrix(self):
        return self._table.as_matrix()

    def concatenate(self, tables):
        return DistributedTable(self._table.concatenate([table._table for table in tables]))

def make_distributed_table_feed(roots, schema, master_table=None, partition_strategy='mod', shard_format='directory'):
    """
        Load data shards for distributed training.

        Parameters:
            root (str):                 The root directory containing shards. Each shard should be a subdirectory of this directory.
            schema (tflon.data.Schema): Schema mapping files to named tables as returned by tflon.model.Model.schema

        Keyword Arguments:
            partition_strategy (str): Strategy for dividing shards among nodes.
                                      Supported values include:
                                      * 'mod': For process rank r and number n,  divide shards evenly by distributing every r+n-th shard
                                      * 'all': Replicate all shards on all processes
            shard_format (str):       Format for shard storage: either 'directory' or 'archive', which expects extension .tar.gz (default='directory')
    """
    if isinstance(roots, string_types):
        roots = [roots]

    rank = hvd.rank()
    size = hvd.size()
    shard_directories = list_shards(roots, shard_format)

    if partition_strategy=='mod':
        myshards = [sd for i, sd in enumerate(shard_directories) if i % size == rank]
    elif partition_strategy=='all':
        myshards = shard_directories

    assert len(myshards)>0, "No shards assigned to distributed process at rank %d" % (rank)

    if len(myshards)==1:
        logging.info("Loading 1 shard")
        sd, = myshards
        shard_map = schema.load(*sd)
        table_map = {tn:DistributedTable(shard_map[tn]) for tn in shard_map}
    else:
        table_map = {}
        for i, sd in enumerate(myshards):
            logging.info("Loading %d / %d shards", i+1, len(myshards))
            shard_map = schema.load(*sd)
            for tn in shard_map:
                table_map.setdefault(tn, list()).append(shard_map[tn])
        table_map = {tn:DistributedTable(table_map[tn][0].concatenate(table_map[tn][1:])) for tn in table_map.keys()}

    return TableFeed(table_map, master_table=master_table)
