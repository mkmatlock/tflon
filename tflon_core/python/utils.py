from functools import wraps
from threading import Lock
import numpy as np
import sys, os

class CacheEntry: pass

def memoized(f):
    """Decorator that caches a function's result each time it is called.
    If called later with the same arguments, the cached value is
    returned, and not re-evaluated. Access to the cache is
    thread-safe.

    """
    cache = {}                  # Map from key to CacheEntry
    cache_lock = Lock()         # Guards cache

    @wraps(f)
    def memoizer(*args, **kwargs):
        key = args, tuple(kwargs.items())
        result_lock = None

        with cache_lock:
            try:
                entry = cache[key]
            except KeyError:
                entry = cache[key] = CacheEntry()
                result_lock = entry.lock = Lock() # Guards entry.result
                result_lock.acquire()

        if result_lock:
            # Compute the new entry without holding cache_lock, to avoid
            # deadlock if the call to f is recursive (in which case
            # memoizer is re-entered).
            result = entry.result = f(*args, **kwargs)
            result_lock.release()
            return result
        else:
            # Wait on entry.lock without holding cache_lock, to avoid
            # blocking other threads that need to use the cache.
            with entry.lock:
                return entry.result

    return memoizer

def cached_property(f):
    lock_name = "_%s_lock" % (f.__name__)
    prop_name = "_%s_value" % (f.__name__)
    @wraps(f)
    def property_cache(self):
        obj_dict = self.__dict__

        if lock_name not in obj_dict:
            obj_dict.setdefault(lock_name, Lock())
        cache_lock = obj_dict[lock_name]

        with cache_lock:
            if prop_name not in obj_dict:
                obj_dict[prop_name] = f(self)
            return obj_dict[prop_name]
    return property(property_cache)

def synchronized(lock_name):
    def synchronized_decorator(method):
        def synchronized_method(self, *args, **kws):
            objdict = self.__dict__
            if not lock_name in objdict:
                objdict.setdefault(lock_name, Lock())
            lock = objdict[lock_name]
            with lock:
                return method(self, *args, **kws)

        return synchronized_method
    return synchronized_decorator

def nanlen(i):
    if i is None:
        return 0
    try:
        if np.isnan(i):
            return 0
    finally:
        return len(i)

def nanempty(i):
    if i is None:
        return []
    try:
        if np.isnan(i):
            return []
    finally:
        return i

def nansorted(L, key=lambda x:x):
    def nankey(x):
        k = key(x)
        return (np.isnan(k), 0 if np.isnan(k) else k)
    return sorted(L, key=nankey)

def uniq(L):
    seen = set()
    U = []
    for i in L:
        if i not in seen:
            seen.add(i)
            U.append(i)
    return U

class ManagedTTY(object):
    def __init__(self):
        self._stream = sys.stderr if sys.stderr.isatty() else open(os.devnull, 'w')
        self._close = not sys.stderr.isatty()

    @property
    def stream(self):
        return self._stream

    def __enter__(self):
        pass

    def __exit__(self, *args):
        if self._close:
            self._stream.close()


