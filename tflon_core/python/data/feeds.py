from __future__ import division
from six import string_types
from builtins import range
from threading import Thread
from tflon import logging
from tflon.utils import synchronized
import tensorflow as tf
import pandas as pd
import random
import numpy as np
import traceback
import sys, os
import re
import numbers
from scipy.sparse import csr_matrix, csc_matrix
from scipy.sparse.coo import coo_matrix
import dill as pickle
try:
    from Queue import Queue, Empty, Full
except:
    from queue import Queue, Empty, Full
from multiprocessing import Process, Event, Manager
import pyarrow

def convert_sparse_matrix_to_sparse_tensor(X):
    """
        Convert sparse scipy matrices to tf.SparseTensorValue. csr and csc matrices are converted to coo first.
    """
    if type(X) in [csr_matrix, csc_matrix]:
        X = X.tocoo()
    indices = np.array([X.row, X.col]).transpose()
    return tf.SparseTensorValue(indices, X.data, X.shape)

def convert_to_feedable(X):
    """
        Converts input to a type valid for input to the tensorflow.Session.run feed_dict argument

        Parameters:
            X: A feedable type (valid types: tflon.data.Table, numpy.ndarray, scipy.sparse.csr_matrix,
                                             scipy.sparse.coo.coo_matrix, tf.Tensor, tf.SparseTensorValue)

        Returns:
            np.array, tf.Tensor, or tf.SparseTensorValue

        Raises:
            ValueError: If X cannot be converted to a feedable type
    """
    from tflon.data.tables import Table
    if isinstance(X, Table):
        X = X.as_matrix()

    if type(X) in [np.ndarray, tf.Tensor, tf.SparseTensorValue, RaggedTensorValue, np.float64, np.float32, np.int64, np.int32, np.bool_, bool] or isinstance(X, numbers.Number):
        return X
    elif type(X) in [pd.DataFrame]:
        return X.values
    elif type(X) in [coo_matrix, csr_matrix, csc_matrix]:
        return convert_sparse_matrix_to_sparse_tensor(X)
    else:
        raise ValueError("Unsupported feed type %s" % (str(type(X))))

class RaggedTensorValue(object):
    """
        Value wrapper used to pass data to RaggedTensor model inputs.

        Parameters:
            values (np.array):   A 1-D numpy array containing the values of the ragged tensor
            lengths (np.array):  A 1-D array specifying the length of each vector in the ragged tensor
            dense_shape (tuple): The dense shape of the ragged tensor, usually equal to (max(lengths), len(lengths))
    """
    def __init__(self, values, lengths, dense_shape):
        self._values = values
        self._lengths = lengths
        self._dense_shape = dense_shape

    @property
    def values(self):
        return self._values

    @property
    def dense_shape(self):
        return self._dense_shape

    @property
    def lengths(self):
        return self._lengths

    def todense(self):
        Z = np.zeros(self._dense_shape)
        begin = [0] + np.cumsum(self._lengths).tolist()
        size = self._lengths
        V = self._values
        for i, (B,S) in enumerate(zip(begin, size)):
            Z[:S,i] = V[B:B+S]
        return Z

class RaggedTensor(object):
    """
        Implementation of ragged tensor input placeholder. This is an alternative to SparseTensor, which uses a tensor of ragged lengths used for slicing variable length vectors.

        This is not currently designed to be used directly in any ops, but should be used by selecting vectors with RaggedTensor.slice

        Parameters:
            values (tensor-like):        Values of the sparse tensor of dimension V
            dense_shape (tensor-like):   The dense shape of the tensor
            ragged_shapes (tensor-like): A tensor of ragged shapes with number of dimensions N-1
    """
    def __init__(self, values, lengths, dense_shape):
        self.values = values
        self.lengths = lengths
        self.dense_shape = dense_shape
        self._begin = tf.concat([tf.constant([0], dtype=tf.int64), tf.cumsum(lengths)], axis=0)

    def slice(self, index):
        begin = tf.slice(self._begin, [index], [1])
        size = tf.slice(self.lengths, [index], [1])
        return tf.slice(self.values, begin, size)

    def get_shape(self):
        return self.dense_shape

class SparseTensor(tf.SparseTensor):
    """
        This is an extension to tf.SparseTensor which enables shape inference for e.g setting weight shapes in matmul ops.

        Parameters:
            indices (tensor-like):       2-D tensor of int64 indices, locations of non-zero values
            values (tensor-like):        1-D tensor of non-zero values
            dense_shape (tensor-like):   The final dense shape of the tensor at feed time. Differs from infered_shape
            infered_shape (tensor-like): The infered shape of the sparse tensor, returned by SparseTensor.get_shape()
    """

    def __init__(self, indices, values, dense_shape, infered_shape):
        super(SparseTensor, self).__init__(indices=indices, values=values, dense_shape=dense_shape)
        self._infered_shape = infered_shape

    def get_shape(self):
        return self._infered_shape

class Fetchable(object):
    """
        Interface for implementing data sources such as queues

        Fetchable.fetch returns a dictionary of data inputs.
    """
    def fetch(self):
        raise NotImplementedError()

    @property
    def utilization(self):
        return 100.

class FetchableGroup(Fetchable):
    """
        Interface for implementing grouped data sources, calls Fetchable.fetch on each member fetchable to construct a dictionary of outputs
    """
    def __init__(self, fetchables=[]):
        self.fetchables = fetchables

    def fetch(self):
        feed_dict = {}
        for fetchable in self.fetchables:
            feed_dict.update( fetchable.fetch() )
        return feed_dict

class IndexQueue(object):
    """
        IndexQueue provides a thread-safe, optionally eternal, optionally randomized iterator over IDs in a dataset.

        The number of epochs can be tracked with the IndexQueue.epochs attribute
    """
    def __init__(self, IDlist, epoch_limit=sys.maxsize, shuffle=True):
        self.epochs = 0
        self._index = 0
        self._shuffle = shuffle
        self._epoch_limit = epoch_limit
        self._IDlist = list(IDlist)

        self._reset_iterator()

    @synchronized("_lock")
    def reset(self):
        self.epochs = 0
        self._reset_iterator()

    def _reset_iterator(self):
        self._index = 0
        if self._shuffle: np.random.shuffle(self._IDlist)

    def _prevent_collisions(self, batch, end):
        bset = set(batch)
        j=-1
        move = [i for i in range(end) if self._IDlist[i] in bset]
        copy = []
        while len(copy) < len(move):
            if self._IDlist[j] not in bset:
                copy.append(j)
            j-=1
        for i, j in zip(move, copy):
            x,y = self._IDlist[i], self._IDlist[j]
            self._IDlist[i] = y
            self._IDlist[j] = x

    @synchronized("_lock")
    def empty(self):
        if self.epochs >= self._epoch_limit:
            return True
        return False

    @synchronized("_lock")
    def dequeue(self, size):
        if self.epochs >= self._epoch_limit:
            raise Exception("IndexQueue is empty")

        end = self._index + size
        if end < len(self._IDlist):
            batch = self._IDlist[self._index:end]
            self._index = end
        else:
            batch = self._IDlist[self._index:len(self._IDlist)]
            self.epochs += 1
            if self.epochs < self._epoch_limit:
                self._reset_iterator()
                end = size-len(batch)
                if self._shuffle: self._prevent_collisions(batch, end)
                batch += self._IDlist[0:end]
                self._index = end
            else:
                self._index = len(self._IDlist)
        return batch

def list_shards(roots, shard_format='directory'):
    if isinstance(roots, string_types):
        roots = [roots]
    def is_shard(path):
        assert shard_format in ['directory', 'archive']
        return (shard_format == 'directory' and os.path.isdir(path)) or\
               (shard_format == 'archive' and path.endswith('tar.gz'))
    def int_or_str(t):
        try:
            return int(t)
        except:
            return t
    def smart_split(fn):
        tokens = re.split(r'[\._\-\/\\\(\)\+]', fn)
        return tuple(int_or_str(t) for t in tokens)

    shards_by_root = []
    for root in roots:
        shards = {fn:(i,os.path.join(root, fn)) for i, fn in enumerate(sorted(os.listdir(root), key=lambda k: smart_split(k))) if is_shard(os.path.join(root, fn))}
        shards_by_root.append(shards)

    all_discovered = set()
    for shards in shards_by_root:
        all_discovered |= set(shards.keys())

    assert all(set(shards.keys())==all_discovered for shards in shards_by_root)
    sort_order = sorted(shards_by_root[0].keys(), key=lambda fn: shards_by_root[0][fn][0])

    return [tuple(shards[fn][1] for shards in shards_by_root) for fn in sort_order]


def make_table_feed(roots, schema, master_table=None, shard_format='directory'):
    """
        Load shards for training.

        Parameters:
            root (str):                 The root directory containing shards. Each shard should be a subdirectory of this directory.
            schema (tflon.data.Schema): Schema mapping files to named tables as returned by tflon.model.Model.schema

        Keyword Arguments:
            shard_format (str):       Format for shard storage: either 'directory' or 'archive', which expects extension .tar.gz (default='directory')
    """
    if isinstance(roots, string_types):
        roots = [roots]

    shard_directories = list_shards(roots, shard_format)
    if len(shard_directories)==1:
        logging.info("Loading 1 shard")
        sd, = shard_directories
        table_map = schema.load(*sd)
    else:
        table_map = {}
        for i, sd in enumerate(shard_directories):
            logging.info("Loading %d / %d shards", i+1, len(shard_directories))
            shard_map = schema.load(*sd)
            for tn in shard_map:
                table_map.setdefault(tn, list()).append(shard_map[tn])
        table_map = {tn:table_map[tn][0].concatenate(table_map[tn][1:]) for tn in table_map.keys()}

    return TableFeed(table_map, master_table=master_table)


class TableFeed(object):
    """
        The standard high level data preprocessor for tflon. Use this to define input data to Model.fit and Model.infer

        Parameters:
            table_map (dict): Dictionary of str -> tflon.data.Table objects defining the raw data tables

        Keyword Arguments:
            master_table (str): The key for the table defining the master index
    """
    def __init__(self, table_map, master_table=None, aligned=False):
        if master_table is None: master_table = list(table_map.keys())[0]
        self._master_table_name = master_table
        self._master_table = table_map[master_table]
        self._table_map = table_map
        self._aligned = aligned
        if '_index' not in self._table_map:
            from tflon.data.tables import IndexTable
            self._table_map['_index'] = IndexTable(self._master_table.index)

    @property
    def index(self):
        """
            Get the master index of this table feed
        """
        return self._master_table.index

    def align(self):
        """
            Align all the examples by the master table index.

            Skip if already aligned.
        """
        if self._aligned: return
        index = self.index
        for name in self._table_map.keys():
            if name == self._master_table_name: continue
            self._table_map[name].align( index )
        self._aligned=True

    def merge(self, *feeds):
        """
            Merge this table feed with other feeds
        """
        ntables = {}
        for name, table in self._table_map.items():
            ntables[name] = table.concatenate([feed[name] for feed in feeds])

        return TableFeed(ntables, master_table=self._master_table_name, aligned=np.all([feed._aligned for feed in feeds]))

    def batch(self, indices):
        """
            Get a subset of examples as a new TableFeed

            Parameters:
                indices (iterable): The indices of examples to include in the batch
        """
        result = {}
        for name, table in self._table_map.items():
            table = table.filter_rows(indices)
            result[name] = table
        return TableFeed(result, master_table=self._master_table_name, aligned=True)

    def __iter__(self):
        return self._table_map.__iter__()

    def __setitem__(self, key, value):
        self._table_map[key] = value

    def __getitem__(self, key):
        return self._table_map[key]

    def __len__(self):
        return self._table_map.__len__()

    def keys(self):
        return self._table_map.keys()

    def items(self):
        return self._table_map.items()

    def update(self, other):
        for k in other:
            self[k] = other[k]

    def copy(self):
        return TableFeed(self._table_map.copy(), self._master_table_name, self._aligned)

    def _queue(self):
        if not hasattr(self, '_iq'):
            self._iq = IndexQueue(self.index, shuffle=True)
        return self._iq

    def holdout(self, holdout_set):
        """
            Return a specified holdout set of examples as a new TableFeed and drop those examples from this feed

            Parameters:
                holdout_set (iterable): Indices of holdout examples
        """
        idx = self.index
        holdout_set = idx[np.isin(idx, list(holdout_set))]
        holdout_tables = self.batch(holdout_set)
        self.drop(holdout_set)
        return holdout_tables

    def drop(self, drop_set, errors='ignore'):
        """
            Drop a specified set of examples from this feed

            Parameters:
                drop_set (iterable): Indices of examples to drop
        """
        idx = self.index
        tables = self._table_map
        drop_set = idx[np.isin(idx, list(drop_set))]

        logging.info("Dropping %d examples", drop_set.shape[0])
        for tn in tables:
            tables[tn].drop(drop_set, errors=errors)

    def _xval_random(self, batch_size, shuffle):
        idx = self.index
        iq = IndexQueue(idx, epoch_limit=1, shuffle=shuffle)
        while not iq.empty():
            selector = np.isin(idx, iq.dequeue(batch_size))
            holdout = self.batch(idx[selector])
            remainder = self.batch(idx[~selector])
            yield holdout, remainder

    def _xval_groups(self, folds, groups):
        idx = groups.index
        groups = groups.values.reshape((-1,))
        for i in range(folds):
            selector = groups==i
            holdout = self.batch(idx[selector])
            remainder = self.batch(idx[~selector])
            yield holdout, remainder

    def xval(self, folds=None, groups=None, shuffle=True):
        """
            Generate datasets for xval

            Keyword Arguments:
                folds (int):        The number of folds, if None, then perform leave-one-out (default=None)
                groups (DataFrame): A dataframe containing a single column specifying holdout groups for each fold (folds are numbered from 0 to n-1)
                shuffle (boolean):  Whether to shuffle the data or use the index ordering (default=True)

            Returns
                iterable:    A generator yielding tuples of (holdout, remainder)
        """
        if folds is None:
            folds = len(self.index)
        if groups is None:
            batch_size = int(np.ceil(len(self.index)/folds))
            return self._xval_random(batch_size, shuffle)
        else:
            return self._xval_groups(folds, groups)

    def epoch(self):
        """
            Get the current data epoch as reached by TableFeed.shuffle
        """
        if not hasattr(self, '_iq'): return 0
        return self._iq.epochs

    def sample(self):
        """
            Return a function which can be used to sample batches of varying size from this feed
        """
        def sampler(batch_size):
            indices = self._queue().dequeue( batch_size )
            return self.batch( indices )
        return sampler

    def shuffle(self, batch_size):
        """
            Construct an eternal iterator over shuffled mini-batches of examples.

            Parameters:
                batch_size (int): The number of examples per batch
        """
        while 1:
            indices = self._queue().dequeue( batch_size )
            yield self.batch( indices )

    def iterate(self, batch_size):
        """
            Construct an in-order iterator over mini-batches of examples. Limited to one epoch.

            Parameters:
                batch_size (int): The number of examples per batch
        """
        iq = IndexQueue(self.index, epoch_limit=1, shuffle=False)
        while not iq.empty():
            indices = iq.dequeue( batch_size )
            yield self.batch( indices )

class PersistentTensorManager(dict, Fetchable):
    """ Deletes persistent tensors loaded into a session after they have been expired. """

    def __init__(self, feed):
        self._feed = feed

    def fetch(self):
        return self._feed

    def keys(self):
        return self._feed.keys()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        for H in self._feed.values(): H.delete()

class TensorQueue(Fetchable):
    DEFAULT_TIMEOUT = 1000
    """
        Loads data tensors into a session via a queueing system, allowing multiple threads to enqueue data. Dequeue operations result in persistent
        tensors stored on the same device as the executing Tower.

        TensorQueue is less flexible than TensorLoader in that data for all inputs passed to the initializer must be included in each enqueue op.
    """
    def __init__(self, tower, capacity=10, staging=False, timeout=None, device=None, name="TensorQueue"):
        """Construct a TensorQueue and setup persistence operations

            Parameters:
                inputs (Input):  A dictionary containing names and input Tensors for a (subset) of model inputs or targets
                capacity (int):  Queue capacity (number of batches), enqueue operations at capacity will block (default = 10)
                timeout (int):   Time to wait (in milliseconds) for data during dequeue operations,
                                     if no data is enqueued, the system will request a shutdown (default = 30 seconds)
                name (str):      A string name for the queue and associated persistent tensor ops (default = TensorQueue)

        """
        if timeout is None: timeout=TensorQueue.DEFAULT_TIMEOUT
        self.name = name
        self.capacity = capacity
        self.run_opts = tf.RunOptions(timeout_in_ms=timeout)

        inputs = tower.placeholders
        self._tower = tower
        self.input_order = sorted(inputs.keys())
        self.input_placeholders = [inputs[k] for k in self.input_order]
        self.dtypes = [T.dtype for T in self.input_placeholders]
        self.shapes = [tuple(dim.value for dim in T.get_shape()) for T in self.input_placeholders]

        with tf.name_scope(name):
            if staging:
                with tf.device( device ):
                    self.queue = tf.contrib.staging.StagingArea( capacity=capacity, dtypes=self.dtypes )
                    self.qsize = self.queue.size()
                    self.enqueue_op = self.queue.put( self.input_placeholders )
                    self.dequeue_op = self.queue.get()
            else:
                self.queue = tf.FIFOQueue( capacity=self.capacity, dtypes=self.dtypes )
                self.qsize = self.queue.size()
                self.enqueue_op = self.queue.enqueue( self.input_placeholders )
                self.dequeue_op = self.queue.dequeue()

            if isinstance(self.dequeue_op, tf.Tensor):
                self.dequeue_op = [self.dequeue_op]
            for T, S in zip(self.dequeue_op, self.shapes):
                T.set_shape(S)
            self._dequeue_handles = [tf.get_session_handle(T) for T in self.dequeue_op]

    def full(self):
        return self.size() == self.capacity

    def enqueue(self, feed, run_opts=None):
        if run_opts is None: run_opts = self.run_opts
        for k, dtype, shape in zip(self.input_placeholders, self.dtypes, self.shapes):
            if k not in feed:
                shape = tuple(0 if d is None else d for d in shape)
                feed[k] = np.zeros(shape, dtype=dtype.as_numpy_dtype)
        S = tf.get_default_session()
        S.run(self.enqueue_op, feed_dict=feed, options=run_opts)

    def dequeue(self):
        S = tf.get_default_session()
        return S.run(self.dequeue_op, options=self.run_opts)

    def fetch(self):
        S = tf.get_default_session()
        return dict(zip(self.input_placeholders, S.run(self._dequeue_handles, options=self.run_opts)))

    def size(self):
        S = tf.get_default_session()
        return S.run(self.qsize)

    def empty(self):
        S = tf.get_default_session()
        while S.run(self.qsize) > 0:
            S.run(self.dequeue_op, options=self.run_opts)

    @property
    def utilization(self):
        return self.size()*100. / self.capacity

class ThreadCoordinator(object):
    """Construct a coordinator that performs featurization on a thread

        Parameters:
            model (tflon.Model):  Source for featurize preprocessing
            source (iterator):    Iterable which returns tensor dictionaries
            queue (TensorQueue):  A tensorflow queue for loading session feed_dict
    """
    def __init__(self, model, source, queue, processes, timeout=5, limit=10):
        self.tf_queue = queue
        self.kill = Event()
        self.timeout = timeout
        self.row_queue = Queue(maxsize=limit)
        if processes>1:
            self.threads = [Thread(target=self.row_queue_putter, args=(source, self.row_queue, tf.get_default_session(), timeout, self.kill))] +\
                        [Thread(target=self.featurize_batch, args=(i, tf.get_default_session(), model, self.row_queue, queue, timeout, self.kill)) for i in range(processes)]
        else:
            self.threads = [Thread(target=self.featurize_batch, args=(0, tf.get_default_session(), model, source, queue, timeout, self.kill))]

    @staticmethod
    def row_queue_putter(source, row_queue, session, timeout, kill):
        logging.debug("Row queuer: start")
        batch = None
        with session.as_default():
            while not kill.is_set():
                try:
                    if batch is None:
                        batch = next(source)
                        logging.debug("Row queuer: serialize rows")
                        batch = serialize_batch(batch)
                    logging.debug("Row queuer: enqueue rows")
                    row_queue.put(batch, timeout=timeout)
                    batch = None
                except Full:
                    logging.debug("Row queuer: row queue is full")
                except:
                    kill.set()
                    logging.error("Row queuer: thread failed with exception:\n%s", traceback.format_exc(limit=0))
                    raise
            logging.debug("Row queuer: exit")

    @staticmethod
    def featurize_batch(proc_num, session, model, source, tf_queue, timeout, kill):
        with session.as_default():
            logging.debug("Featurizer: start")
            batch = None
            run_opts = tf.RunOptions(timeout_in_ms=int(timeout*1000))
            while not kill.is_set():
                try:
                    if batch is None:
                        logging.debug("Featurizer: dequeue rows")
                        if isinstance(source, Queue):
                            batch = source.get(timeout=timeout)
                            batch = deserialize_batch(batch)
                        else:
                            batch = next(source)
                        logging.debug("Featurizer: featurize")
                        batch = model.feed( batch )
                    if not tf_queue.full():
                        logging.debug("Featurizer: enqueue batch")
                        tf_queue.enqueue(batch, run_opts)
                        batch = None

                except Empty:
                    logging.debug("Featurizer %d: Row queue is empty", proc_num)
                except tf.errors.DeadlineExceededError:
                    logging.debug("Featurizer %d: tf queue is full", proc_num)
                except:
                    kill.set()
                    logging.error("Featurizer {0}: thread failed with exception:\n{1}".format(proc_num, traceback.format_exc(limit=0)))
                    raise
            logging.debug("Featurizer: exit")

    def __enter__(self):
        try:
            logging.info("Starting queue: %s..." % (self.tf_queue.name))
            for t in self.threads:
                t.daemon=True
                t.start()
        except:
            logging.error("ThreadCoordinator caught error during initialization: %s" % (traceback.format_exc(limit=0)))
            self._stop()
            raise
        return self

    def _stop(self):
        try:
            self.kill.set()
            for t in self.threads:
                t.join(timeout=self.timeout*2)
            logging.debug("Emptying queues...")
            self.tf_queue.empty()
        except:
            logging.error("Failed to shutdown threads: {0}\n{1}".format(len(self.threads), traceback.format_exc(limit=0)))
            raise

    def __exit__(self, exception_type, exception_value, traceback):
        self._stop()

class QueueCoordinator(ThreadCoordinator):
    """Construct a coordinator that orchestrates subprocess featurizers

        Parameters:

            model (tflon.Model):  Source for featurize preprocessing
            source (iterator):    Iterable which returns tensor dictionaries
            queue (TensorQueue):  A tensorflow queue for loading session feed_dict
            processes (int):      Number of processes
            timeout (float):     Timeout for queue ops, in seconds (default=5)
            limit (int):         Maximum number of queue elements per subprocess
    """
    def __init__(self, model, source, queue, processes, timeout=5, limit=10):
        self.tf_queue = queue
        self.manager = Manager()
        self.batch_queue = self.manager.Queue(maxsize=limit)
        self.row_queue = self.manager.Queue(maxsize=limit*processes)
        self.kill = Event()
        self.timeout = timeout

        self.processes = [ Process(target=self.featurize_batch, args=(i, list(model.feedables), pickle.dumps(model.featurizer), self.row_queue, self.batch_queue, timeout, self.kill)) for i in range(processes) ]
        bq = Thread(target=self.batch_queue_getter, args=(model, self.tf_queue, self.batch_queue, tf.get_default_session(), timeout, self.kill))
        rq = Thread(target=self.row_queue_putter, args=(source, model, self.row_queue, tf.get_default_session(), timeout, self.kill))
        self.threads = [bq, rq]

    @staticmethod
    def batch_queue_getter(model, tf_queue, batch_queue, session, timeout, kill):
        logging.debug("Batch queuer: start")
        run_opts = tf.RunOptions(timeout_in_ms=int(timeout*1000))
        with session.as_default():
            batch = None
            while not kill.is_set():
                try:
                    if batch is None:
                        logging.debug("Batch queuer: get batch")
                        batch = batch_queue.get(timeout=timeout)
                        batch = deserialize_feed(batch)
                        batch = model._tower.feed(batch)
                    if not tf_queue.full():
                        logging.debug("Batch queuer: enqueue batch")
                        tf_queue.enqueue(batch, run_opts)
                        batch = None
                except Empty:
                    logging.debug("Batch queuer: batch queue is empty")
                except tf.errors.DeadlineExceededError:
                    logging.debug("Batch queuer: Tensorflow queue is full")
                except:
                    kill.set()
                    logging.error("Batch queuer: thread failed with exception:\n%s", traceback.format_exc(limit=0))
                    raise
        logging.debug("Batch queuer: exit")

    @staticmethod
    def row_queue_putter(source, model, row_queue, session, timeout, kill):
        logging.debug("Row queuer: start")
        batch = None
        with session.as_default():
            while not kill.is_set():
                try:
                    if batch is None:
                        logging.debug("Row queuer: fetch")
                        batch = next(source)
                        logging.debug("Row queuer: data preprocess")
                        batch = model._data( batch )
                        logging.debug("Row queuer: serialize")
                        batch = serialize_batch(batch)
                    if not row_queue.full():
                        logging.debug("Row queuer: enqueue rows")
                        row_queue.put(batch, timeout=timeout)
                        batch = None
                except Full:
                    logging.debug("Row queuer: row queue is full")
                except:
                    kill.set()
                    logging.error("Row queuer: thread failed with exception:\n%s", traceback.format_exc(limit=0))
                    raise
        logging.debug("Row queuer: exit")

    @staticmethod
    def featurize_batch(proc_num, feedables, featurizer, row_queue, batch_queue, timeout, kill):
        featurizer = pickle.loads(featurizer)
        logging.debug("Featurizer %d: start", proc_num)
        batch = None
        while not kill.is_set():
            try:
                if batch is None:
                    logging.debug("Featurizer %d: dequeue", proc_num)
                    batch = deserialize_batch(row_queue.get(timeout=timeout))
                    logging.debug("Featurizer %d: featurize", proc_num)
                    batch = featurizer( batch )
                    logging.debug("Featurizer %d: convert", proc_num)
                    batch = {k:convert_to_feedable(batch[k]) for k in batch if k in feedables}
                    logging.debug("Featurizer %d: serialize", proc_num)
                    batch = serialize_feedables(batch)
                    logging.debug("Featurizer %d: batch generated", proc_num)
                batch_queue.put(batch, timeout=timeout)
                batch = None
            except Empty:
                logging.debug("Featurizer %d: Row queue is empty", proc_num)
            except Full:
                logging.debug("Featurizer %d: Batch queue is full", proc_num)
            except:
                kill.set()
                logging.error("Featurizer %d: process failed with exception:\n%s", proc_num, traceback.format_exc(limit=0))
                raise
        logging.debug("Featurizer %d: exit", proc_num)

    def __enter__(self):
        try:
            logging.info("Coordinating queue: %s...", self.tf_queue.name)
            for t in self.threads: t.start()
            for p in self.processes:
                p.daemon=True
                p.start()
                logging.debug("Started process %d", p.pid)
        except:
            logging.error("Coordinator caught error during initialization: %s", traceback.format_exc())
            self._stop()
            raise
        return self

    def _stop(self):
        try:
            logging.info("Stopping %d processes...", len(self.processes))
            self.kill.set()
            for p in self.processes:
                logging.debug("Join process %d", p.pid)
                p.join(timeout=self.timeout*2)
            for t in self.threads:
                t.join(timeout=self.timeout*2)
            logging.debug("Emptying queues...")
            self.tf_queue.empty()
            while not self.batch_queue.empty(): self.batch_queue.get()
            while not self.row_queue.empty(): self.row_queue.get()
        except:
            logging.error("Failed to shutdown processes: {0}\n{1}".format(len(self.processes), traceback.format_exc()))
            for p in self.processes:
                p.terminate()
                p.join()
            raise

    def __exit__(self, exception_type, exception_value, traceback):
        self._stop()

class DataService(object):
    ID_NUM = 0
    MESSAGE_START = 1
    MESSAGE_FAILED = 2
    HMAC_KEY = str(random.random()).encode()

    def __init__(self, **kwargs):
        if '_id' not in kwargs:
            kwargs['_id'] = DataService.ID_NUM
            DataService.ID_NUM += 1
        self._id = kwargs['_id']
        self._featurizer = pickle.loads(kwargs['featurizer'])
        self._feedables = kwargs['feedables']
        self.kwargs = kwargs

    def start(self):
        import Pyro4
        self.manager = Manager()
        self.kill = Event()
        self.inbox = self.manager.Queue(maxsize=5)
        self.p = Process(target=DataService._start_remote, args=(self.__class__, self.inbox, self.kill, self.HMAC_KEY), kwargs=self.kwargs)
        self.p.daemon = True
        self.p.start()

        msg_type, message = self.inbox.get(timeout=5)
        assert msg_type==self.MESSAGE_START, "Got unexpected message %d: %s" % (msg_type, str(message))
        Pyro4.config.HMAC_KEY = self.HMAC_KEY
        Pyro4.config.SERIALIZER = 'marshal'
        Pyro4.config.SERIALIZERS_ACCEPTED = set(['marshal'])
        self.proxy = Pyro4.Proxy(message)
        self.proxy._pyroReconnect()
        self.asyncProxy = self.proxy._pyroAsync()


    def restart(self):
        import Pyro4
        self.inbox.empty()
        self.kill.clear()

        self.p = Process(target=DataService._start_remote, args=(self.__class__, self.inbox, self.kill, self.HMAC_KEY), kwargs=self.kwargs)
        self.p.daemon = True
        self.p.start()

        msg_type, message = self.inbox.get(timeout=5)
        assert msg_type==self.MESSAGE_START, "Got unexpected message %d: %s" % (msg_type, str(message))
        self.proxy = Pyro4.Proxy(message)
        self.proxy._pyroReconnect()
        self.asyncProxy = self.proxy._pyroAsync()

    @property
    def started(self):
        return hasattr(self, 'p') and self.p.is_alive()

    def stop(self, timeout):
        try:
            logging.debug("Featurizer %d: stop", self._id)
            self.kill.set()
            self.proxy._pyroRelease()
            self.p.join(timeout=timeout)
        except:
            logging.error("Failed to shutdown data service, force termination...")
            self.p.terminate()
            self.p.join()

    @staticmethod
    def _start_remote(classname, outbox, kill, hmac_key, **kwargs):
        try:
            import Pyro4
            Pyro4.config.SERVERTYPE = "thread"
            Pyro4.config.HMAC_KEY = hmac_key
            Pyro4.config.SERIALIZER = 'marshal'
            Pyro4.config.SERIALIZERS_ACCEPTED = set(['marshal'])

            obj = classname(**kwargs)
            obj.outbox = outbox
            daemon = Pyro4.Daemon()
            uri = daemon.register(obj)

            outbox.put((DataService.MESSAGE_START, uri))
            logging.debug("Featurizer %d: start", kwargs['_id'])
            daemon.requestLoop(lambda: not kill.set())
        except Exception as e:
            outbox.put((DataService.MESSAGE_FAILED, str(e)))
            raise

    def featurize_batch(self, batch):
        try:
            id_num = self._id
            featurizer = self._featurizer
            feedables = self._feedables

            logging.debug("Featurizer %d: deserialize", id_num)
            batch = deserialize_batch( batch )
            logging.debug("Featurizer %d: featurize", id_num)
            batch = featurizer( batch )
            logging.debug("Featurizer %d: map feed dict", id_num)
            batch = {k:convert_to_feedable(batch[k]) for k in batch if k in feedables}
            logging.debug("Featurizer %d: serialize", id_num)
            batch = serialize_feedables(batch)
            logging.debug("Featurizer %d: batch generated", id_num)

            return batch
        except:
            logging.error("Featurizer %d: exception\n%s", id_num, traceback.format_exc())
            raise

def _no_conversion(arg):
    return arg

def serialize_element(e):
    from tflon.data.tables import Table
    if isinstance(e, Table):
        return e.serialize().totuple()
    return e

def deserialize_element(e):
    if isinstance(e, tuple):
        t, args, kwargs = e
        return t(*args, **kwargs).deserialize()
    return e

def serialize_batch(batch):
    pre_serialized = {k:serialize_element(v) for k,v in batch.items()}
    return pyarrow.serialize(pre_serialized).to_buffer().to_pybytes()

def deserialize_batch(batch):
    batch = pyarrow.deserialize(batch)
    batch = {k:deserialize_element(e) for k, e in batch.items()}
    return batch

def serialize_feedables(batch):
    for k in batch.keys():
        v = batch[k]

        if type(v) is RaggedTensorValue:
            batch[k] = RaggedTensorValue, (v.values, v.lengths, v.dense_shape)
        elif type(v) is tf.SparseTensorValue:
            batch[k] = tf.SparseTensorValue, (v.indices, v.values, v.dense_shape)
        else:
            batch[k] = _no_conversion, (v,)

    return pyarrow.serialize(batch).to_buffer().to_pybytes()

def deserialize_feed(batch):
    batch = pyarrow.deserialize(batch)
    return {k:t(*args) for k,(t,args) in batch.items()}

class RestartProxy(Exception):
    pass

class PyroCoordinator(ThreadCoordinator):
    def __init__(self, model, source, tf_queue, processes, timeout=5, limit=10):
        self.model = model
        self.timeout = timeout
        self.tf_queue = tf_queue
        self.kill = Event()

        self.source = source
        self.row_queue = Queue(maxsize=limit)

        self.services = [DataService(featurizer=pickle.dumps(model.featurizer), feedables=list(model.feedables)) for _ in range(processes)]
        self.threads = [Thread(target=self.row_queue_putter, args=(source, model, self.row_queue, tf.get_default_session(), timeout, self.kill))]
        self.threads += [Thread(target=self.batch_featurizer_interface, args=(i, model, self.row_queue, tf_queue, self.services[i], tf.get_default_session(), timeout, self.kill)) for i in range(processes)]

    @staticmethod
    def row_queue_putter(source, model, row_queue, session, timeout, kill):
        logging.debug("Row queuer: start")
        batch = None
        with session.as_default():
            while not kill.is_set():
                try:
                    if batch is None:
                        logging.debug("Row queuer: fetch")
                        batch = next(source)
                        logging.debug("Row queuer: data preprocess")
                        batch = model._data( batch )
                        logging.debug("Row queuer: serialize")
                        batch = serialize_batch( batch )
                    if not row_queue.full():
                        logging.debug("Row queuer: enqueue rows")
                        row_queue.put(batch, timeout=timeout)
                        batch = None
                except Full:
                    logging.debug("Row queuer: row queue is full")
                except:
                    kill.set()
                    logging.error("Row queuer: thread failed with exception:\n%s", traceback.format_exc(limit=0))
                    raise
        logging.debug("Row queuer: exit")

    def batch_featurizer_interface(self, i, model, row_queue, tf_queue, service, session, timeout, kill):
        batch = None
        logging.debug("Batch queuer: start")
        run_opts = tf.RunOptions(timeout_in_ms=int(timeout*1000))

        with session.as_default():
            while not kill.is_set():
                try:
                    try:
                        if batch is None:
                            batch = row_queue.get(timeout=timeout)
                            logging.debug("Batch queuer %d: featurize", i)
                            future = service.asyncProxy.featurize_batch(batch)

                            available = future.wait(timeout=timeout)
                            if not available:
                                batch = None
                                raise RestartProxy
                            batch = deserialize_feed(future.value)
                            batch = model._tower.feed(batch)
                        if not tf_queue.full():
                            logging.debug("Batch queuer %d: enqueue batch", i)
                            tf_queue.enqueue(batch, run_opts)
                            batch = None
                    except RestartProxy:
                        logging.warn("Batch queuer %d: restart service", i)
                        service.stop(timeout)
                        service.restart()
                    except Empty:
                        logging.debug("Batch queuer %d: row queue is empty", i)
                    except tf.errors.DeadlineExceededError:
                        logging.debug("Batch queuer %d: tensorflow queue is full", i)
                except:
                    kill.set()
                    logging.error("Batch queuer %d: thread failed with exception:\n%s", i, traceback.format_exc(limit=0))
                    raise
        logging.debug("Batch queuer %d: exit", i)

    def __enter__(self):
        try:
            logging.info("Coordinating queue: %s...", self.tf_queue.name)
            for s in self.services:
                s.start()
            for t in self.threads: t.start()
        except:
            logging.error("Coordinator caught error during initialization: %s", traceback.format_exc())
            self._stop()
            raise
        return self

    def _stop(self):
        try:
            logging.info("Stopping %d threads, %d services...", len(self.threads), len(self.services))
            self.kill.set()
            for t in self.threads:
                if t.is_alive(): t.join(timeout=self.timeout*2)
            for s in self.services:
                if s.started: s.stop(timeout=self.timeout*2)
            logging.debug("Emptying queues...")
            self.tf_queue.empty()
        except:
            logging.error("Failed to shutdown processes: {0}\n{1}".format(len(self.threads), traceback.format_exc()))
            raise

    def __exit__(self, exception_type, exception_value, traceback):
        self._stop()
