import sys
import numpy as np
import networkx as nx
import ujson as json
from tflon.utils import cached_property, uniq
from sklearn.neighbors import BallTree
from tflon.graph.center import select_min, eccentricity_scorer

def edge(i, j):
    if i is None: return (i,j)
    if j is None: return (j,i)
    if i>j: return (j,i)
    return (i,j)

def block_preorder(ancestors, types):
    blocks = []
    nodes = set(ancestors.keys())
    visited = set()
    while visited != nodes:
        empty = sorted([v for v in ancestors if v not in visited and {u for u in ancestors[v] if types[edge(u,v)]=='t'} <= visited])
        assert len(empty)>0, "Could not block preorder ancestor list: %s" % (str(ancestors))
        blocks.append(empty)
        visited.update(empty)
    return blocks

def flip_ancestors(D):
    nD = {k:set() for k in D}
    for k in D:
        for v in D[k]:
            nD[v].add(k)
    return {k:sorted(nD[k]) for k in nD}

class IndexMap(object):
    def __init__(self, identifiers, index_dict=None):
        self._ids = identifiers
        self._index = index_dict
        self._properties = []
        if index_dict is None:
            self._index = {idx:i for i, idx in enumerate(identifiers)}

    def __iter__(self):
        return iter(self._ids)

    def __len__(self):
        return len(self._ids)

    def __getitem__(self, id):
        return self._index.get(id, None)

    def __eq__(self, other):
        if type(other) is IndexMap:
            other = other.ids
        return self.ids == other

    def __repr__(self):
        return self._ids.__repr__()

    def get(self, id, default=None):
        return self._index.get(id, default)

    def drop(self, indices):
#        filter_ids = set(ix for i,ix in enumerate(self._ids) if i in indices)
        self._ids = [ix for i,ix in enumerate(self._ids) if i not in indices]
        self._index = {ix:i for i,ix in enumerate(self._ids)}
        for prop in self._properties:
            values = self.__dict__[prop]
            self.__dict__[prop] = [v for i,v in enumerate(values) if i not in indices]

    def add_property(self, prop, values):
        assert len(values) == len(self._ids)
        self.__dict__[prop] = values
        self._properties.append(prop)

    @property
    def ids(self):
        return self._ids

    @property
    def index(self):
        return self._index

    def get_value(self, node, prop):
        i = self._index[node]
        return self.__dict__[prop][i]

class Graph(object):
    def __init__(self, graph, node_properties=[], edge_properties=[]):
        self._graph = graph
        self._components = [sorted(c) for c in nx.connected_components(graph)]

        self._nodes = IndexMap(sorted([idx for idx in graph.nodes]))
        edges = sorted([(i,j) for i,j in graph.edges])
        edge_idx = {(u,v):i for i, (u,v) in enumerate(edges)}
        edge_idx.update({(v,u):i for i, (u,v) in enumerate(edges)})
        self._edges = IndexMap(edges, edge_idx)

        self._node_properties = node_properties
        for prop in node_properties:
            self._nodes.add_property(prop, [graph.nodes[id][prop] for id in self._nodes])

        self._edge_properties = edge_properties
        for prop in edge_properties:
            self._edges.add_property(prop, [graph.edges[id][prop] for id in self._edges])

    @property
    def graph(self):
        return self._graph

    @property
    def nodes(self):
        return self._nodes

    @property
    def edges(self):
        return self._edges

    @property
    def components(self):
        return self._components

    @property
    def num_ports(self):
        return 2

    def neighbors(self, u):
        nindex=self._nodes.index
        return sorted(self._graph.neighbors(u), key=lambda v: nindex[v])

    strict_neighbors=neighbors

    def node_edges(self, u):
        return [edge(u,v) for v in self.neighbors(u)]

    @cached_property
    def tree(self):
        assert getattr(self.nodes, "coordinates", None) != None, "Coordinates required to create ball tree"
        coords = np.array(self.nodes.coordinates)
        return BallTree(coords, metric='euclidean')

    def center_of_system(self):
        coords = np.array(self.nodes.coordinates)
        return self.nodes.ids[np.argmin(np.sum(np.square(coords - np.mean(coords,axis=0)), axis=1))]

    def nearest_node(self, nodes, exclude_args=False):
        assert getattr(self.nodes, "coordinates", None) != None, "Coordinates required for neighbor query"

        if exclude_args:
            selection_nodes = [n for n in self.nodes.ids if n not in nodes]
            coords = np.array([self.nodes.coordinates[self.nodes[n]] for n in selection_nodes])
            tree = BallTree(coords, 1)
            dists, neighbors = tree.query([self.nodes.coordinates[self.nodes[n]] for n in nodes], 1)
        else:
            selection_nodes = self.nodes.ids
            dists, neighbors = self.tree.query([self.nodes.coordinates[self.nodes[n]] for n in nodes], 2)

        neighbors = neighbors[dists>0]
        dists = dists[dists>0]
        min_idx = np.argmin(dists)
        connecting_idx = min_idx // (len(neighbors) // len(nodes))
        return dists[min_idx], selection_nodes[neighbors[min_idx]], nodes[connecting_idx]

    def query_nodes(self, nodes, radius, return_distance=False):
        assert getattr(self.nodes, "coordinates", None) != None, "Coordinates required for neighbor query"
        coordinates = [self.nodes.coordinates[self.nodes[n]] for n in nodes]
        balls = self.tree.query_radius(coordinates, radius, return_distance=return_distance)
        if isinstance(balls, tuple):
            balls, dist = balls
            dist = [[d for d in ball if d > 0] for  ball in dist]
            return dist, { nodes[idx]: [ near_idx+1 for near_idx in ball if nodes[idx] != near_idx+1 ] for idx,ball in enumerate(balls) }
        return { nodes[idx]: [ near_idx+1 for near_idx in ball if nodes[idx] != near_idx+1 ] for idx,ball in enumerate(balls) }


    def neighborhood(self, node, max_depth=sys.maxsize):
        """
            Find all nodes within a given depth from a given node

            Parameters:
                node: Starting node index

            Keyword Arguments:
                max_depth (int): The maximum depth in edges (default=sys.maxsize)

            Returns:
                dict: A dictionary of node_index:depth pairs
        """
        G = self._graph
        D = {node: 0}
        queue = [node]
        while len(queue)>0:
            node = queue.pop(0)

            for nnode in G.neighbors(node):
                if nnode in D: continue
                D[nnode] = D[node]+1
                if D[nnode] < max_depth:
                    queue.append(nnode)
        return D

    def _bfs_edges(self, r):
        G = self._graph

        visited = {r}
        queue = [r]
        while len(queue)>0:
            tree_edges  = { (u,v) for u in queue for v in sorted(G.neighbors(u)) if v not in visited }
            new_nodes   = { v for u,v in tree_edges }
            cross_edges = { (u,v) for v in new_nodes for u in G.neighbors(v) if u in new_nodes }

            for u,v in tree_edges:
                yield u,v,'t'
            for u,v in cross_edges:
                yield u,v,'x'
            queue = list(new_nodes)
            visited |= new_nodes

    def _emedian_edges(self, r, epsilon=.05):
        visited = {r}
        queue = [r]
        ref,_,_ = self.nearest_node([r])
        dists,_ = self.query_nodes([r], ref*(1+epsilon), return_distance=True)
        ref = np.amax(dists)
        distances = []

        while(1):
            while len(queue)>0:
                new_dists, neighbors = self.query_nodes(list(queue), ref*(1+epsilon), return_distance=True)
                new_dists = [d for nest1 in new_dists for d in nest1]

                new_edges = { (u,v) for u in queue for v in sorted(neighbors[u]) if v not in visited }
                new_nodes = { v for u,v in new_edges }

                for u,v in new_edges:
                    yield u,v

                distances = np.concatenate((distances, np.array(new_dists).flatten()))
                ref = np.median(distances)

                queue = list(new_nodes)
                visited |= new_nodes

            if(len(visited) < len(self.nodes.ids)):
                ref, u, v = self.nearest_node(list(visited), exclude_args=True)
                distances = [u]
                queue = [u]
                visited.add(u)
                yield v,u
            else: break

    def bfs(self, roots):
        """
            Return a block-partitioned BFS of all subgraphs

            Parameters:
                roots (list): A list of starting nodes, one for each connected component
        """
        block_tracer = self.__class__._bfs_edges
        ancestors = {i:set() for i in self._nodes}
        types = {}

        for r in roots:
            for i,j,label in block_tracer(self,r):
                if i==j: continue
                e = edge(i,j)
                if e in types and types[e] == 't': continue
                elif e not in types:
                    types[e] = label
                else:
                    assert types[e] == label
                ancestors[j].add(i)

        return block_preorder(ancestors, types), {k:sorted(ancestors[k]) for k in ancestors}, types

    def dfs(self, roots):
        """
            Return a block-partitioned DFS of all subgraphs

            Parameters:
                roots (list): A list of starting nodes, one for each connected component
        """
        G = self._graph

        ancestors = {i:set() for i in self._nodes}
        types = {}

        for r in roots:
            for i,j,label in nx.dfs_labeled_edges(G, r):
                if i==j: continue
                e = edge(i,j)
                if e in types and types[e] == 't': continue
                if label=='forward':
                    ancestors[j].add(i)
                    types[e] = 't'
                else:
                    types[e] = 'b'
                    ancestors[i].add(j)

        return block_preorder(ancestors, types), {k:sorted(ancestors[k]) for k in ancestors}, types

    @property
    def radius(self):
        e = self.eccentricity
        return  min(e.values())

    @cached_property
    def eccentricity(self):
        return {u:max(len(paths[v]) for v in paths) for u,paths in nx.all_pairs_shortest_path(self._graph)}

    @cached_property
    def cfc_centraility(self):
        centralities = []
        for c in self._components:
            g = self._graph.subgraph(c)
            centralities.append(nx.current_flow_closeness_centrality(g))
        return centralities

    def centroids(self, scorer=eccentricity_scorer, selector=select_min):
        """
            Args should be lambdas with args for individual scorer
        """
        centers = []
        for c in self._components:
            g = self._graph.subgraph(c)
            nodes, scores = zip(*scorer(g).items())
            weights = selector(scores)
            centers.append(self.sample(c=nodes, p=list(weights)))
        return centers

    def _eccentricity_centers(self):
        """
            Choose a random node from the center of each connected component (nodes with eccentricity equal to radius)
        """
        C = self._components
        ecc = self.eccentricity

        centers = []
        for c in C:
            radius = min(ecc[n] for n in c)
            center = [n for n in c if ecc[n]==radius]
            assert len(center) > 0
            #centers.append( random.sample(center,1)[0] )
            centers.append(center)
        return centers

    def _cfc_centers(self, epsilon=.01):
        """
            Current-flow-closeness centrality.

            Parameters:
                epsilon (float): Error threshold for determining centers.

            Returns:
                centers (list): Node ids of highest cfc-centrality within epsilon limit.
        """
        cfcc = self.cfc_centraility
        centers = []

        for subg in cfcc:
            scores = np.array(subg.values())
            thresh = np.amax(scores)*(1-epsilon)
            center = np.where(scores >= thresh)[0] + 1
            assert len(center) > 0
            centers.append(center)
        return list(centers)

    def find_nodes(self, property, value):
        return [n for n, v in zip(self.nodes, self.nodes.__dict__[property]) if v == value]

    def sample(self, p=None, c=None):
        """
            Samples from nodes according to given probabilities (default: uniform)

            Keyword Arguments:
                p (array-like): Distribution of node probabilities, in order of node index. Should sum to 1. (default: uniform)
                c (list):       A list of nodes to sample from (default: all nodes)
        """
        if c is None: c = self._nodes.ids
        return c[np.random.choice(np.arange(len(c)), p=p)]

    def __getstate__(self):
        D = self.__dict__.copy()
        rmkeys = [k for k in D.keys() if '_lock' in k]
        for k in rmkeys:
            D.pop(k)
        return D

    def multiplicity(self, u, v):
        if not self._graph.has_edge(u,v):
            raise KeyError("No such edge %s,%s" % (str(u),str(v)))
        return 1

    def traversal_direction(self, u, v, e):
        return 0

    def edge_direction(self, u, v):
        if not self._graph.has_edge(u,v):
            raise KeyError("No such edge %s,%s" % (str(u),str(v)))
        return 0

    def get_edges(self, u, v):
        if self._graph.has_edge(u,v):
            yield edge(u,v)

    def has_edge(self, u, v):
        return self._graph.has_edge(u,v)

    @cached_property
    def bidirectional_edges(self):
        has_edge = self.has_edge
        elist = [(u,v) for u,v in self.edges]
        elist += [(v,u) for u,v in elist if has_edge(v,u)]
        elist = uniq(sorted(elist))
        return elist

    def port_number(self, u, e):
        """ Assigns Digraph graph edge to one of four ports """
        assert self.edges.types is not None, 'Edge types not set. Cannot assign to port without dagger type.'
        v,w = e
        if v is None: return 0
        return 1 if bool(self.edges.types[self.edges.get(e,-1)]=='x') else 0

    def set_edge_attributes(self, key, values):
        self._edges.__dict__[key] = [values[edge(*i)] for i in self._edges]

    @property
    def json(self):
        G = self._graph
        N = G.nodes
        E = G.edges
        nodes = []
        edges = []
        for idx in N:
            n = G.nodes[idx]
            props = tuple(n[k] for k in self._node_properties)
            nodes.append((idx, props))

        for i,j in E:
            e = G.edges[i,j]
            props = tuple(e[k] for k in self._edge_properties)
            edges.append((i,j,props))

        return json.dumps({'nodes': nodes, 'edges': edges,
                           'nprops': self._node_properties,
                           'eprops': self._edge_properties})

    @classmethod
    def from_dict(cls, json, nxg=None):
        """
            Generate a networkx representation from a deserialized json dictionary

            Arguments:
                json (dict): The dictionary representation (see json conversions below)
        """
        if nxg is None:
            nxg = nx.Graph()
        np, ep = json['nprops'], json['eprops']

        for i, props in json['nodes']:
            pdict = {k:v for k,v in zip(np, props)}
            nxg.add_node(i, **pdict)

        for i, j, props in json['edges']:
            pdict = {k:v for k,v in zip(ep, props)}
            nxg.add_edge(i, j, **pdict)

        return nxg, np, ep

    @classmethod
    def from_json(cls, serialized):
        """
            Generate a networkx representation of a serialized json format graph

            Arguments:
                serialized (str): The serialized json representation
        """
        gdict = json.loads(serialized)
        nxg, np, ep = Graph.from_dict(gdict, nx.Graph())
        return Graph(nxg, np, ep)

class DiGraph(Graph):
    """
        Wrapper for directed graph support. Casts digraph to undirected graph and provides edge_direction query.

        Parameters:
            digraph (networkx.DiGraph): A directed graph
    """
    def __init__(self, digraph, node_properties=[], edge_properties=[]):
        graph = nx.Graph(digraph)

        self._digraph = digraph
        self._graph = graph
        self._components = [sorted(c) for c in nx.connected_components(graph)]
        self._nodes = IndexMap(sorted([idx for idx in graph.nodes]))
        edges = sorted([(i,j) for i,j in digraph.edges])
        edge_indices = {(u,v):i for i, (u,v) in enumerate(edges)}
        edge_indices.update({(v,u):i for i, (u,v) in enumerate(edges) if not digraph.has_edge(v,u)})
        self._edges = IndexMap(edges, edge_indices)

        self._node_properties = node_properties
        for prop in node_properties:
            self._nodes.__dict__[prop] = [digraph.nodes[id][prop] for id in self._nodes]

        self._edge_properties = edge_properties
        for prop in edge_properties:
            self._edges.__dict__[prop] = [digraph.edges[id][prop] for id in self._edges]

    @property
    def digraph(self):
        return self._digraph

    def traversal_direction(self, u, v, e):
        if e == (v,u): return 0
        elif e == (u,v): return 1
        raise ValueError("Edge does not match query: %s (%s, %s)" % (str(e), str(u), str(v)))

    def edge_direction(self, u, v):
        """
            Return the direction of an edge relative to a query

            Parameters:
                u: Origin node index
                v: Destination node index

            Returns:
                char: If u->v and v->u are both edges, returns 0
                      If only u->v is an edge, returns 1
                      If only v->u is an edge, returns -1

            Raise:
                KeyError: If neither u->v nor v->u are edges
        """
        has_forward = 1 if (u,v) in self._digraph.edges else 0
        has_backward = -1 if (v,u) in self._digraph.edges else 0
        if has_forward==0 and has_backward==0:
            raise KeyError("No such edge %s,%s" % (str(u),str(v)))
        return has_forward + has_backward

    def get_edges(self, u, v):
        """
            Get all edges between nodes u and v

            Parameters:
                u: Node index
                v: Node index

            Returns:
                generator: a generator yielding edges
        """
        if (u,v) in self._digraph.edges:
            yield (u,v)
        if (v,u) in self._digraph.edges:
            yield (v,u)

    def has_edge(self, u, v):
        return self._digraph.has_edge(u,v)

    def multiplicity(self, u, v):
        """
            Count the number of edges (1 or 2) between adjacent vertices u and v

            Parameters:
                u: Origin node index
                v: Destination node index

            Returns:
                int: The number of edges between u and v
        """
        has_forward = 1 if (u,v) in self._digraph.edges else 0
        has_backward = 1 if (v,u) in self._digraph.edges else 0
        if has_forward==0 and has_backward==0:
            raise KeyError("No such edge %s,%s" % (str(u),str(v)))
        return has_forward + has_backward

    def neighbors(self, u):
        multiplicity = self.multiplicity
        return sum([[v]*multiplicity(u,v) for v in Graph.neighbors(self, u)], [])

    def strict_neighbors(self, u):
        G = self._graph
        return [v for v in G.neighbors(u)]

    def node_edges(self, u):
        """
            Get all the edges into or out of a given node

            Parameters:
                u: Node index

            Returns:
                list: A list of edges
        """
        edge_idx = self._edges.index
        edges = []
        for v in Graph.neighbors(self, u):
            d = self.edge_direction(u,v)
            if d == 1:
                edges.append((u,v))
            if d == -1:
                edges.append((v,u))
            if d == 0:
                if edge_idx[u,v]<edge_idx[v,u]:
                    edges.append((u,v))
                    edges.append((v,u))
                else:
                    edges.append((v,u))
                    edges.append((u,v))
        return edges

    def port_number(self, u, e):
        """ Assigns Digraph graph edge to one of four ports """
        assert self.edges.types is not None, 'Edge types not set. Cannot assign to port without dagger type.'
        v,w = e
        if v is None: return 0
        etype = ( 1 if bool(self.edges.types[self.edges.get(e,-1)]=='x') else 0 ) * 2
        return etype + ( 1 if u==v else 0 )

    @property
    def num_ports(self):
        return 4

    @classmethod
    def from_json(cls, serialized):
        """
            Generate a networkx representation of a serialized json format graph

            Arguments:
                serialized (str): The serialized json representation
        """
        gdict = json.loads(serialized)
        nxg, np, ep = Graph.from_dict(gdict, nx.DiGraph())
        return DiGraph(nxg, np, ep)

class ViGraph(Graph):
    """
        Wrapper for virtual graph support.

        Parameters:
            graph (networkx.Graph): An undirected graph
            bonds (IndexMap, list-type): Bonding edge indices
            edge_properties (list-type) (optional)
            node_properties (list-type) (optional)
    """
    def __init__(self, graph, parent_graph, node_properties=[], edge_properties=[], preserve_bonds=False, single_virtual_port=False):
        super(ViGraph, self).__init__(graph, node_properties=node_properties, edge_properties=edge_properties)

        self._preserve_bonds = preserve_bonds
        self._single_virtual_port = single_virtual_port
        self._reference_graph = parent_graph

    def is_bond(self, u, v):
        return edge(u,v) in self._reference_graph.edges

    def centroids(self, **kwargs):
        return self._reference_graph.centroids(**kwargs)

    def bfs(self, roots):
        """
            Return a block-partitioned BFS of all subgraphs

            Parameters:
                roots (list): A list of starting nodes, one for each connected component
        """
        subgraph = self._reference_graph
        blocks, ancestors, types = subgraph.bfs(roots)
        ancestors = {k:set(ancestors[k]) for k in ancestors}
        virtual_edges = set(self.edges.ids) - set(subgraph.edges.ids)

        node_block_idx = {}
        for val, block in enumerate(blocks):
            for n in block:
                node_block_idx[n] = val

        for n1, n2 in virtual_edges:
            e = edge(n1, n2)
            iblock = node_block_idx[n1]
            jblock = node_block_idx[n2]

            if iblock == jblock:
                types[e] = 'x'
                ancestors[n1].add(n2)
                ancestors[n2].add(n1)
            elif iblock < jblock:
                types[e] = 't'
                ancestors[n2].add(n1)
            else:
                types[e] = 't'
                ancestors[n1].add(n2)

        return blocks, {k:sorted(ancestors[k]) for k in ancestors}, types

    @property
    def bond_gather(self):
        redges = self._reference_graph.edges
        return [list(map(lambda e: redges.get(e, -1), self._edges)), len(redges)]

    @property
    def num_ports(self):
        if self._single_virtual_port: return 3
        return 4

    def port_number(self, u, e):
        """ Assigns Virtual graph edge to one of four ports: virtual/bonding +- tree/cross edge """
        assert self.edges.types is not None, 'Edge types not set. Cannot assign to port without dagger type.'
        v,w = e
        if v is None: return 0

        is_bond = 1 if self._reference_graph.has_edge(v,w) else 0
        if self._single_virtual_port and not is_bond: return 0
        etype = 1 if bool(self.edges.types[self.edges.get(e,-1)]=='x') else 0
        return etype*2 + is_bond
