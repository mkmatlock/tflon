from past.builtins import reduce
from builtins import range
import tensorflow as tf
from tflon.toolkit import Featurizer, Module, Dense, segment_softmax, register_callable, LayerNorm
from tflon.graph.graph import Graph, flip_ancestors, edge
from tflon.graph.data import SparseToDenseRowSelector, RaggedIndexTable, RaggedReduceTable, GatherTable
from tflon.data import DenseNestedTable, RaggedTable
import pandas as pd
import numpy as np

class GRNNFeaturizer(Featurizer):
    def _featurize(self, batch):
        rooter = self._rooter
        dagger = self._dagger
        graphs = batch[self._graph_table].data.iloc[:,0]

        block = np.full((graphs.shape[0], 3), None, dtype=np.object_)
        degree = np.full((graphs.shape[0], 2), None, dtype=np.object_)

        fn = np.full((graphs.shape[0], 3), None, dtype=np.object_)
        fe = np.full((graphs.shape[0], 3), None, dtype=np.object_)
        fl = np.full((graphs.shape[0], 2), None, dtype=np.object_)
        fr = np.full((graphs.shape[0], 2), None, dtype=np.object_)

        bn = np.full((graphs.shape[0], 3), None, dtype=np.object_)
        be = np.full((graphs.shape[0], 3), None, dtype=np.object_)
        bl = np.full((graphs.shape[0], 2), None, dtype=np.object_)
        br = np.full((graphs.shape[0], 2), None, dtype=np.object_)

        if self._edge_properties:
            fep = np.full((graphs.shape[0], 3), None, dtype=np.object_)
            bep = np.full((graphs.shape[0], 3), None, dtype=np.object_)

        def choose_other(value, items):
            if items[0]==value:
                return items[1]
            if items[1]==value:
                return items[0]
            raise Exception("Value '%s' not in '%s'" % (str(value), str(items)))

        def get_ancestor_edges(g, ancestors):
            get_edges = g.get_edges
            get_ancestors = lambda u: [None] if len(ancestors[u])==0 else [choose_other(u,t) for v in ancestors[u] for t in get_edges(u,v)]
            get_ancestor_edges = lambda u: [(None,u)] if len(ancestors[u])==0 else [e for v in ancestors[u] for e in get_edges(u,v)]

            return get_ancestors, get_ancestor_edges

        def get_port_number(g,u,v,e,t,props):
            if None in e: binary = [0]*(len(self.port_properties)+1)
            else: binary = [ g.traversal_direction(u,v,e), 0 if t=='t' else 1 ] + [ 1 if props[p](*e) else 0 for p in self.port_properties ]
            return reduce(lambda a,x: 2*a+x, binary, 0)

        for idx, (_, g) in enumerate(graphs.iteritems()):
            edge_port_props = { prop: getattr(g, prop) for prop in self.port_properties }
            node_indices = g.nodes.index
            node_idx = lambda k: 0 if k is None else node_indices[k]+1
            edge_indices = g.edges.index
            edge_idx = lambda k: 0 if k[0] is None else edge_indices[k]+1

            roots = rooter(g)
            blocks, ancestors, types = dagger(g, roots)
            reverse = flip_ancestors(ancestors)

            ancestors, ancestor_edges = get_ancestor_edges(g, ancestors)
            reverse, reverse_edges = get_ancestor_edges(g, reverse)

            num_nodes = len(g.nodes)
            block[idx,:] = [ [node_idx(u) for b in blocks for u in b], [len(b) for b in blocks], num_nodes ]


            forward_edge_counter = 0
            forward_edge_counts = []
            fnl = []
            fel = []
            fll = []
            frl = []

            backward_edge_counter = 0
            backward_edge_counts = []
            bnl = []
            bel = []
            bll = []
            brl = []

            if self._edge_properties:
                fepl = []
                bepl = []
            # for u,b in (u,b for b in blocks for u in b):
            for b in blocks:
                for u in b:
                # this loop will iterate through every node. So I can put the degree arguments in here as well.
                    ancestors_u = ancestors(u)
                    reverse_u = reverse(u)
                    forward_edge_counter += len(ancestors_u)

                    fnl += [node_idx(u) for i in range(len(ancestors_u))]
                    fel += [node_idx(v) for v in ancestors_u]
                    fll += [get_port_number(g, u, v, e, types.get(edge(*e), 't'), edge_port_props) for v, e in zip(ancestors_u, ancestor_edges(u))]

                    frl.append(len(ancestors_u))

                    backward_edge_counter += len(reverse_u)
                    bnl += [node_idx(u) for i in range(len(reverse_u))]
                    bel += [node_idx(v) for v in reverse_u]
                    bll += [get_port_number(g, u, v, e, types.get(edge(*e), 't'), edge_port_props) for v, e in zip(reverse_u, reverse_edges(u))]

                    brl.append(len(reverse_u))


                    if self._edge_properties:
                        fepl += [edge_idx(e) for e in ancestor_edges(u)]
                        bepl += [edge_idx(e) for e in reverse_edges(u)]
                forward_edge_counts.append(forward_edge_counter)
                backward_edge_counts.append(backward_edge_counter)
                forward_edge_counter = 0
                backward_edge_counter = 0

            indegree = [sum(0 if v is None else 1 for v in ancestors(n)) for n in g.nodes]

            outdegree = [sum(0 if v is None else 1 for v in reverse(n)) for n in g.nodes]

            fn[idx,:] = [fnl, forward_edge_counts, num_nodes]
            fe[idx,:] = [fel, forward_edge_counts, num_nodes]
            fl[idx,:] = [fll, forward_edge_counts]
            fr[idx,:] = [frl, forward_edge_counts]
            bn[idx,:] = [bnl, backward_edge_counts, num_nodes]
            be[idx,:] = [bel, backward_edge_counts, num_nodes]
            bl[idx,:] = [bll, backward_edge_counts]
            br[idx,:] = [brl, backward_edge_counts]
            degree[idx,:] = [ indegree, outdegree ]

            if self._edge_properties:
                num_edges = len(g.edges)
                fep[idx,:] = [fepl,forward_edge_counts,num_edges]
                bep[idx,:] = [bepl,backward_edge_counts,num_edges]

        block_df, degree_df, fn_df, fe_df, fl_df, fr_df, bn_df, be_df, bl_df, br_df = \
            [pd.DataFrame(data=data, index=graphs.index) for data in [block, degree, fn, fe, fl, fr, bn, be, bl, br]]

        update = {'blocks': block_df,
                    'degrees': degree_df,
                    'forward_nodes': fn_df,
                    'forward_edges': fe_df,
                    'forward_ports': fl_df,
                    'forward_reduce': fr_df,
                    'backward_nodes': bn_df,
                    'backward_edges': be_df,
                    'backward_ports': bl_df,
                    'backward_reduce': br_df}
        if self._edge_properties:
            update['forward_edge_props'] = pd.DataFrame(data=fep, index=graphs.index)
            update['backward_edge_props'] = pd.DataFrame(data=bep, index=graphs.index)
        return update

class GRNN(Module):
    """
        Graph recursive neural network (wave network).

        Keyword Arguments:
            graph_table: The table containing objects inheriting from tflon.graph.Graph
            num_ports (int):        Number of ports for mixgate (number of edge types * 2) (default=2)
            edge_properties (bool): Whether edges have properties (default=False)

        Other Arguments:
            dagger (callable):      A function which returns node blocks, ancestors, and edge types for scheduling graph recursion state updates (default=Graph.bfs)
            rooter (callable):      A function which returns the starting node for the dagger (default=Graph.centroids)
    """
    def build(self, graph_table, num_ports=2, edge_properties=False, port_properties=[]):
        self._rooter = self.rooter
        self._dagger = self.dagger
        self._edge_properties = edge_properties
        self._num_ports  = num_ports
        self.port_properties = port_properties

        self._graph_table       = graph_table
        self._degrees           = self.add_input('degrees', [None, 2], dtype=tf.float32, ttype=DenseNestedTable)
        self._blocks            = self.add_input('blocks',  [None, None], dtype=tf.int32, ttype=SparseToDenseRowSelector)

        self._forward_nodes     = self.add_input('forward_nodes', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedIndexTable)
        self._forward_edges     = self.add_input('forward_edges', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedIndexTable)
        self._forward_ports     = self.add_input('forward_ports', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedTable)
        self._forward_reduce    = self.add_input('forward_reduce', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedReduceTable)

        self._backward_nodes    = self.add_input('backward_nodes', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedIndexTable)
        self._backward_edges    = self.add_input('backward_edges', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedIndexTable)
        self._backward_ports    = self.add_input('backward_ports', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedTable)
        self._backward_reduce   = self.add_input('backward_reduce', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedReduceTable)
        self._recursion_depth = tf.shape(self._blocks)[-1]

        if self._edge_properties:
            self._forward_edge_props = self.add_input('forward_edge_props', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedIndexTable)
            self._backward_edge_props = self.add_input('backward_edge_props', [None, None], dtype=tf.int32, ragged=True, ttype=RaggedIndexTable)

    def _featurizer(self):
        return GRNNFeaturizer(self, _graph_table=self._graph_table, _rooter=self._rooter, _dagger=self._dagger, _edge_properties=self._edge_properties, port_properties=self.port_properties)

    @register_callable
    def wrap_cell(self, cell, **kwargs):
        """
            Add a mix gate to an RNNCell

            Parameters:
                cell (RNNCell): An RNNCell to wrap
                **kwargs:       Additional arguments passed to the GRNNCell constructor

            Returns:
                GRNNCell: A graph recurrent cell
        """
        return GRNNCell(cell, self._num_ports, self._edge_properties, **kwargs)

    def call(self, inputs=None, forward_cell=None, backward_cell=None, edge_properties=None, node_properties=None):
        """
            Perform a single forward-backward pass of graph recursion

            Parameters:
                inputs (tensor-like):     The node properties/states
                forward_cell (callable):  RNNCell to use for the forward pass, usually an instance of tf.nn.rnn_cell.RNNCell, or a callable accepting two arguments (inputs, states)
                backward_cell (callable): RNNCell to use for the backward pass, usually an instance of tf.nn.rnn_cell.RNNCell, or a callable accepting two arguments (inputs, states)

            Keyword Arguments:
                edge_properties (tensor-like): Edge properties for the graph, if available (default=None)

            Returns:
                Tensor: The updated node states
        """
        def get_blocks(i):
            blocks = tf.reshape(tf.slice(self._blocks, [0,i], [-1,1]), shape=(-1,))
            return tf.concat([tf.zeros(shape=[1], dtype=tf.int32), blocks], axis=0)

        W = self._recursion_depth

        # Forward pass
        if not isinstance(forward_cell, GRNNCell):
            forward_cell = GRNNCell(forward_cell, self._num_ports, self._edge_properties)
        inputs = tf.concat([tf.zeros(shape=(1, tf.shape(inputs)[1])), inputs], axis=0)
        degrees = tf.concat([tf.zeros(shape=(1,2)), self._degrees], axis=0)
        if self._edge_properties:
            assert edge_properties is not None, "With edge_properties=True, must past tensor of edge_properties to call()"
            edge_properties = tf.concat([tf.zeros(shape=(1, tf.shape(edge_properties)[1])), edge_properties], axis=0)
        if node_properties is not None:
            node_properties = tf.concat([tf.zeros(shape=(1, tf.shape(node_properties)[1])), node_properties], axis=0)
        states = inputs
        iterator = tf.constant(0, dtype=tf.int32)
        _, intermediate = tf.while_loop(lambda i, s: i < W,
                                  lambda i, s: (i+1, forward_cell(s, degrees,
                                                                  get_blocks(i),
                                                                  self._forward_nodes.slice(i),
                                                                  self._forward_edges.slice(i),
                                                                  self._forward_ports.slice(i),
                                                                  self._forward_reduce.slice(i),
                                                                  self._forward_edge_props.slice(i) if self._edge_properties else None,
                                                                  edge_properties if self._edge_properties else None,
                                                                  node_properties)),
                                  (iterator, states),
                                  back_prop=True,
                                  swap_memory=False)

        # Backward pass
        if not isinstance(backward_cell, GRNNCell):
            backward_cell = GRNNCell(backward_cell, self._num_ports, self._edge_properties)
        states = intermediate
        iterator = tf.constant(0, dtype=tf.int32)
        degrees = tf.reverse(degrees, axis=[1])
        _, output = tf.while_loop(lambda i, s: i < W,
                                  lambda i, s: (i+1, backward_cell(s, degrees,
                                                                   get_blocks(W-(i+1)),
                                                                   self._backward_nodes.slice(W-(i+1)),
                                                                   self._backward_edges.slice(W-(i+1)),
                                                                   self._backward_ports.slice(W-(i+1)),
                                                                   self._backward_reduce.slice(W-(i+1)),
                                                                   self._backward_edge_props.slice(W-(i+1)) if self._edge_properties else None,
                                                                   edge_properties if self._edge_properties else None,
                                                                   node_properties)),
                                  (iterator, states),
                                  back_prop=True,
                                  swap_memory=False)
        return tf.slice(output, [1,0], [-1,-1])

    def _parameters(self):
        return dict(dagger=Graph.bfs, rooter=Graph.centroids)

class MixGate(Module):
    """
        Module defining the special mix gate used to combine input states from multiple edges for form a message state

        Parameters:
            state_size (int): The width of the node state
            activation (callable): An activation function for computing weightings
            ports (int): The number of ports
            types (list): The types of weightings to include, valid values include 'softmax', 'softsign', and 'sum'
            edge_properties (bool): Whether edge properties will be provided
            normalization (str):    The type of normalization to apply: 'none', 'layer', 'degree' (default='none')
    """
    def build(self, state_size, activation, ports, types, edge_properties, normalization):
        self._num_ports = ports
        self._funcs = {}
        if edge_properties:
            self._reduce_state = Dense(state_size, activation=activation)
        for port in range(ports):
            if 'softmax' in types:
                self._funcs.setdefault('softmax', list()).append( Dense(state_size, activation=activation) )
            if 'softsign' in types:
                self._funcs.setdefault('softsign', list()).append( Dense(state_size, activation=tf.nn.softsign, bias_initializer=tf.constant_initializer(1.) ) )
            if 'sum' in types:
                self._funcs.setdefault('sum', list()).append( Dense(state_size, activation=activation) )

        if 'softmax' in types:
            self._mix = self.get_bias(shape=[state_size], name="mixture")
        self._offset = self.get_bias(shape=[state_size], name="offset")
        self._normalization = normalization
        assert normalization in ['none', 'layer', 'degree'], "Invalid value for parameter normalization: must be 'none', 'layer', or 'degree'"
        if normalization == 'layer':
            self._layer_normalizer = LayerNorm()

    def mix_ports(self, inputs_and_states, ports, mix_funcs):
        parts = tf.dynamic_partition( inputs_and_states, ports, self._num_ports )
        restitch = tf.dynamic_partition( tf.range(0, tf.shape(inputs_and_states)[0]), ports, self._num_ports )

        logits = [ func(parts[i]) for i, func in enumerate(mix_funcs) ]
        return tf.dynamic_stitch( restitch, logits )

    def call(self, states, degrees, nodes, ancestors, ports, redux, edges, edge_properties, node_properties):
        """
            Combine input states to produce a state message for graph recursive node updates

            Parameters:
                states (tensor-like):    Node properties
                degrees (tensor-like):   In and out degrees for each node
                nodes (tensor-like):     Indices for the nodes being updated
                ancestors (tensor-like): Indices of ancestor nodes
                ports (tensor-like):     Port types for each edge
                redux (tensor-like):     Reduction indices
                edges (tensor-like):     Edge indices
                edge_properties (tensor-like): Edge properties

            Returns:
                Tensor: Message states for each node in the update
        """
        inputs = tf.gather(states, nodes)
        degrees = tf.gather(degrees, nodes)
        states = tf.gather(states, ancestors)
        inputs_and_states = tf.concat([inputs, degrees, states], axis=1)
        if edges is not None:
            edge_props = tf.gather(edge_properties, edges)
            inputs_and_states = tf.concat([inputs_and_states, edge_props], axis=1)
            states = self._reduce_state(tf.concat([states, edge_props], axis=1))
        if node_properties is not None:
            self_properties = tf.gather(node_properties, nodes)
            ancestor_properties = tf.gather(node_properties, ancestors)
            inputs_and_states = tf.concat([inputs_and_states, self_properties, ancestor_properties], axis=1)

        terms = []
        if 'softmax' in self._funcs:
            softmax_wts = segment_softmax(self.mix_ports(inputs_and_states, ports, self._funcs['softmax']), redux)
            softmax_term = self._mix * tf.math.segment_sum(softmax_wts*states, redux)
            terms.append(softmax_term)
        if 'softsign' in self._funcs:
            softsign_wts = self.mix_ports(inputs_and_states, ports, self._funcs['softsign'])
            softsign_term = tf.math.segment_sum(softsign_wts*states, redux)
            terms.append(softsign_term)
        if 'sum' in self._funcs:
            sum_wts = self.mix_ports(inputs_and_states, ports, self._funcs['sum'])
            sum_term = tf.math.segment_sum(sum_wts, redux)
            terms.append(sum_term)

        rval = tf.add_n(terms) + self._offset
        if self._normalization == 'layer':
            rval = self._layer_normalizer(rval)
        if self._normalization == 'degree':
            degrees = degrees[:,0:1]
            degrees = tf.segment_max(degrees, redux)
            rval = rval / tf.maximum(degrees, 1)
        return rval


class MiniGRUCell(Module):
    """
        A special GRUCell which does not have a read gate. This is commonly used in GRNNs because the read gate is redundant when a mix gate is used.

        Parameters:
            state_size (int): The width the node states

        Keyword Arguments:
            activation (callable): An activation function used for computing the output gate network
    """
    def build(self, state_size, activation=tf.nn.elu):
        self._state_size = state_size
        self._update_net = Dense(state_size, activation=tf.nn.sigmoid, bias_initializer=tf.constant_initializer(-1.))
        self._output_net = Dense(state_size, activation=activation)

    @property
    @register_callable
    def state_size(self):
        """
            Get this RNNCell's state size
        """
        return self._state_size

    @register_callable
    def zero_state(self, batch_size, dtype):
        """
            Create initial zero state with given batch size and type

            Parameters:
                batch_size (int): The first dimension size of the state vector
                dtype (tf.type):  The state vector dtype
        """
        return tf.zeros((batch_size, self._state_size), dtype=dtype)

    def call(self, inputs, state):
        """
            Perform a state update

            Parameters:
                inputs (tensor-like): The current input values
                states (tensor-like): The current state values

            Returns:
                tuple (Tensor, Tensor): The output and state tensors, which are identical for a GRU
        """
        inputs_and_state = tf.concat([inputs, state], axis=1)
        s = state
        u = self._update_net(inputs_and_state)
        o = self._output_net(inputs_and_state)
        c = o*u + s*(1 - u)
        return c, c

class GRNNCell(Module):
    """
        An RNNCell wrapper which applies the mix gate to combine multiple states prior to a state update

        Parameters:
            rnn_cell (callable):    A cell matching the tf.nn.rnn_cell.RNNCell API, which is used to compute the state update
            ports (int):            The number of mix gate ports
            edge_properties (bool): Whether edge properties are included in the mix gate calculation

        Other Parameters:
            activation (callable): An activation function for the mix gate weighting functions (default=tf.nn.elu)
            gate_types (list):     Types of mix gate weightings to include (default=['softmax','softsign'])
            normalization (str):   Perform a normalization on mix gate 'none', 'layer', or 'degree' (default='none')
            normalize_output (bool): If True, perform layer normalization on the output of this recurrence (default=False)
    """
    def build(self, rnn_cell, ports, edge_properties):
        self._mix_network = MixGate(rnn_cell.state_size, self.activation, ports, self.gate_types, edge_properties, self.normalization)
        self._rnn_cell = rnn_cell
        self._normalizer = LayerNorm() if self.normalize_output else lambda x:x

    def call(self, states, degrees, block, nodes, ancestors, ports, redux, edges, edge_properties, node_properties):
        """
            Compute a partial state update of a subset of nodes

            Parameters:
                states (tensor-like):          Current node states
                degrees (tensor-like):         Node in and out degrees
                block (tensor-like):           The subset of nodes to update
                nodes (tensor-like):           A list of nodes at the arrow side of each update edge
                ancestors (tensor-like):       A list of ancestor nodes of each update edge
                ports (tensor-like):           A list of port types for each update edge
                redux (tensor-like):           Reduction indices (update edges -> nodes)
                edges (tensor-like):           Edge indices for each update edge
                edge_properties (tensor-like): Edge properties for each update edge

            Returns:
                Tensor: updated node states
        """
        pass_states, block_inputs = tf.dynamic_partition(states, block, 2)
        pass_indices, block_indices = tf.dynamic_partition(tf.range(tf.shape(states)[0]), block, 2)
        block_degrees = tf.gather(degrees, block_indices)
        if node_properties is not None:
            block_properties = tf.gather(node_properties, block_indices)
            block_inputs = tf.concat([block_inputs, block_properties, block_degrees], axis=1)
        else:
            block_inputs = tf.concat([block_inputs, block_degrees], axis=1)
        mixed_states = self._mix_network(states, degrees, nodes, ancestors, ports, redux, edges, edge_properties, node_properties)

        _, new_states = self._rnn_cell(block_inputs, mixed_states)
        new_states = self._normalizer(new_states)

        return tf.dynamic_stitch([pass_indices, block_indices], [pass_states, new_states])

    def _parameters(self):
        return {'activation': tf.nn.elu,
                'gate_types': ['softmax','softsign'],
                'normalization': 'none',
                'normalize_output': False}

class DynamicPassesFeaturizer(Featurizer):
    def _featurize(self, batch):
        graphs     = batch[self._graph_table].data.iloc[:,0]
        node_pass = np.full((graphs.shape[0],1), None, dtype=np.object_)
        node_index = np.full((graphs.shape[0],2), None, dtype=np.object_)
        if self._get_edges:
            edge_pass = np.full((graphs.shape[0],1), None, dtype=np.object_)
            edge_index = np.full((graphs.shape[0],2), None, dtype=np.object_)

        for idx, (_, g) in enumerate(graphs.iteritems()):
            num_nodes = len(g.nodes)
            if self.feature == 'radius':
                p = np.ceil( num_nodes / float(g.radius)) - 1
            elif self.feature == 'quadratic':
                p = np.floor(np.sqrt(num_nodes)) - 1
            elif self.feature == 'logarithmic':
                p = max(np.floor(np.log2(num_nodes)) - 1, 0)
            else:
                raise ValueError("feature must be 'radius', 'quadratic' or 'logarithmic', got '%s'" % (self.feature))
            p = min(p, self.max_passes-1)
            node_pass[idx,0] = [p]*num_nodes
            node_index[idx,:] = list(range(num_nodes)), num_nodes

            if self._get_edges:
                num_edges = len(g.edges)
                edge_pass[idx,0] = [p]*num_edges
                edge_index[idx,:] = list(range(num_edges)), num_edges

        update = {
            'node_pass': pd.DataFrame(data=node_pass),
            'node_index': pd.DataFrame(data=node_index)
        }
        if self._get_edges:
            update['edge_pass'] = pd.DataFrame(data=edge_pass)
            update['edge_index'] = pd.DataFrame(data=edge_index)
        return update

class DynamicPasses(Module):
    """Module for collecting the output node properties for dynamic depth messaging operations on variable size and shape graphs.

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph

        Keyword Arguments:
            get_edges (bool): Whether to get edge properties as well as nodes (default=False)

        Other Parameters:
            feature (str):    The type of graph feature used to decide the number of passes, either 'radius', 'quadratic', or 'logarithmic' (default=radius)
            max_passes (int): The maximum number of allowed passes (default=7)

    """
    def build(self, graph_table, get_edges=False):
        # access parameters to supress warning
        self.feature, self.max_passes

        self._graph_table = graph_table

        self._node_pass   = self.add_input('node_pass', [None, 1], dtype=tf.int32, ttype=DenseNestedTable)
        self._node_index  = self.add_input('node_index',  [None, 1], dtype=tf.int32, ttype=GatherTable)
        self._node_gather = tf.concat([self._node_pass, self._node_index], axis=1)

        self._get_edges = get_edges
        if get_edges:
            self._edge_pass = self.add_input('edge_pass', [None, 1], dtype=tf.int32, ttype=DenseNestedTable)
            self._edge_index = self.add_input('edge_index',  [None, 1], dtype=tf.int32, ttype=GatherTable)
            self._edge_gather = tf.concat([self._edge_pass, self._edge_index], axis=1)

    def _featurizer(self):
        return DynamicPassesFeaturizer(self, _graph_table=self._graph_table, _get_edges=self._get_edges)

    @property
    @register_callable
    def batch_max_passes(self):
        """Return the number of passes required to compute all message operations for the current mini-batch (may be less than max_passes)"""
        return tf.reduce_max(self._node_pass) + 1

    def call(self, node_states, edge_states=None):
        """Gather node (optionally, edge states) from a tensor array containing output from each messaging pass up to max_passes

            Parameters:
                node_states (TensorArray): An array of node states from each pass

            Keyword Arguments:
                edge_states (TensorArray): An array of edge states from each pass (default=None)

            Returns:
                Tensor or tuple: If edge_states is provided, returns a tuple of extracted node and edge states, otherwise returns a tensor of node states
        """

        def gather_states(states, index):
            all_states = states.stack()
            return tf.gather_nd(all_states, index)

        rval = gather_states(node_states, self._node_gather)
        if edge_states is not None:
            rval = tuple((rval, gather_states(edge_states, self._edge_gather)))
        return rval

    def _parameters(self):
        return {'feature':'radius', 'max_passes':7}
