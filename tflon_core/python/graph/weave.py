from builtins import range
import numpy as np
import pandas as pd
import tensorflow as tf
from tflon.data import DenseNestedTable
from tflon.graph.data import PoolTable, GatherTable
from tflon.toolkit import Featurizer, Module, Dense, batch_normalize, register_callable

class Weave(Module):
    """
        Implementation of the weave module from the paper Molecular Graph Convolutions: Moving Beyond Fingerprints (https://link.springer.com/article/10.1007/s10822-016-9938-8)

        Parameters:
            graph_table: The table containing objects inheriting from tflon.graph.Graph

        Other Parameters:
            edge_properties (bool): Whether to expect edge properties (default=False)
            digraphs (bool):        Whether to expect digraphs (default=False; not supported)
            activation (callable):  Activation function to use for weave ops (default=tf.nn.elu)
            batch_normalize (bool): Perform batch normalization (default=False)
            distance (int):         The distance, in number of bonds, over which to perform pair weave ops (default=2)
            pair_width (int):       Number of output pair features at each layer, required
            node_width (int):       Number of output node features at each layer, required
            pair_hidden (int):      Number of hidden features for pair weave, if None, same as pair_width (default=None)
            node_hidden (int):      Number of hidden features for node weave, if None, same as node_width (default=None)
    """
    def _node_to_node(self, nodes, width, layer, ext=False, apply_norm=False):
        with tf.name_scope("node_to_node_%d" % (1 if ext else 0)):
            node_input = nodes.get_shape()[-1].value

            weights = self.get_weight("node_to_node_%d_%d_weights" % (layer, 1 if ext else 0), [node_input, width])
            bias = self.get_bias("node_to_node_%d_%d_bias" % (layer, 1 if ext else 0), [width])

            preactivation = tf.matmul( nodes, weights ) + bias
            if apply_norm:
                preactivation = batch_normalize(preactivation)
            return self.activation( preactivation )

    def _apply_ports(self, inputs, funcs):
        partitions = tf.dynamic_partition( inputs, self._pair_features, self.num_ports )
        restitch = tf.dynamic_partition( tf.range(0, tf.shape(inputs)[0]), self._pair_features, self.num_ports )
        logits = [func(partitions[i]) for i, func in enumerate(funcs)]
        return tf.dynamic_stitch( restitch, logits )

    def _node_to_pair(self, nodes, width, layer):
        with tf.name_scope("node_to_pair"):
            left = tf.gather(nodes, self._left_node_gather)
            right = tf.gather(nodes, self._right_node_gather)

            funcs = [Dense(width, activation=self.activation) for _ in range(self.num_ports)]
            return self._apply_ports(tf.concat([left, right], axis=1), funcs) +\
                   self._apply_ports(tf.concat([right, left], axis=1), funcs)

    def _pair_to_node(self, pairs, width, layer):
        with tf.name_scope("pair_to_node"):
            funcs = [Dense(width, activation=self.activation) for _ in range(self.num_ports)]
            activations = self._apply_ports(pairs, funcs)

            extended = tf.concat([tf.zeros((1, tf.shape(activations)[-1])), activations], axis=0)
            gathered = tf.gather(extended, self._pair_gather+1)
            return tf.math.segment_sum(gathered, self.pair_reduce)

    def _pair_to_pair(self, pairs, width, layer, ext=False, apply_norm=False):
        with tf.name_scope("pair_to_pair_%d" % (1 if ext else 0)):
            pair_input = pairs.get_shape()[-1].value

            weights = self.get_weight("pair_to_pair_%d_%d_weights" % (layer, 1 if ext else 0), [pair_input, width])
            bias = self.get_bias("pair_to_pair_%d_%d_bias" % (layer, 1 if ext else 0), [width])

            preactivation = tf.matmul( pairs, weights ) + bias
            if apply_norm:
                preactivation = batch_normalize(preactivation)
            return self.activation( preactivation )

    def _node_to_edge(self, nodes, width, layer):
        with tf.name_scope("node_to_edge"):
            node_input = nodes.get_shape()[-1].value

            weights = self.get_weight("node_to_edge_%d_weights" % (layer), [2*node_input, width])
            bias = self.get_bias("node_to_edge_%d_bias" % (layer), [width])
            left_node = tf.gather(nodes, tf.gather(self._left_node_gather, self._edge_pairs))
            right_node = tf.gather(nodes, tf.gather(self._right_node_gather, self._edge_pairs))

            left_right = tf.concat([left_node, right_node], axis=1)
            right_left = tf.concat([right_node, left_node], axis=1)

            return self.activation( tf.matmul( left_right, weights ) + bias ) +\
                   self.activation( tf.matmul( right_left, weights ) + bias )

    def _pair_to_edge(self, pairs, width, layer, ext=False, apply_norm=False):
        with tf.name_scope("pair_to_edge_%d" % (1 if ext else 0)):
            edge_pairs = tf.gather(pairs, self._edge_pairs)
            pair_input = edge_pairs.get_shape()[-1].value

            weights = self.get_weight("pair_to_edge_%d_%d_weights" % (layer, 1 if ext else 0), [pair_input, width])
            bias = self.get_bias("edge_to_edge_%d_%d_bias" % (layer, 1 if ext else 0), [width])

            preactivation = tf.matmul( edge_pairs, weights ) + bias
            if apply_norm:
                preactivation = batch_normalize(preactivation)
            return self.activation( preactivation )

    def build(self, graph_table):
        self._graph_table = graph_table
        self._digraphs = self.get_parameter('digraphs')
        self._edges = self.get_parameter('edge_properties')

        self.num_ports = 1
        if self.distance>1:
            self.num_ports+=1
        if self._digraphs:
            self.num_ports+=1
        if not self.has_parameter('node_hidden'): self.set_parameter('node_hidden', self.node_width)
        if not self.has_parameter('pair_hidden'): self.set_parameter('pair_hidden', self.pair_width)

        self._node_gather = self.add_input('node_to_pair', shape=[None,2], dtype=tf.int32, ttype=GatherTable)
        self._left_node_gather, self._right_node_gather = [tf.reshape(T, (-1,)) for T in tf.split(self._node_gather, 2, axis=1)]

        self._pair_pool = self.add_input('pair_to_node', shape=[None,2], dtype=tf.int32, ttype=PoolTable)
        self._pair_gather, self.pair_reduce = [tf.reshape(T, (-1,)) for T in tf.split(self._pair_pool, 2, axis=1)]

        self._pair_features_input = self.add_input('pair_features', shape=[None,1], dtype=tf.int32, ttype=DenseNestedTable)
        self._pair_features = tf.reshape(self._pair_features_input, (-1,))
        if self._edges:
            self._edge_gather_input = self.add_input('edge_indices', shape=[None,1], dtype=tf.int32, ttype=GatherTable)
            self._edge_gather = tf.reshape(self._edge_gather_input, (-1,))
            self._edge_pairs_input = self.add_input('edge_pair_indices', shape=[None,1], dtype=tf.int32, ttype=GatherTable)
            self._edge_pairs = tf.reshape(self._edge_pairs_input, (-1,))

    @register_callable
    def initial_pair_state(self, edges=None):
        """
            Create initial zero-state values for pairs. If edge properties are available, gather edge properties for adjacent pairs.

            Keyword Arguments:
                edges (tensor-like): Optional edge properties (default=None)

            Returns:
                Tensor: initial pair states
        """
        if self._edges:
            edges = tf.concat([tf.zeros(shape=(1,tf.shape(edges)[-1])), edges], axis=0)
            pairs = tf.gather(edges, self._edge_gather+1)
        else:
            pairs = tf.zeros((tf.shape(self._pair_features_input)[0], self.pair_width))
        return pairs

    def outputs(self, nodes, pairs, include_edges=None):
        """
            Add a weave module to the compute graph without updating pair states.

            Parameters:
                nodes (tensor-like): Node properties
                pairs (tensor-like): Pair properties

            Keyword Arguments:
                include_edges (bool): Include an edge update and return the resulting edge properties (ignore non-adjacent pairs)

            Returns:
                Tensor or tuple(Tensor, Tensor): output node states and (optional) edge states
        """
        if include_edges is None: include_edges = self._edges
        apply_norm = self.get_parameter('batch_normalize')

        AtoA = self._node_to_node(nodes, self.node_hidden, self.calls)
        PtoA = self._pair_to_node(pairs, self.pair_hidden, self.calls)
        nodes = self._node_to_node(tf.concat([AtoA, PtoA], axis=1), self.node_width, self.calls, ext=True, apply_norm=apply_norm)

        if include_edges:
            AtoE = self._node_to_edge(nodes, self.node_hidden, self.calls)
            PtoE = self._pair_to_edge(pairs, self.pair_hidden, self.calls)
            edges = self._pair_to_pair(tf.concat([AtoE, PtoE], axis=1), self.pair_width, self.calls, ext=True, apply_norm=apply_norm)
            return nodes, edges

        return nodes

    def call(self, nodes, pairs=None):
        """
            Add a single weave module to the compute graph. Pair properties for the first weave should be generated with Weave.initial_pair_state

            Parameters:
                nodes (tensor-like): Node properties
                pairs (tensor-like): Pair properties

            Returns:
                tuple (Tensor, Tensor): Updated node and pair properties
        """
        apply_norm = self.get_parameter('batch_normalize')

        if pairs is None:
            nodes = self._node_to_node(nodes, self.node_hidden, self.calls)
            pairs = self._node_to_pair(nodes, self.node_hidden, self.calls)
        else:
            AtoA = self._node_to_node(nodes, self.node_hidden, self.calls)
            PtoA = self._pair_to_node(pairs, self.pair_hidden, self.calls)
            nodes = self._node_to_node(tf.concat([AtoA, PtoA], axis=1), self.node_width, self.calls, ext=True, apply_norm=apply_norm)

            AtoP = self._node_to_pair(nodes, self.node_hidden, self.calls)
            PtoP = self._pair_to_pair(pairs, self.pair_hidden, self.calls)
            pairs = self._pair_to_pair(tf.concat([AtoP, PtoP], axis=1), self.pair_width, self.calls, ext=True, apply_norm=apply_norm)

        return nodes, pairs


    def _featurizer(self):
        return WeaveFeaturizer(self, _graph_table=self._graph_table, _edges=self._edges)

    def _parameters(self):
        return {'edge_properties':False,
                'digraphs':False,
                'activation': tf.nn.elu,
                'batch_normalize': False,
                'distance': 2,
                'pair_width':None,
                'node_width':None,
                'pair_hidden':None,
                'node_hidden':None}


class WeaveFeaturizer(Featurizer):
    def _process_graph(self, graph):
        node_indices = graph.nodes.index
        edge_indices = graph.edges.index
        pair_indices = {}
        pair_ports = []
        node1_gather = []
        node2_gather = []
        pair_gather = []
        pair_reduce = []
        edge_gather = []
        edge_pairs = []

        for v in graph.nodes:
            depth = graph.neighborhood(v, self.distance)
            depth.pop(v)
            neighbors = sorted(depth.keys())
            for w in neighbors:
                D = depth[w]
                pair_idx = (v, w) if v < w else (w, v)
                if pair_idx not in pair_indices:
                    pair_indices[pair_idx] = len(pair_indices)
                    pair_ports.append(1 if D>1 else 0)
                    if D==1:
                        edge_gather.append( edge_indices[pair_idx] )
                        edge_pairs.append( pair_indices[pair_idx] )
                    else:
                        edge_gather.append( -1 )

                    node1_gather.append( node_indices[v] )
                    node2_gather.append( node_indices[w] )

                pair_gather.append( pair_indices[pair_idx] )

            if len(neighbors)==0:
                pair_gather.append( -1 )
                pair_reduce.append(  1 )
            else:
                pair_reduce.append( len(neighbors) )

        return node1_gather, node2_gather, pair_gather, pair_reduce, len(pair_indices), pair_ports, edge_gather, edge_pairs

    def _featurize(self, batch):
        graphs = batch[self._graph_table].data

        node_gather = np.full((graphs.shape[0],3), None, dtype=np.object_)
        pair_pool = np.full((graphs.shape[0],3), None, dtype=np.object_)
        pair_features = np.full((graphs.shape[0],1), None, dtype=np.object_)
        if self._edges:
            edge_gather = np.full((graphs.shape[0],2), None, dtype=np.object_)
            edge_pairs = np.full((graphs.shape[0],2), None, dtype=np.object_)

        for i in range(graphs.shape[0]):
            graph = graphs.iloc[i,0]

            node1_gather, node2_gather, pair_gather, pair_reduce, num_pairs, ports, edge_indices, edge_pair_indices  = self._process_graph(graph)

            node_gather[i,0] = node1_gather
            node_gather[i,1] = node2_gather
            node_gather[i,2] = len(graph.nodes)
            pair_pool[i,0] = pair_gather
            pair_pool[i,1] = pair_reduce
            pair_pool[i,2] = num_pairs
            pair_features[i,0] = ports
            if self._edges:
                edge_gather[i,0] = edge_indices
                edge_gather[i,1] = len(graph.edges)
                edge_pairs[i,0] = edge_pair_indices
                edge_pairs[i,1] = num_pairs

        update = {'node_to_pair': pd.DataFrame(data=node_gather, index=graphs.index),
                'pair_to_node': pd.DataFrame(data=pair_pool, index=graphs.index),
                'pair_features': pd.DataFrame(data=pair_features, index=graphs.index)}

        if self._edges:
            update['edge_indices'] = pd.DataFrame(data=edge_gather, index=graphs.index)
            update['edge_pair_indices'] = pd.DataFrame(data=edge_pairs, index=graphs.index)
        return update


