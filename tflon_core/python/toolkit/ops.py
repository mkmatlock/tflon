from builtins import range
import tensorflow as tf
from tflon.toolkit.toolkit import set_name_scope, py_func, current_tower
import numpy as np

# Placeholder for importing future C op extensions
#_op_lib = tf.load_op_library( resource_filename( 'tflon.lib', 'tflon_ops.so' ) )

@set_name_scope
def residual_covariance(residuals, include_diag=False):
    """
        Given estimated residuals (X-mu), compute the covariances (and optionally, variances)

        Parameters:
            residuals (tensor-like): A 2-D tensor of batch x num_variables, the inputs, centered by subtracting the estimated means (X-mu)

        Keyword Arguments:
            include_diag (bool):     If true, also calculate the variances along the diagonal. If false, output zeros on the diagonal (default=False)

        Returns:
            Tensor: a 2-D covariance tensor of shape num_variables x num_variables
    """

    O = residuals.get_shape()[-1].value
    N = tf.shape(residuals)[0]

    residuals = tf.where( tf.is_nan( residuals ), tf.zeros_like( residuals ), residuals )
    residuals = tf.split( residuals, O, axis=1 )

    cov = [[0]*O for _ in range(O)]
    for i in range(O):
        for j in range(i if include_diag else i+1, O):
            cov[i][j] = tf.reduce_sum(residuals[i] * residuals[j]) / tf.cast(N-1, dtype=tf.float32)
            cov[j][i] = cov[i][j]
    return tf.stack([tf.stack(row) for row in cov])

def _segment_topN_impl(inputs, segments, N):
    num_segments = np.max(segments)+1
    cols = inputs.shape[1]
    result = np.full([num_segments, cols * N, 2], -1, dtype=np.int32)
    result[:,:,1] = sum([[i]*N for i in range(cols)], [])

    for i in range(num_segments):
        indices = np.argwhere(segments==i)[:,0]
        block = inputs[indices]
        for j in range(cols):
            argindices = np.argsort( block[:,j], axis=0 )
            topN = indices[argindices[-N:]]
            L = min(N, len(topN))
            result[i,j*N:j*N+L,0] = topN[::-1]
    result[:,:,0] += 1
    return result

@set_name_scope
def segment_topN(inputs, segments, N=5, default_value=0):
    """
        Computes indices the topN elements for each segment along the first axis of inputs, returning the indices of each element in the final dimension

        For input of shape (?, 4), num_segments=10, N=5, output rows would have shape: (10, 20, 2)

        use with values = tf.gather_nd(inputs, segment_topN_result) to collect the output

        Parameters:
            inputs (tensor): a 2-dimensional tensor
            segments (tensor): a 1-dimensional tensor of segment IDs corresponding to the first axis of inputs

        Keyword Arguments:
            N (int):       The number of top elements to return (default=5)
            default_value: The default value for the tensor, used when fewer than N top values are available (default=0)
    """
    indices = py_func(_segment_topN_impl, [inputs, segments, N], [tf.int32], grad=None)[0]
    padded = tf.concat([tf.fill((1, tf.shape(inputs)[-1]), tf.constant(default_value, dtype=inputs.dtype)), inputs], axis=0)
    gather = tf.gather_nd(padded, indices)

    shape = [dim.value for dim in inputs.get_shape()]
    shape[1] = shape[1] * N
    gather.set_shape(shape)

    return gather

@set_name_scope
def segment_reduce_scatter(X, segs, axis=None, reduce_func = 'sum'):
    assert reduce_func in ['sum', 'mean'], "Argument reduce_func invalid value: %s" % (reduce_func)
    assert axis in [None, 0], "Argument axis invalid value: %s" % (str(axis))

    segment_op = tf.math.segment_sum if reduce_func=='sum' else tf.segment_mean
    Xs = segment_op(X, segs)

    if axis is None:
        reduce_func = tf.reduce_sum if reduce_func=='sum' else tf.reduce_mean
        Xs = reduce_func(Xs, axis=1, keepdims=True)

    return tf.gather(Xs, segs)

@set_name_scope
def interpolate_linear(x, fx, y):
    batch_size = tf.shape(x)[0]
    num_feat = tf.shape(y)[-1]
    num_bins = fx.get_shape()[0].value
    zeros = tf.zeros( (batch_size, num_feat), dtype=y.dtype )

    total = zeros
    total += tf.where( tf.less_equal(x, fx[0]), tf.tile(tf.slice(y, (0,0), (1,-1)), [batch_size, 1]), zeros )
    for i in range(num_bins-1):
        x1,x2 = fx[i],fx[i+1]
        y1 = tf.tile(tf.slice(y, (i, 0), (1,-1)), [batch_size, 1])
        y2 = tf.tile(tf.slice(y, (i+1, 0), (1,-1)), [batch_size, 1])
        interp = tf.tile(tf.reshape((x-x1)/(x2-x1), (-1,1)), [1, num_feat])
        val = y2 * interp + y1 * (1-interp)
        cond = tf.logical_and( tf.greater(x, x1), tf.less_equal(x, x2) )
        total += tf.where( cond, val, zeros )

    total += tf.where( tf.greater(x, fx[-1]), tf.tile(tf.slice(y, (num_bins-1,0), (1,-1)), [batch_size, 1]), zeros )
    return total

@set_name_scope
def interpolate_rbf(x, fx, y, radius=1., dtype=tf.float32):
    radius = tf.cast(radius, dtype)
    if len(radius.get_shape()) == 0 or radius.get_shape()[0].value == 1:
        radius = tf.fill( dims=tf.shape(fx), value=radius )
    K = lambda x0: tf.expand_dims( tf.exp( -tf.square(x0 - fx) / (2. * tf.square(radius)) ), axis=-1 )
    y_hat = lambda x0:  tf.reduce_sum( K(x0) * y, axis=0 ) / tf.reduce_sum( K(x0), axis=0 )
    return tf.map_fn( y_hat, x, dtype=dtype )

def interpolate1d(x, fx, y, method='linear', **kwargs):
    """
        Interpolation interface.

        Parameters:
            x (tensor-like): A 1-D tensor of shape [batch_size] containing values at which to evaluate f(x)
            fx (tensor-like): A 1-D tensor of shape [num_samples] containing sampled x values of interpolated function f
            y (tensor-like): A 2-D tensor of shape [num_samples, num_features] containing sampled values y=f(x) of interpolated function f

        Keyword Arguments:
            method (str): linear or rbf methods are supported

        Returns:
            tensor:
    """
    fx = tf.convert_to_tensor(fx)

    if method=='linear':
        return interpolate_linear(x, fx, y, **kwargs)
    elif method=='rbf':
        return interpolate_rbf(x, fx, y, **kwargs)
    else:
        raise ValueError("Unsupported method: %s" % (method))

@set_name_scope
def segment_softmax(X, segs, epsilon=1e-6):
    """
        Compute the softmax along segments of a tensor. This is implemented using a numerically stable solution.

        Parameters:
            X (tensor-like):     The input tensor to normalize
            segs (tensor-like):  A 1-D tensor of integer segment assignments

        Keyword Arguments:
            epsilon (float):     A constant factor used to prevent divide by zero error
    """
    mX = tf.gather(tf.segment_max(X, segs), segs)
    eX = tf.exp(X - mX)
    norm = tf.gather(tf.math.segment_sum(eX, segs), segs)
    return eX * tf.reciprocal(norm + epsilon)

def batch_normalize(inputs, **kwargs):
    """
        Apply batch normalization using tf.layers.batch_normalization, controlled by the models' phase indicator
    """
    return tf.layers.batch_normalization(inputs, training=current_tower().phase, **kwargs)

def dropout(inputs, rate=0.5, **kwargs):
    """
        Apply dropout using tf.nn.dropout only during training phase, correctly scales outputs for testing phase

        Parameters:
            inputs (tensor-like): An input tensor to be randomly dropped

        Keyword Arguments:
            rate (float): The dropout rate (default=0.5)
            **kwargs: Additional keyword arguments to be passed to tf.nn.dropout
    """
    phase=current_tower().phase
    rate = tf.where(phase, rate, 0.0)
    return tf.nn.dropout(inputs, rate, **kwargs)

def nan_replace(T, axis=0, strategy='mean'):
    """
        Replace nan values from a tensor with a value computed from the non-nan values

        Parameters:
            T (tensor-like):    An input tensor of any shape, possibly containing nans

        Keyword Arguments:
            axis (int):         The axis over which to compute the replacement strategy
            strategy (str):     The replacement strategy, currently only supports 'mean' (default='mean')
    """
    z = tf.zeros_like(T)
    nanT = tf.is_nan(T)
    filtT = tf.where(nanT, z, T)
    indicator = tf.cast(nanT, T.dtype)
    if strategy == 'mean':
        repl = tf.reduce_sum(filtT, axis=axis, keepdims=True) / tf.reduce_sum(1-indicator, axis=axis, keepdims=True)
        repl = tf.tile(repl, [tf.shape(T)[0], 1])
    return tf.where(nanT, repl, T)

def nan_filter(T, axis=1, partitions=None):
    """
        Filter nan values from a tensor and append an indicator tensor along the specified axis

        Parameters:
            T (tensor-like):    An input tensor of any shape, possibly containing nans

        Keyword Arguments:
            axis (int):         The axis on which to append the indicator variables
            partitions (list):  List of groups to create indicators, if None, treat
                                all columns independently (default=None)
    """

    if partitions is None:
        z = tf.zeros_like(T)
        nanT = tf.is_nan(T)
        filtered = tf.where(nanT, z, T)
        indicator = tf.cast(nanT, T.dtype)
        return tf.concat([ filtered, indicator ], axis=axis)
    else:
        Ts = tf.split(T, partitions, axis=axis)
        results = []
        for T in Ts:
            z = tf.zeros_like(T)
            nanT = tf.is_nan(T)
            filtered = tf.where(nanT, z, T)
            indicator = tf.greater(tf.reduce_sum(tf.cast(nanT, dtype=tf.int32), axis=axis, keepdims=True), 0)
            results += [filtered, tf.cast(indicator, dtype=T.dtype)]
        return tf.concat(results, axis=axis)

def nansum(T, axis=None):
    """
        Return a function to perform nan-safe sums on tensors

        Parameters:
            T: A tensor potentially containing nan values to use as a filter

        Keyword Arguments:
            axis (int): The reduction axis (default=None)

        Returns:
            lambda x: nansum(x)
    """
    nanT = tf.is_nan(T)
    def reduce_sum(X):
        z = tf.zeros_like(X)
        return tf.reduce_sum(tf.where(nanT, z, X), axis=axis)
    return reduce_sum

def nanmean(T, axis=None):
    """
        Return a function to perform nan-safe means on tensors

        Parameters:
            T: A tensor potentially containing nan values to use as a filter

        Keyword Arguments:
            axis (int): The reduction axis (default=None)

        Returns:
            lambda x: nanmean(x)
    """
    nanT = tf.is_nan(T)
    N = tf.reduce_sum(1-tf.cast(nanT, T.dtype), axis=axis)
    def reduce_mean(X):
        z = tf.zeros_like(X)
        return tf.reduce_sum(tf.where(nanT, z, X), axis=axis) / N
    return reduce_mean

def batch_duplicate(T, multiples, axis=0):
    """
        Duplicate slices of a tensor along a given axis. Given an input of shape [..., Daxis, ...], produces an output
        of shape [..., Daxis * multiples, ...]

        Parameters:
            T (tensor-like): An input tensor
            multiples (int): Number of copies of each slice to insert

        Keyword Arguments:
            axis (int): The axis along which to copy elements
    """
    dims = len(T.get_shape())
    shape = [v.value for v in T.get_shape()]

    T = tf.expand_dims(T, axis+1)
    tile = [1] * (dims+1)
    tile[axis+1]=multiples
    T = tf.tile(T, tile)
    new_shape = shape[:]
    new_shape[axis] = -1
    return tf.reshape(T, new_shape)

def _broadcast_matmul_left(A,B):
    s1 = tf.shape(A)
    i,j = s1[0], s1[1]
    s2 = tf.shape(B)
    _,l,m = s2[0], s2[1], s2[2]
    assertion = tf.Assert(tf.equal(j,l), ["Matrix dimensions must equal", j, l])

    with tf.control_dependencies([assertion]):
        B = tf.reshape(tf.transpose(B, (1,0,2)), (j, -1))
        AB = tf.matmul(A, B)
        return tf.transpose(tf.reshape(AB, (i,-1,m)), (1,0,2))

def _broadcast_matmul_right(A,B):
    s1 = tf.shape(A)
    _,j,k = s1[0], s1[1], s1[2]
    s2 = tf.shape(B)
    l,m = s2[0], s2[1]
    assertion = tf.Assert(tf.equal(k,l), ["Matrix dimensions must equal", k, l])

    with tf.control_dependencies([assertion]):
        A = tf.reshape(tf.transpose(A, (1,0,2)), (-1, k))
        AB = tf.matmul(A, B)
        return tf.transpose(tf.reshape(AB, (j,-1,m)), (1,0,2))

def broadcast_matmul(A,B):
    """
        Multiply 2-D matrices packed into a 3-D tensor by a single 2-D matrix

        This will perform a left or right multiply depending on which tensor is given for A or B.

        Parameters:
            A (tensor-like)
            B (tensor-like)

        Returns:
            Tensor
    """
    A = tf.convert_to_tensor(A)
    B = tf.convert_to_tensor(B)

    dims = (len(A.get_shape()), len(B.get_shape()))
    if dims == (2,3):
        return _broadcast_matmul_left(A,B)
    elif dims == (3,2):
        return _broadcast_matmul_right(A,B)
    else:
        raise Exception("batch_matmul expects one tensor of 2 dimensions and a second of 3 dimensions")

def batch_matmul(A,B):
    """
        Multiply 2-D matrices packed into a 3-D tensor by vectors packed into a 2-D matrix

        This will perform a left or right multiply depending on which tensor is given for A or B.

        Parameters:
            A (tensor-like): Shape (N, K, L)  or (N, K)
            B (tensor-like): Shape (N, L)     or (N, K, L)

        Returns:
            Tensor
    """
    A = tf.convert_to_tensor(A)
    B = tf.convert_to_tensor(B)
    dims = (len(A.get_shape()), len(B.get_shape()))
    if dims == (2,3):
        return tf.squeeze(tf.matmul(tf.expand_dims(A,1), B), axis=1)
    elif dims == (3,2):
        return tf.squeeze(tf.matmul(A, tf.expand_dims(B,2)), axis=2)
    else:
        raise Exception("batch_matmul expects one tensor of 2 dimensions and a second of 3 dimensions")

