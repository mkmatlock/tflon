from builtins import range
import numpy as np
import tensorflow as tf
from sklearn.metrics import roc_auc_score as get_roc_score
from itertools import product
from tflon import logging
from tflon.toolkit.toolkit import set_name_scope, get_next_name_scope

def concat_or_replace(A, B, axis):
    if len(A.shape)==0 or A.shape[axis]==0:
        return B
    Y = np.concatenate([A,B],axis)
    return Y

@set_name_scope
def accumulate_values(values, axis=None, init_len=0):
    """
        Accumulate tensor values by concatenating along the specified axis

        Parameters:
            values (tf.Tensor): A tensorflow op containing the next value to be added

        Keyword Arguments:
            axis (int):     If provided, concatenate along the specified axis. If None, reshape values to 1-D and concatenate along axis=0
            init_len (int): Specifies the initial size of the concatenation tensor, the tensor will contain init_len vectors of zeros along axis (default=0)

        Returns:
            accumulated, update_op: The tf.Tensor containing the concatenated tensors and an operation to update the concat.
    """
    if axis is None:
        shape = [init_len]
        axis = 0
        values = tf.reshape(values, (-1,))
    else:
        shape = [dim.value for dim in values.get_shape()]
        shape = shape[:axis] + [init_len] + shape[axis+1:]
    accumulated = tf.Variable(np.zeros(shape), dtype=values.dtype,
                              collections=[tf.GraphKeys.METRIC_VARIABLES], trainable=False,
                              name=get_next_name_scope("accumulator"), validate_shape=False)
    concat_op = tf.numpy_function(lambda a,v: concat_or_replace(a,v,axis), (accumulated, values), values.dtype)
    update_op = tf.assign(accumulated, concat_op, validate_shape=False)
    return accumulated, update_op

def _accumulate_average(values, nans, axis, nan_values=None):
    if nans:
        if nan_values is None:
            nan_values = tf.is_nan(values)
        z = tf.zeros_like(values)

        N = tf.reduce_sum(tf.where(nan_values, z, tf.ones_like(values)), axis=axis, keepdims=True)
        S = tf.reduce_sum(tf.where(nan_values, z, values), axis=axis, keepdims=True)
    else:
        N = tf.reduce_sum(tf.ones_like(values), axis=axis, keepdims=True)
        S = tf.reduce_sum(values, axis=axis, keepdims=True)

    sum_accumulator, sum_update = accumulate_values(S, axis, init_len=1)
    num_accumulator, num_update = accumulate_values(N, axis, init_len=1)

    avg = tf.reduce_sum(sum_accumulator, axis=axis)/tf.reduce_sum(num_accumulator, axis=axis)
    return avg, tf.group(sum_update, num_update)

@set_name_scope
def pearson(targets, predictions, nans=False, axis=None):
    """
        Compute the pearson correlation

        Parameters:
            targets (tensor-like):     N-D tensor containing the target data
            predictions (tensor-like): N-D tensor of the same shape as targets containing estimated values

        Keyword Arguments:
            axis (int):  If provided, calculate the average over the specified axis (default=all)
            nans (bool): If True, nans in tensor T will be filtered prior to auc calculations

        Returns:
            Tensor: The average z-score
    """
    nanT = tf.is_nan(targets) if nans else None

    EXY, upEXY = _accumulate_average(targets*predictions, nans, axis, nan_values=nanT)
    EX, upEX = _accumulate_average(targets, nans, axis, nan_values=nanT)
    EY, upEY = _accumulate_average(predictions, nans, axis, nan_values=nanT)

    EX2, upEX2 = _accumulate_average(targets*targets, nans, axis, nan_values=nanT)
    EY2, upEY2 = _accumulate_average(predictions*predictions, nans, axis, nan_values=nanT)

    return (EXY - EX*EY) / tf.sqrt( (EX2 - EX*EX) * (EY2 - EY*EY) ),  tf.group([upEXY, upEX, upEY, upEX2, upEY2])

@set_name_scope
def average_z_score(targets, mu, sigma, nans=False, axis=None):
    """
        Compute the average Z-score of a gaussian regression

        Parameters:
            targets (tensor-like):   N-D tensor containing the target data
            mu (tensor-like):        N-D tensor of the same shape as targets containing estimated distribution means
            sigma (tensor-like):     N-D tensor of the same shape as targets containing estimated distribution standard deviations

        Keyword Arguments:
            axis (int):  If provided, calculate the average over the specified axis (default=all)
            nans (bool): If True, nans in tensor T will be filtered prior to auc calculations

        Returns:
            Tensor: The average z-score
    """
    return _accumulate_average(tf.abs((targets-mu)/sigma), nans, axis)

def roc_auc_score(T, L):
    if np.min(T)==np.max(T):
        logging.warn("Only one class present in y_true. ROC AUC score is not defined in that case.")
        return np.nan
    return get_roc_score(T, L)

def _nansafe_auc(T, L):
    selector = np.logical_not(np.isnan(T))
    if np.sum(selector)==0:
        return np.nan
    return roc_auc_score(T[selector], L[selector])

def _auc_impl(true, logits, axis, nans):
    switch = lambda T, L: _nansafe_auc(T, L) if nans else roc_auc_score(T, L)
    if axis is None:
        return np.array(switch(true, logits), dtype=logits.dtype)
    else:
        aucs = np.zeros(true.shape[:axis] + true.shape[axis+1:], dtype=logits.dtype)
        for index in product(*[range(s) for s in aucs.shape]):
            slc = index[:axis] + (slice(None),) + index[axis:]
            aucs[index] = switch(true[slc], logits[slc])
        return aucs

@set_name_scope
def auc(T, L, axis=None, nans=False):
    """
        Return a tf.numpy_function which wraps sklearn.metrics.roc_auc_score.
        Optionally, handle nan values.

        Parameters:
            T (tensor-like): A 2-D tensor of shape batch x classes containing binary class labels
            L (tensor-like): A 2-D tensor with the same shape as T containing logit scores for each class

        Keyword Arguments:
            axis (int):  If provided, calculate the auc over the specified axis (default=all)
            nans (bool): If True, nans in tensor T will be filtered prior to auc calculations

        Returns:
            A 0-D or 1-D tensor of AUC values
    """

    T_acc, T_up = accumulate_values(T, axis)
    L_acc, L_up = accumulate_values(L, axis)
    return tf.numpy_function(lambda t, l: _auc_impl(t, l, axis, nans), [T_acc, L_acc], L.dtype), tf.group(T_up, L_up)


def average_auc(T, L, axis=1, nans=True):
    auc_cals = tf.numpy_function(lambda t, l: _auc_impl(t, l, axis, nans), [T, L], L.dtype)
    auc_cals.set_shape([None, 1])
    auc_acc, auc_up = accumulate_values(auc_cals, 0)

    def _mask_avg(a):
        avg = np.nanmean(a)
        return avg.astype(np.float64)

    return tf.numpy_function(_mask_avg, [auc_acc], np.float64), auc_up


def _segment_average_auc_impl(T, L, segments, axis, nans):
    mi = max(segments)+1

    if axis is None:
        aucs = np.zeros((mi,), dtype = L.dtype)
    else:
        aucs = np.zeros(tuple(mi if j==axis else dim for j, dim in enumerate(T.shape)), dtype = L.dtype)

    for i in range(mi):
        if axis is None:
            where = segments==i
            auc_index = i
        else:
            where = tuple(segments==i if j==axis else slice(None) for j in range(len(T.shape)))
            auc_index = tuple(i if j==axis else slice(None) for j in range(len(T.shape)))

        aucs[auc_index] = _auc_impl(T[where], L[where], axis, nans)

    return aucs

@set_name_scope
def segment_average_auc(T, L, segments, axis=None, nans=False):
    """
        Return a tf.numpy_function which computes the average auc over segments of two tensors. Handles nans.

        Parameters:
            T (tensor-like): A 2-D tensor of shape batch x classes containing binary class labels
            L (tensor-like): A 2-D tensor with the same shape as T containing logit scores for each class
            segments (tensor-like): A 1-D tensor of segment indices of type tf.int32, with the same first dimension size of T and L

        Keyword Arguments:
            axis (int):  If provided, calculate the auc over the specified axis (default=all)
            nans (bool): If True, nans in tensor T will be filtered prior to auc calculations

        Returns:
            A 0-D or 1-D tensor of average AUC values over segments of the input
    """

    auc_calc = tf.numpy_function(lambda t,l,s: _segment_average_auc_impl(t, l, s, axis, nans), [T, L, segments], L.dtype)
    auc_calc.set_shape([None if (j==axis or j==0 and axis is None) else dim.value for j,dim in enumerate(T.get_shape())])
    auc_acc, auc_up = accumulate_values(auc_calc, axis)

    def mask_avg(a):
        s = np.isnan(a)
        a[s] = 0
        N = np.sum(~s, axis=axis)
        avg = np.sum(a, axis=axis) / N
        if axis is None and N==0:
            avg = np.nan
        elif axis is not None:
            avg[N==0] = np.nan
        return avg.astype(a.dtype)

    return tf.numpy_function(mask_avg, [auc_acc], L.dtype), auc_up

@set_name_scope
def mean_absolute_error(targets, predictions, nans=False, axis=None):
    """
        Compute the mean absolute error

        Parameters:
            targets (tensor-like):     N-D tensor containing the target data
            predictions (tensor-like): N-D tensor of the same shape as targets containing estimated values

        Keyword Arguments:
            axis (int):  If provided, calculate the average over the specified axis (default=all)
            nans (bool): If True, nans in tensor T will be filtered prior to auc calculations

        Returns:
            Tensor: The average z-score
    """
    nanT = tf.is_nan(targets) if nans else None
    return _accumulate_average(tf.abs(targets-predictions), nans, axis, nan_values=nanT)
