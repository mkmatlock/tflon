import tensorflow as tf
from tflon.data import TensorQueue, RaggedTensor, SparseTensor, RaggedTensorValue, TensorTransform
from tflon.toolkit import build_tower
from tflon.data import Schema, Table, SchemaEntry
from tflon import logging
from collections import OrderedDict
import re

def merge(*lists):
    seen = set()
    M = []
    for L in lists:
        for item in L:
            if item.name in seen: continue
            M.append(item)
            seen.add(item.name)
    return M

def subtract(varlist, remove):
    seen = {item.name for item in remove}
    return filter(lambda v: v.name not in seen, varlist)

class Preprocessor(object):
    def __init__(self, schema):
        self.slots = set(schema.keys())
        self.schema = {k: SchemaEntry(k, ttype, required=False, visible=False) for k, ttype in schema.items()}

    def _preprocess(self, data):
        raise NotImplementedError()

    def __call__(self, batch):
        schema = self.schema
        update = self._preprocess(batch)
        return {k:schema[k].instance(update[k]) for k in update}

class Featurizer(object):
    """
        Featurizer is a serializable class containing all data preprocessors and toolkit.featurizers constructed during model.build
    """

    def __init__(self, preprocessors=[], featurizers=[]):
        self._preprocessors = preprocessors
        self._featurizers = featurizers

    def __call__(self, batch):
        """
            Add each module's features to the batch

            Parameters:
                batch (dict-like): A batch dictionary containing data tables
        """
        for preprocessor in self._preprocessors:
            slots = preprocessor.slots
            update = preprocessor(batch)
            new_keys = set(update.keys())

            assert len(slots-new_keys)==0, "Preprocessor did not return data for slots: %s" % (','.join(slots - new_keys))
            assert len(new_keys-slots)==0, "Preprocessor returned keys for unknown slots: %s" % (','.join(new_keys - slots))
            for k in new_keys:
                batch[k] = update[k]

        for featurizer in self._featurizers:
            namespace = featurizer.namespace
            slots = featurizer.slots
            if len(slots)==0 or all(name in batch for name in slots): continue

            update = featurizer( batch )
            new_keys = set(update.keys())

            assert len(slots-new_keys)==0, "Featurizer did not return data for slots: %s" % (','.join(slots - new_keys))
            assert len(new_keys-slots)==0, "Featurizer returned keys for unknown slots: %s" % (','.join(new_keys - slots))
            for k in new_keys:
                if k in batch:
                    raise KeyError("%s tried to overwrite existing key %s in batch" % (namespace, k))
                batch[k] = update[k]
        return batch

class Tower(object):
    """
        Tower is a slave class of the Model, which is designed to specifically abstract away the tensorflow interface of tflon.model.Model.

        The combination of Tower and Model are effectively a hybrid class, since Model exposes its Tower instance methods directly via python missing
        property resolution (e.g __getattribute__).

        The end user interacts with these methods and properties from within their Model instance as though they were attributes of Model.
    """
    def __init__(self, params=None, use_gpu=False):
        self.parent_scope = tf.get_variable_scope().name
        self._params = dict() if params is None else params
        self._phase = tf.get_variable("phase", shape=[], dtype=tf.bool, initializer=tf.constant_initializer(False), collections=[tf.GraphKeys.GLOBAL_VARIABLES], trainable=False)

        self._op_device = "" if use_gpu else "/cpu"
        self._var_device = "" if use_gpu else "/cpu"

        self._schema = {}
        self._tables = {}
        self._inputs = {}
        self._targets = {}
        self._placeholders = {}
        self._outputs = {}
        self._transforms = {}
        self._losses = {}
        self._metrics = {}
        self._gradients = None
        self._queue = None
        self._modules = OrderedDict()
        self._used_params = set()
        self._suppress_warnings = False

        self._weights = None
        self._biases = None
        self._assign_ops = None
        self._metric_vars = None
        self._trainables = None
        self._variables = None
        self._featurizer = None
        self._preprocessors = []

        self.set_parameter_default('weight_initializer', tf.contrib.layers.xavier_initializer())
        self.set_parameter_default('weight_prior', lambda x: (x, None))
        self.set_parameter_default('bias_initializer', tf.constant_initializer(0.1))
        self.set_parameter_default('bias_prior', lambda x: (x, None))

    def suppress_warnings(self):
        self._suppress_warnings = True

    @property
    def phase(self):
        """
            Boolean tensorflow variable specifying the current phase:

                * True = training phase
                * False = testing phase
        """
        return self._phase

    def set_training_phase(self, value):
        """
            Set the current phase. This is used by Model.infer, Model.fit and Model.evaluate

            Values are:

                * True = training phase
                * False = testing phase

            Parameters:
                value (bool): the new value of the phase variable
        """
        self._phase.load(value)

    @property
    def parameters(self):
        """
            Return the parameter dictionary. This combines user-specified parameters, defaults in Model._parameters and all Module._parameters
        """
        return self._params

    def has_parameter(self, key, default=None, namespace=None):
        """
            Check if a parameter exists and is not None

            Parameters:
                key (str):      The parameter name

            Keyword Arguments:
                namespace(str): An optional namespace, searches for namespace/key instead of key (default=None)
        """
        if namespace is not None:
            key = "%s/%s" % (namespace, key)
        return self._params.get(key, default) is not None

    def get_parameter(self, key, default=None, namespace=None):
        """
            Get the value of a parameter if it exists and is not None

            Parameters:
                key (str):      The parameter name

            Keyword Arguments:
                namespace(str): An optional namespace, searches for namespace/key instead of key (default=None)

            Returns:
                The value of the parameter

            Raises:
                KeyError: if the parameter key is not found or if its value is None
        """
        if namespace is not None:
            key = "%s/%s" % (namespace, key)
        value = self._params.get(key, default)
        self._used_params.add(key)
        if value is None:
            raise KeyError("Key not set and no default: %s" % (key))
        return value

    def set_parameter(self, key, value, namespace=None):
        """
            Set the value of a parameter

            Parameters:
                key (str):      The parameter name
                value:          A parameter value (any dill-pickleable type is valid)

            Keyword Arguments:
                namespace(str): An optional namespace, searches for namespace/key instead of key (default=None)
        """

        if namespace is not None:
            key = "%s/%s" % (namespace, key)
        assert key not in self._params, "Cannot override preset parameter '%s'" % (key)
        self._params[key] = value

    def set_parameter_default(self, key, value, namespace=None):
        """
            Set the value of a parameter if it is not already set

            Parameters:
                key (str):      The parameter name
                value:          A parameter value (any dill-pickleable type is valid)

            Keyword Arguments:
                namespace(str): An optional namespace, searches for namespace/key instead of key (default=None)
        """

        if namespace is not None:
            key = "%s/%s" % (namespace, key)
        if key not in self._params or self._params[key] is None:
            self._params[key] = value

    def inject_parameters(self):
        """
            Add the values of Model._parameters as class attributes. For a given (key, value) pair, this will add instance attributes of the form:

                * self.key = value

            This behavior is currently optional, but will eventually become the default.
        """
        def create_property(key):
            return property(lambda self: self.get_parameter(key), lambda self,v: self.set_parameter(key,v))

        for k in self._params:
            if '/' in k: continue
            # assert not hasattr(self, k), "Injected parameter %s overrides an existing property" % (k)
            setattr(self.__class__, k, create_property(k))

    def add_module( self, module, extra_parameters ):
        """
            Add a module to this tower. This is automatically handled by the Module constructor.

            Parameters:
                module (tflon.toolkit.Module):  Instance of Module
                extra_parameters (dict):        A dictionary of parameter overrides for the module
        """
        self._modules[module.name] = module

        for k, v in extra_parameters.items():
            k = "%s/%s" % (module.name, k)
            if v is not None:
                self.parameters.setdefault(k, v)

    def get_module( self, name ):
        """
            Get a module by name

            Parameters:
                name (str): The module name
        """
        return self._modules[name]

    @property
    def modules( self ):
        """
            Get a copy of the modules dictionary, which consists of (module.name, module) pairs.
        """
        return dict(self._modules.items())

    def show_data( self, batch ):
        """
            Show table data to each module attached to this tower. Used by Model.fit to set training parameters (e.g data normalization)

            Parameters:
                batch (dict-like): A batch dictionary containing data tables
        """
        for name in self._modules:
            self._modules[name].show_data( batch )

    @property
    def schema( self ):
        """
            Return a tflon.data.Schema object implementing the model-specified schema (see Model._schema)
        """
        nschema = self._schema.copy()
        return Schema(nschema)

    @property
    def featurizer( self ):
        if self._featurizer is None:
            featurizers = [self._modules[k].featurizer for k in self._modules]
            featurizers = [f for f in featurizers if f is not None]
            self._featurizer = Featurizer(self._preprocessors + featurizers)
        return self._featurizer

    def _initialize_build( self ):
        self.inject_parameters()
        self._index_placeholder = tf.placeholder(dtype=tf.string, shape=(None,), name="batch_index")
        self._placeholders['_index'] = self._index_placeholder

    def _finalize_build( self ):
        if 'priors' in self._losses:
            self._losses['priors'] = tf.add_n(self._losses['priors'])
        self.set_loss(*self._losses.keys())
        if not self._suppress_warnings:
            self._perform_checks()
        self._finalize()

    def populate( self, build_fn ):
        """
            Bootstrap a tensorflow graph and perform sanity checks:

                1. Inject parameters into the tower object attributes
                2. Create the tensorflow ops associated with Model by calling build_fn.
                3. Add additional ops joining all losses, or add a no_op if no losses
                4. Perform sanity checks and emit warnings for missing elements (e.g no inputs, no losses)
                5. Check for unused parameter values (often caused by a typo in a parameter name)

            Parameters:
                build_fn: A reference to Model._model()
        """
        with build_tower(self), tf.device( self._op_device ):
            build_fn()

    def _finalize(self):
        self._weights = tf.get_collection( tf.GraphKeys.WEIGHTS, scope=self.parent_scope )
        self._biases = tf.get_collection( tf.GraphKeys.BIASES, scope=self.parent_scope )
        self._metric_vars = tf.get_collection( tf.GraphKeys.METRIC_VARIABLES, scope=self.parent_scope )
        self._trainables = tf.get_collection( tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.parent_scope )
        globals = tf.get_collection( tf.GraphKeys.GLOBAL_VARIABLES, scope=self.parent_scope )
        locals = tf.get_collection( tf.GraphKeys.LOCAL_VARIABLES, scope=self.parent_scope )
        self._variables = merge(globals, locals, self._trainables)
        def name_placeholder(T):
            name,_ = T.name.split(':')
            if name.startswith(self.parent_scope):
                name = name[len(self.parent_scope):]
            if name.startswith('/'):
                name = name[1:]
            return "%s_assignment_placeholder" % (name)
        self._variable_placeholders = {T.name:tf.placeholder(shape=T.get_shape(), dtype=T.dtype, name=name_placeholder(T)) for T in self._variables}
        self._assignment_ops = {T.name:T.assign(self._variable_placeholders[T.name], name='assign_%s' % (T.name.replace(':','_'))) for T in self._variables}

    def _perform_checks( self ):
        if len(self._inputs)==0:
            logging.warn("No inputs defined for model %s", self.parent_scope)
        if len(self._outputs)==0:
            logging.warn("No outputs defined for model %s", self.parent_scope)
        if len(self._targets)==0:
            logging.warn("No targets defined for model %s", self.parent_scope)
        if len(self._losses)==0:
            logging.warn("No losses defined for model %s", self.parent_scope)
        for name, module in self._modules.items():
            if not module.initialized:
                logging.warn("Module instantiated but not called: %s", name)

        ignore_missing_usage = {'weight_initializer', 'weight_prior', 'bias_initializer', 'bias_prior'}
        for k in self._params.keys():
            if k not in self._used_params and k.rsplit('/', 1)[-1] not in ignore_missing_usage:
                logging.warn("Parameter setting %s was not used by any module", k)

    def assign( self, session, variables, values ):
        fd = {self._variable_placeholders[w.name]:v for w, v in zip(variables, values)}
        ops = [self._assignment_ops[v.name] for v in variables]
        session.run(ops, feed_dict=fd)

    def get_variable( self, name, shape, dtype=tf.float32, initializer=tf.zeros_initializer(), trainable=False ):
        """
            Get a tensorflow variable. Automatically selects the appropriate compute device.

            Parameters:
                name (str):            The parameter name, which will result in a tensorflow variable referenced at scope/name:x
                shape (list or tuple): The explicit shape of this variable

            Keyword Parameters:
                dtype (tf.dtype):           The data type (default=tf.float32)
                initializer (tf.Operation): An initializer op (default=tf.zeros_initializer())
                trainable (bool):           Whether to add to collection tf.GraphKeys.TRAINABLE_VARIABLES
        """
        with tf.device(self._var_device):
            return tf.get_variable( name, shape=shape, dtype=dtype, initializer=initializer, collections=[tf.GraphKeys.LOCAL_VARIABLES], trainable=trainable )

    def _get_weight_initializer(self, scope=None):
        param = 'weight_initializer'
        if scope is not None: param = '%s/weight_initializer' % (scope)
        if param not in self._params: param = 'weight_initializer'
        return self.get_parameter(param)

    def _get_weight_prior(self, scope=None):
        param = 'weight_prior'
        if scope is not None: param = '%s/weight_prior' % (scope)
        if param not in self._params: param = 'weight_prior'
        return self.get_parameter(param)

    def _get_bias_initializer(self, scope=None):
        param = 'bias_initializer'
        if scope is not None: param = '%s/bias_initializer' % (scope)
        if param not in self._params: param = 'bias_initializer'
        return self.get_parameter(param)

    def _get_bias_prior(self, scope=None):
        param = 'bias_prior'
        if scope is not None: param = '%s/bias_prior' % (scope)
        if param not in self._params: param = 'bias_prior'
        return self.get_parameter(param)

    def get_weight( self, name, shape, dtype=tf.float32, initializer=None, prior=None, scope=None ):
        """
            Get a weight variable. Weights are automatically added to tf.GraphKeys.TRAINABLE_VARIABLES, tf.GraphKeys.LOCAL_VARIABLES and tf.GraphKeys.WEIGHTS

            Parameters:
                name (str):            The parameter name, which will result in a tensorflow variable referenced at scope/name:x
                shape (list or tuple): The explicit shape of this variable

            Keyword Parameters:
                dtype (tf.dtype):           The data type (default=tf.float32)
                initializer (tf.Operation): An initializer op (default=None, which gets this scope's default initializer)
                scope (str):                A scope for this var, used to select the appropriate default initializer for a module
        """
        if initializer is None: initializer = self._get_weight_initializer(scope)
        if prior is None: prior = self._get_weight_prior(scope)
        with tf.device(self._var_device):
            w = tf.get_variable( name, shape=shape, dtype=dtype, initializer=initializer, collections=[tf.GraphKeys.LOCAL_VARIABLES, tf.GraphKeys.WEIGHTS], trainable=True )
            w, l = prior(w)
            if l is not None: self.add_prior(l)
            return w

    def get_bias( self, name, shape, dtype=tf.float32, initializer=None, prior=None, scope=None ):
        """
            Get a bias variable. Biases are automatically added to tf.GraphKeys.TRAINABLE_VARIABLES, tf.GraphKeys.LOCAL_VARIABLES and tf.GraphKeys.BIASES

            Parameters:
                name (str):            The parameter name, which will result in a tensorflow variable referenced at scope/name:x
                shape (list or tuple): The explicit shape of this variable

            Keyword Parameters:
                dtype (tf.dtype):           The data type (default=tf.float32)
                initializer (tf.Operation): An initializer op (default=None, which gets this scope's default initializer)
                scope (str):                A scope for this var, used to select the appropriate default initializer for a module
        """
        if initializer is None: initializer = self._get_bias_initializer(scope)
        if prior is None: prior = self._get_bias_prior(scope)
        with tf.device(self._var_device):
            b = tf.get_variable( name, shape=shape, dtype=dtype, initializer=initializer, collections=[tf.GraphKeys.LOCAL_VARIABLES, tf.GraphKeys.BIASES], trainable=True )
            b, l = prior(b)
            if l is not None: self.add_prior(l)
            return b

    def _get_placeholder(self, name, shape, dtype, sparse, ragged):
        if sparse:
            sparse_dims = len(shape)
            infered_shape = tf.TensorShape(shape)
            indices = tf.placeholder(dtype=tf.int64, shape=(None, sparse_dims), name=name+"_indices")
            values = tf.placeholder(dtype=dtype, shape=(None,), name=name+"_values")
            shape = tf.placeholder(dtype=tf.int64, shape=(sparse_dims,), name=name+"_shape")

            self._placeholders['%s_indices' % (name)] = indices
            self._placeholders['%s_values' % (name)] = values
            self._placeholders['%s_shape' % (name)] = shape

            tensor = SparseTensor(indices, values, shape, infered_shape)
            pl = (indices, values, shape)
        elif ragged:
            dense_shape = tf.TensorShape(shape)
            values = tf.placeholder(dtype=dtype, shape=(None,), name=name+"_values")
            lengths = tf.placeholder(dtype=tf.int64, shape=(shape[-1],), name=name+"_lengths")
            self._placeholders['%s_values' % (name)] = values
            self._placeholders['%s_lengths' % (name)] = lengths

            tensor = RaggedTensor(values, lengths, dense_shape)
            pl = (values, lengths)
        else:
            tensor = tf.placeholder(dtype=dtype, shape=shape, name=name)
            self._placeholders[name] = tensor
            pl = tensor
        return tensor, pl

    def add_preprocessor(self, featurizer):
        self._preprocessors.append(featurizer)
        self._schema.update(featurizer.schema)

    def add_table(self, name, ttype=Table, required=True, namespace=None):
        fullname = name
        if namespace is not None:
            fullname = '%s/%s' % (namespace, name)
        assert fullname not in self._tables, "Table '%s' already exists" % (fullname)
        self._tables[fullname] = ttype
        self._schema[fullname] = SchemaEntry(fullname, ttype, required=required, visible=namespace is None)
        return fullname

    def add_input(self, name, shape=[None], dtype=tf.float32, sparse=False, ragged=False, ttype=Table, required=True, namespace=None):
        """
            Add an input tensor. This method creates a dictionary alias name which can be used when feeding data to the model.

            Input tensors can come in three varieties: dense (default), sparse, and ragged.

            Parameters:
                name (str): The name reference for this tensor.

            Keyword Parameters:
                shape (iterable): The dense shape of this input tensor
                dtype (tf.type):  The data type (e.g tf.float32)
                sparse (bool):    Specify sparse tensor, sparse tensors are input via tf.SparseTensorValue
                ragged (bool):    Specify a ragged array-type tensor, ragged tensor are input via tflon.data.RaggedTensorValue

            Returns:
                tf.Tensor: The input placeholder tensor
        """
        fullname = name
        if namespace is not None:
            fullname = '%s/%s' % (namespace, name)
        assert fullname not in self._inputs, "Input '%s' already exists" % (fullname)
        inp, pl = self._get_placeholder(name, shape, dtype, sparse, ragged)
        self._inputs[fullname] = pl
        self._schema[fullname] = SchemaEntry(fullname, ttype, required=required, visible=namespace is None)
        return inp

    def add_target(self, name, shape=[None], dtype=tf.float32, sparse=False, ragged=False, ttype=Table, required=False, namespace=None):
        """
            Alias of Tower.add_input, differs only by semantic distinction.
        """
        fullname = name
        if namespace is not None:
            fullname = '%s/%s' % (namespace, name)
        assert fullname not in self._targets, "Target '%s' already exists" % (fullname)
        tar, pl = self._get_placeholder(name, shape, dtype, sparse, ragged)
        self._targets[fullname] = pl
        self._schema[fullname] = SchemaEntry(fullname, ttype, required=required, visible=namespace is None)
        return tar

    def add_output(self, name, out, transform=None):
        """
            Add an output tensor. Output tensors are returned by Model.infer

            Optionally, output tensors can be transformed by a tflon.data.TensorTransform post-processing operation.

            Parameters:
                name (str):        A name for this output, used as a reference key in the dictionary returned by Model.infer
                out (tensor-like): A tensor to fetch when writing output

            Keyword Parameters:
                transform (TensorTransform): A transformation to apply (default=None)

        """
        assert transform is None or isinstance(transform, TensorTransform), "Argument transform must have type TensorTransform"
        assert name not in self._outputs, "Output '%s' already exists" % (name)
        self._outputs[name] = out
        self._transforms[name] = transform
        return out

    def add_loss(self, name, loss):
        """
            Add a named loss function to this model. All losses are summed to compute the final optimization target

            Parameters:
                name (str): A name reference for this loss value
                loss (tensor-like): The op calculating this loss
        """

        assert name not in self._losses, "Loss '%s' already exists" % (name)
        self._losses[name] = loss

    def add_prior(self, loss):
        """
            Add a prior loss to this model.

            Parameters:
                loss (tensor-like): The op calculating this loss
        """
        self._losses.setdefault('priors', []).append(loss)

    def add_metric(self, name, metric):
        """
            Add a named metric op to this model.

            Parameters:
                name (str): A name reference for this metric
                metric (tuple): The op calculating this metric. This op should be able to handle aggregation of multiple batches.
                                Tuples must have the form (eval_op, update_op), where eval_op computes the final metric value after multiple
                                calls to update_op.
        """
        assert name not in self._metrics, "Metric '%s' already exists" % (name)
        self._metrics[name] = metric

    @property
    def name(self):
        """
            Get the name scope of the tower
        """
        return self.parent_scope

    @property
    def tables(self):
        return dict(self._tables.items())

    @property
    def index(self):
        """
            Get the placeholder for batch indices
        """
        return self._index_placeholder

    @property
    def inputs(self):
        """
            Get the input dictionary, (name, tf.Tensor) pairs. This does not necessarily contain feedable placeholders
        """
        return dict(self._inputs.items())

    @property
    def outputs(self):
        """
            Get the output dictionary, (name, tf.Tensor) pairs.
        """
        return dict(self._outputs.items())

    @property
    def transforms(self):
        """
            Get the transform dictionary, (name, TensorTransform) pairs.
        """
        return dict(self._transforms.items())

    @property
    def targets(self):
        """
            Get the target dictionary, (name, tf.Tensor) pairs. This does not necessarily contain feedable placeholders
        """
        return dict(self._targets.items())

    @property
    def inputs_and_targets(self):
        """
            Get the joint dictionary of all inputs and targets
        """
        it = self._inputs.copy()
        it.update(self._targets)
        return it

    @property
    def placeholders(self):
        """
            Get a dictionary of feedable placeholders. This will contain all inputs and targets, as well as support tensors for sparse and ragged inputs
        """
        return dict(self._placeholders.items())

    @property
    def placeholder_names(self):
        """
            Get a dictionary of feedable placeholders to names. This will contain all inputs and targets, as well as support tensors for sparse and ragged inputs
        """
        return {v:k for k,v in self._placeholders.items()}

    @property
    def losses(self):
        """
            Get a dictionary of all losses (name, tf.Tensor) pairs.
        """
        return dict(self._losses.items())

    @property
    def metrics(self):
        """
            Get a dictionary of all metrics (name, (eval_op, update_op)) pairs.
        """
        return dict(self._metrics.items())

    @property
    def weights(self):
        """
            Get the collection of all weights for this model
        """
        if self._weights is None:
            return tf.get_collection( tf.GraphKeys.WEIGHTS, scope=self.parent_scope )
        return self._weights

    @property
    def biases(self):
        """
            Get the collection of all biases for this model
        """
        if self._biases is None:
            return tf.get_collection( tf.GraphKeys.BIASES, scope=self.parent_scope )
        return self._biases


    @property
    def trainables(self):
        """
            Get the collection of all trainables (weights + biases + other trainables) for this model
        """
        if self._trainables is None:
            return tf.get_collection( tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.parent_scope )
        return self._trainables

    @property
    def all_variables(self):
        globals = tf.get_collection( tf.GraphKeys.GLOBAL_VARIABLES, scope=self.parent_scope )
        locals = tf.get_collection( tf.GraphKeys.LOCAL_VARIABLES, scope=self.parent_scope )
        trainables = tf.get_collection( tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.parent_scope )
        return merge(globals, locals, trainables)

    @property
    def variables(self):
        """
            Get the complete collection of all variables for this model (weights + biases + other trainbles + non-trainables)
        """
        if self._variables is None:
            return self.all_variables
        return self._variables

    @property
    def metric_variables(self):
        """
            Get all variables associated with metric update operations
        """
        if self._metric_vars is None:
            return tf.get_collection( tf.GraphKeys.METRIC_VARIABLES, scope=self.parent_scope )
        return self._metric_vars

    @property
    def vardict(self):
        """
            Get a dictionary of all variables belonging to this tower
        """
        return {v.name:v for v in self.variables}

    def find_variables(self, pattern, case_sensitive=False):
        regex = re.compile(pattern, flags=re.IGNORECASE if not case_sensitive else 0)
        varlist = [(v.name,v) for v in self.variables]
        if not case_sensitive:
            varlist = [(vname.lower(),v) for vname,v in varlist]
        matches = [v for vname, v in varlist if regex.search(vname) is not None]

        if len(matches)==0:
            raise KeyError("No variables matching '%s'" % (pattern))
        return matches

    @property
    def loss(self):
        """
            Get the aggregated loss op for this tower. Constructed as a sum of all named losses.
        """
        return self._loss_op

    def set_loss(self, *keys):
        """
            Set the total loss op (Tower.loss) to the sum of the given named loss ops
        """
        if len(keys)==0:
            self._loss_op = tf.no_op()
        elif len(keys)==1:
            k, = keys
            self._loss_op = self._losses[k]
        else:
            self._loss_op = tf.add_n([self._losses[k] for k in keys])

    @property
    def gradients(self):
        """
            Return the gradients for all trainable variables.

            Returns:
                grads (list):               Tower gradients
        """
        if self._gradients is None:
            tvars = self.trainables
            grads = list(zip( tf.gradients( self.loss, tvars ), tvars ))
            self.set_gradients(grads)
        return self._gradients

    def set_gradients(self, grads):
        """
            Set the gradients for trainable variables in this tower.

            This is used to override the gradients property for use with optimizer-specific gradients.
        """
        for G, V in grads:
            if G is None:
                logging.warn("No gradient defined for variable: %s" % (V.name))
        self._gradients = grads

    def get_queue(self, **kwargs):
        """
            Construct a data queue for this tower, which can be used to pre-cache mini-batch data on the GPU in parallel.
        """
        if self._queue is None:
            kwargs['name'] = "TensorQueue"
            self._queue = TensorQueue( self, device=self._op_device, **kwargs )
        return self._queue

    def feed(self, batch):
        """
            Given an input batch, construct a feed dictionary. This method handles mapping tables to support tensors for sparse and ragged inputs.

            Returns:
                dict: of (placeholder, value) pairs, a valid feed_dict
        """
        placeholders = self._inputs.copy()
        placeholders.update(self._targets)
        placeholders['_index'] = self._index_placeholder
        feed_dict = {}

        for k, v in batch.items():
            if k not in placeholders: continue
            pl = placeholders[k]

            if type(v) is RaggedTensorValue:
                feed_dict[pl[0]] = v.values
                feed_dict[pl[1]] = v.lengths
            elif type(v) is tf.SparseTensorValue:
                feed_dict[pl[0]] = v.indices
                feed_dict[pl[1]] = v.values
                feed_dict[pl[2]] = v.dense_shape
            else:
                feed_dict[pl] = v

        return feed_dict

    @property
    def feedables(self):
        return {'_index'} | set(self._inputs.keys()) | set(self._targets.keys())

    def feed_names(self, batch):
        """
            Given an input batch, construct a list of the names of all placeholder tensors required to feed data to the model. This will include
            names of support tensors for sparse and ragged inputs.

            Returns:
                list: A list of placeholder names
        """
        placeholders = dict(self._inputs.items() + self._targets.items())

        feed_names = []
        for k in sorted(batch.keys()):
            if k not in placeholders: continue
#            pl = placeholders[k]
            v = batch[k]
            if type(v) is tf.SparseTensorValue:
                feed_names += ['%s_indices' % (k), '%s_values' % (k), '%s_shape' % (k)]
            elif type(v) is RaggedTensorValue:
                feed_names += ['%s_values' % (k), '%s_lengths' % (k)]
            else:
                feed_names += [k]
        return feed_names

