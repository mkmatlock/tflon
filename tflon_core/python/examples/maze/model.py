from __future__ import division
import tflon
import tensorflow as tf

class Wave(tflon.Model):
    def _model(self):
        graphs = self.add_table('graphs')
        nodes = self.add_input('nodes', shape=[None, 2])
        size = self.add_input('size', shape=[], dtype=tf.int32)
        solution = self.add_target('solution', shape=[None,1])

        cell_factory = lambda: tf.nn.rnn_cell.BasicRNNCell(self.state_size)

        grnn = tflon.graph.GRNN(graphs)
        grnn.initialize()
        fcell, bcell = cell_factory(), cell_factory()
        hwy = tflon.toolkit.Highway(lambda x: grnn(x, fcell, bcell))

        def grnn_pass(x):
            return hwy(x)

        net = tflon.graph.Neighborhood(graphs, depth=1) |\
              tflon.toolkit.Dense(self.state_size, activation=tf.nn.elu) |\
              tflon.toolkit.ForLoop(lambda i,x: grnn_pass(x), (size+1)//2) |\
              tflon.toolkit.Dense(5, activation=tf.nn.elu) |\
              tflon.toolkit.Dense(1)

        logits = net(nodes)
        self.add_output('solution', tf.nn.sigmoid(logits))
        self.add_loss('loss', tflon.toolkit.xent(solution, logits))

    def _parameters(self):
        return {'state_size': 10}
