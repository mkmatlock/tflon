from __future__ import print_function, division
from builtins import range
import tensorflow as tf
import tflon
import click
import os, sys
import numpy as np
from .maze import maze_generator, mazes_to_tables, get_graph

def surround(pos, size):
    x,y=pos
    if x<size-1:
        yield (x+1,y)
    if x>0:
        yield (x-1,y)
    if y<size-1:
        yield (x,y+1)
    if y>0:
        yield (x,y-1)

def argmax_solution(walls, goals, solution, size):
    start, end = np.split(np.argwhere(goals==1), 2)
    start=tuple(start[0].tolist())
    end=tuple(end[0].tolist())
    history = [start]
    while history[-1] != end:
        candidates = list(pos for pos in surround(history[-1], size) if pos not in history)
        scores = list(solution[pos] for pos in candidates)
        if len(scores)==0:
            break
        mx = np.argmax(scores)
        nxt = candidates[mx]
        history.append(nxt)
        if walls[nxt]==1:
            break
        if nxt == end:
            break
    return history

def check_solution(maze, outputs, size):
    walls = maze[:,:,0]
    goals = maze[:,:,1]
    solution = maze[:,:,2]

    h1=argmax_solution(walls,goals,solution,size)
    h2=argmax_solution(walls,goals,outputs,size)
    return all(i1==i2 for i1,i2 in zip(h1,h2))

def evaluate(model, test_data):
    print("Evaluating model...", file=sys.stderr)
    weights = len(model)

    for i, batch_size, maze_batch in test_data:
        print("Maze size %2d x %2d..." % (i, i), end='', file=sys.stderr)
        maze_input = mazes_to_tables(maze_batch, get_graph(i), batch_size)
        outputs = model.infer(maze_input)
        outputs = outputs['solution'].reshape((batch_size,2*i-1,2*i-1))

        score = 0.
        for j in range(batch_size):
            if check_solution(maze_batch[j,:,:,:], outputs[j,:,:], 2*i-1):
                score += 1.
        pct = int(score / batch_size * 100.)
        print("solved: %3d / %3d examples (%3d %%)" % (score, batch_size, pct), file=sys.stderr)

def generate_test_data(batch_size, test_range):
    print("Generating test data...", file=sys.stderr)
    test_data = []
    with click.progressbar(list(range(test_range[0], test_range[1]+1))) as pb:
        for i in pb:
            maze_batch = next(maze_generator(i, i, batch_size))
            test_data.append((i, batch_size, maze_batch))
    return test_data

@click.command()
@click.option('--batch-size', default=50)
@click.argument('modelfile')
def main(batch_size, modelfile):
    model = tflon.Model.load(modelfile)

    with tf.Session() as S:
        model.initialize()
        evaluate(model, generate_test_data(50, (3,20)))

if __name__=='__main__':
    main()
