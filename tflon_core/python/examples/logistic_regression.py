# Example training logistic regressor to classify the
# wisconsin breast cancer dataset

from __future__ import print_function
import subprocess, os
import pandas as pd
import tensorflow as tf
import tflon

class MyModel(tflon.model.Model):
    def _model(self):
        I = self.add_input('desc', shape=[None, 30])
        T = self.add_target('targ', shape=[None, 1])

        net = tflon.toolkit.WindowInput() |\
              tflon.toolkit.Dense(1)
        L = net(I)

        self.add_output( 'pred', tf.nn.sigmoid(L) )
        self.add_loss( 'xent', tflon.toolkit.xent_uniform_sum(T, L) )
        self.add_loss( 'l2', tflon.toolkit.l2_penalty(self.weights) )
        self.add_metric( 'auc', tflon.toolkit.auc(T, L) )

if __name__=='__main__':
    if not os.path.exists('./wdbc.data'):
        link = 'http://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data'
        subprocess.check_call(['wget', link])

    df = pd.read_csv("./wdbc.data" , header=None)
    df[1] = df[1].apply(lambda x : 1 if (x == 'M') else 0)
    df = df.iloc[:,1:]
    targ, desc = tflon.data.Table(df).split([1])
    feed = tflon.data.TableFeed({'desc':desc, 'targ':targ})

    LR = MyModel()

    trainer = tflon.train.OpenOptTrainer( iterations=100 )

    with tf.Session():
        LR.fit( feed, trainer, restarts=2 )
        LR.save("saved_lr.p")
        auc = LR.evaluate(feed, query='auc')
        print("AUC:", auc)
