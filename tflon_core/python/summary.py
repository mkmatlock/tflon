import os
import tensorflow as tf
from six import string_types

def get_latest_file(directory):
    filename = sorted([(os.path.getmtime(os.path.join(directory, fn)), fn) for fn in os.listdir(directory)])[-1][1]
    return os.path.join(directory, filename)

def get_logfiles(logdir):
    logfiles = [os.path.join(logdir, fn) for fn in os.listdir(logdir) if fn.startswith('events.out.tfevents')]
    return sorted(logfiles, key=lambda fpath: os.path.getmtime(fpath))

def search_summary(logfiles, *search_terms):
    """
        Search a logfile or sequence of logfiles

        Parameters:
            logfiles: path to a log file or list of log file paths
            *search_terms: log summary keys to gather from the logfiles
    """
    if isinstance(logfiles, string_types):
        logfiles = [logfiles]

    search_terms = [search.replace(' ', '_') for search in search_terms]

    step_numbers = []
    log_values = {}
    for lf in logfiles:
        iterator = tf.train.summary_iterator(lf)
        for event in iterator:
            for F in event.ListFields():
                if F[0].name=='step':
                    step_numbers.append(F[1])
                if F[0].name=='summary' and len(search_terms) > 0:
                    for value in F[1].ListFields()[0][1]:
                        var_field, content_field = value.ListFields()
                        var_name = var_field[1]
#                        summ_type = content_field[0].name
                        summ_value = content_field[1]

                        if any(search in var_name for search in search_terms):
                            D = log_values.setdefault(var_name, {})
                            D[step_numbers[-1]] = summ_value
    return step_numbers, log_values
