from setuptools import setup
from tf_ext import TensorflowExtension

try:
    from sphinx.setup_command import BuildDoc
    import subprocess

    class CustomDoc(BuildDoc):
        def run(self):
            subprocess.check_call(['./develop.sh'])
            BuildDoc.run(self)
    cmdclass={'build_sphinx': CustomDoc}
except:
    cmdclass={}

# Placeholder for building future C op extensions
# ops_mod = TensorflowExtension('tflon.lib.tflon_ops',
#                                sources=['tflon_core/cc/tflon_ops.cc'])

setup(name='tflon',
      version='0.1',
      description='tflon: non-stick tensorflow',
      author='Matt Matlock',
      author_email='matt.matlock@gmail.com',
      install_requires=['click', 'dill',
                        'ujson', 'sklearn',
                        'pandas', 'numpy',
                        'pyarrow', 'scipy',
                        'GPUtil', 'psutil',
                        'Pyro4==4.23'],
      url='http://bitbucket.org/mkmatlock/tflon',
      packages=['tflon', 'tflon.data', 'tflon.model', 'tflon.toolkit', 'tflon.train',
                'tflon.lib', 'tflon.chem', 'tflon.distributed', 'tflon.graph', 'tflon.image'],
      package_dir={'tflon': 'tflon_core/python', 'tflon.lib': 'tflon_core/cc'},
      scripts=['scripts/tflon'],
      cmdclass=cmdclass)
