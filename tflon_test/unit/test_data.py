from builtins import range
import unittest
import sys, os
import tarfile
import numpy as np
import scipy as sp
import pandas as pd
import tensorflow as tf
from pkg_resources import resource_filename
import tflon
from tflon.model import tower, model
from tflon.toolkit import build_tower
from tflon.data.feeds import TableFeed, ThreadCoordinator, QueueCoordinator, PyroCoordinator, TensorQueue, IndexQueue, convert_to_feedable
from tflon.data.tables import Table, DenseNestedTable, PaddedNestedTable, SparseIndexTable, RaggedTable, SparseIndexedValuesTable
from tflon_test.utils import RandomGenerator, with_tempfile, with_tempdir
from multiprocessing import managers

class StringyTable(Table):
    def serialize(self):
        return StringyTable(self.data.applymap(str))

    def deserialize(self):
        return StringyTable(self.data.applymap(int))

def _make_nested_column_data(nest_lengths):
    col_data = []
    data = np.arange(sum(nest_lengths)).tolist()
    start = 0
    for i in nest_lengths:
        col_data.append( data[start:start+i] )
        start+=i
    return col_data

class TableTests(unittest.TestCase):
    def test_table_drop_duplicates(self):
        index = [0, 1, 2, 3, 4, 0, 5, 3]
        t1 = Table( pd.DataFrame(np.arange(32).reshape(8,4), index=index) )
        t1.drop_duplicates()

        np.testing.assert_array_equal(t1.index, [0,1,2,3,4,5])
        np.testing.assert_array_equal(t1.shape, (6,4))

    def test_ragged_table(self):
        row_data = np.arange(13, dtype=np.int32).tolist()
        row2_data = np.arange(13,27, dtype=np.int32).tolist()
        row3_data = None
        nest_data = [3,6,4]
        nest2_data = [3,5,3,3]
        nest3_data = None
        nested_data = pd.DataFrame(columns=['ID', 'data', 'lengths']).set_index('ID')
        nested_data.loc[0] = [row_data, nest_data]
        nested_data.loc[1] = [row2_data, nest2_data]
        nested_data.loc[2] = [row3_data, nest3_data]
        table = RaggedTable(nested_data)

        self.assertEqual((11, 4), table.shape)
        rval = table.as_matrix()

        exp = [[0,1,2,13,14,15], [3,4,5,6,7,8,16,17,18,19,20], [9,10,11,12,21,22,23], [24,25,26]]
        exp_vec = np.array(sum(exp, []))
        exp_lens = np.array([len(r) for r in exp])
        exp_mat = np.array([L+[0]*(11-len(L)) for L in exp], dtype=np.int64).T
        np.testing.assert_array_equal(rval.values, exp_vec)
        np.testing.assert_array_equal(rval.lengths, exp_lens)
        np.testing.assert_array_equal(rval.todense(), exp_mat)


    def test_table_split(self):
        df = pd.DataFrame(np.arange(16*19).reshape(16,19))
        t = Table(df)

        ntables = t.split([2, 10, -2])

        self.assertEqual( len(ntables), 4 )
        self.assertEqual( ntables[0].shape, (16, 2) )
        self.assertEqual( ntables[1].shape, (16, 8) )
        self.assertEqual( ntables[2].shape, (16, 7) )
        self.assertEqual( ntables[3].shape, (16, 2) )

    def test_table_copy(self):
        col_data1 = np.arange(13).tolist()
        col_data2 = (np.arange(13)+13).tolist()
        nest_data1 = [col_data1[:3], col_data1[3:9], col_data1[9:13]]
        nest_data2 = [col_data2[:3], col_data2[3:9], col_data2[9:13]]
        nested_data = pd.DataFrame({'ID':[0,1,2], 'col1': nest_data1, 'col2': nest_data2}).set_index('ID')
        t = DenseNestedTable(nested_data)

        t2 = t.copy()

        self.assertEqual(t.__class__.__name__, t2.__class__.__name__)
        self.assertEqual(t.shape, t2.shape)
        self.assertFalse(t.data is t2.data)
        self.assertTrue(np.all(t.data.values==t2.data.values))
        t.data.iloc[2,1]=[44,55,66,77]
        self.assertFalse(np.all(t.data.values==t2.data.values))

    def test_table_statistics(self):
        col_data1 = np.arange(13).tolist()
        col_data2 = (np.arange(13)+13).tolist()
        data = pd.DataFrame({'ID':np.arange(13), 'col1': col_data1, 'col2': col_data2}).set_index('ID')
        t = Table(data)

        np.testing.assert_array_almost_equal( t.min(),   [np.min(col_data1), np.min(col_data2)] )
        np.testing.assert_array_almost_equal( t.max(),   [np.max(col_data1), np.max(col_data2)] )
        np.testing.assert_array_almost_equal( t.count(), [13, 13] )
        np.testing.assert_array_almost_equal( t.sum(),   [np.sum(col_data1), np.sum(col_data2)] )
        mu, sigma = t.moments()
        np.testing.assert_array_almost_equal(mu, [np.mean(col_data1), np.mean(col_data2)])
        np.testing.assert_array_almost_equal(sigma, [np.std(col_data1), np.std(col_data2)])

    def test_nestedtable_statistics(self):
        col_data1 = np.arange(13).tolist()
        col_data2 = (np.arange(13)+13).tolist()
        nest_data1 = [col_data1[:3], col_data1[3:9], col_data1[9:13]]
        nest_data2 = [col_data2[:3], col_data2[3:9], col_data2[9:13]]
        nested_data = pd.DataFrame({'ID':[0,1,2], 'col1': nest_data1, 'col2': nest_data2}).set_index('ID')
        t = DenseNestedTable(nested_data)

        np.testing.assert_array_almost_equal( t.min(),   [np.min(col_data1), np.min(col_data2)] )
        np.testing.assert_array_almost_equal( t.max(),   [np.max(col_data1), np.max(col_data2)] )
        np.testing.assert_array_almost_equal( t.count(), [13, 13] )
        np.testing.assert_array_almost_equal( t.sum(),   [np.sum(col_data1), np.sum(col_data2)] )
        mu, sigma = t.moments()
        np.testing.assert_array_almost_equal(mu, [np.mean(col_data1), np.mean(col_data2)])
        np.testing.assert_array_almost_equal(sigma, [np.std(col_data1), np.std(col_data2)])

    def test_paddedtable_statistics(self):
        col_data = _make_nested_column_data([3,6,4])
        t = PaddedNestedTable( pd.DataFrame({'col':col_data, 'ID':list(range(0, 3))}).set_index('ID') )
        values = sum(col_data, [])

        np.testing.assert_array_almost_equal(t.min(), np.min(values))
        np.testing.assert_array_almost_equal(t.max(), np.max(values))
        np.testing.assert_array_almost_equal(t.count(), len(values))
        np.testing.assert_array_almost_equal(t.sum(), np.sum(values))
        mu, std = t.moments()
        np.testing.assert_array_almost_equal(mu, np.mean(values))
        np.testing.assert_array_almost_equal(std, np.std(values))

    def test_table_concatenate(self):
        df1 = pd.DataFrame(np.arange(32).reshape(8,4))
        df2 = pd.DataFrame(np.arange(64).reshape(16,4))
        t1 = Table(df1)
        t2 = Table(df2)

        t3 = t1.concatenate([t2])

        np.testing.assert_array_equal(t3._data.values, np.vstack([df1.values, df2.values]))

    def test_paddedtable_concatenate(self):
        col_data1 = _make_nested_column_data([3,6,4])
        col_data2 = _make_nested_column_data([5,8,9])
        t1 = PaddedNestedTable( pd.DataFrame({'col':col_data1, 'ID':list(range(0, 3))}).set_index('ID') )
        t2 = PaddedNestedTable( pd.DataFrame({'col':col_data2, 'ID':list(range(3, 6))}).set_index('ID') )
        t3 = t1.concatenate([t2])

        exp_data = np.array([ col_data1[0] + [0]*6,
                              col_data1[1] + [0]*3,
                              col_data1[2] + [0]*5,
                              col_data2[0] + [0]*4,
                              col_data2[1] + [0]*1,
                              col_data2[2] ])

        np.testing.assert_array_equal( t3.as_matrix().toarray(), exp_data )

    def test_table_filter_rows_creates_null_fill(self):
        master = [0,1,2,3,4,5,6]
        index = [6,5,7,0]
        data = np.arange(28).reshape(7, 4)

        table = Table( pd.DataFrame(data, index=master) )
        table = table.filter_rows( index )

        exp = np.array([data[6].tolist(), data[5].tolist(), [np.nan]*4, data[0].tolist()])
        np.testing.assert_array_equal( table.as_matrix(), exp )


    def test_table_align_aligns_tables_inplace(self):
        master = [0,1,2,3,4,5,6]
        index = [4,6,5,3,0,2,1]
        data = np.arange(28).reshape(7, 4)

        table = Table( pd.DataFrame(data, index=index) )
        table.align( master )

        exp_data = np.array([data[index.index(id),:] for id in master])
        np.testing.assert_array_equal(table.as_matrix(), exp_data)

    def test_dense_nested_table_aligns_column_nested_data(self):
        col_data1 = np.arange(13).tolist()
        col_data2 = (np.arange(13)+13).tolist()
        nest_data1 = [col_data1[:3], col_data1[3:9], col_data1[9:13]]
        nest_data2 = [col_data2[:3], col_data2[3:9], col_data2[9:13]]
        nested_data = pd.DataFrame({'ID':[0,1,2], 'col1': nest_data1, 'col2': nest_data2}).set_index('ID')
        table = DenseNestedTable(nested_data)

        mat = table.as_matrix()
        np.testing.assert_array_equal(mat, np.array([col_data1, col_data2]).transpose())

    def test_padded_nested_table_adds_zero_padding(self):
        col_data = np.arange(13)
        nest_data = [col_data[:3].tolist(), col_data[3:9].tolist(), col_data[9:13].tolist()]
        nested_data = pd.DataFrame({'ID':[0,1,2], 'data':nest_data}).set_index('ID')
        table = PaddedNestedTable(nested_data)

        exp_data = np.array([ nest_data[0] + [0]*3,
                              nest_data[1],
                              nest_data[2] + [0]*2 ])
        mat = table.as_matrix().toarray()
        np.testing.assert_array_equal(mat, exp_data)

    def test_padded_nested_table_can_add_extra_padding(self):
        col_data = np.arange(13)
        nest_data = [col_data[:3].tolist(), col_data[3:9].tolist(), col_data[9:13].tolist()]
        nested_data = pd.DataFrame({'ID':[0,1,2], 'data':nest_data}).set_index('ID')
        table = PaddedNestedTable(nested_data, 8)

        exp_data = np.array([ nest_data[0] + [0]*5,
                              nest_data[1] + [0]*2,
                              nest_data[2] + [0]*4 ])
        mat = table.as_matrix().toarray()
        np.testing.assert_array_equal(mat, exp_data)

    def test_padded_nested_table_error_when_small(self):
        col_data = np.arange(13)
        nest_data = [col_data[:3].tolist(), col_data[3:9].tolist(), col_data[9:13].tolist()]
        nested_data = pd.DataFrame({'ID':[0,1,2], 'data':nest_data}).set_index('ID')
        table = PaddedNestedTable(nested_data, 3)

        try:
            table.as_matrix()
        except ValueError:
            pass
        else:
            raise AssertionError("Expected ValueError exception")

    def test_sparse_index_table_constructs_references(self):
        IDs = [0,1,2]
        index_refs = [[4,5,7], [0,3,7,8], [1,2,3,5,6,8]]
        for row in index_refs:
            for j in range(len(row)):
                row[j] = chr(row[j]+65)
        code = [chr(i+65) for i in range(9)]

        table = SparseIndexTable( pd.DataFrame({'ID':IDs, 'refs':index_refs}).set_index('ID'),
                                  pd.DataFrame({'code': list(range(len(code)))}, index=code) )
        exp_data = np.array([[0,0,0,0,1,1,0,1,0], [1,0,0,1,0,0,0,1,1], [0,1,1,1,0,1,1,0,1]])

        mat = table.as_matrix().toarray()
        np.testing.assert_array_equal(mat, exp_data)

    def test_sparse_index_table_concatenate(self):
        code = [i for i in range(9)]

        IDs1 = [0,1,2]
        index_refs1 = [[4,5,7], [0,3,7,8], [1,2,3,5,6,8]]
        table1 = SparseIndexTable( pd.DataFrame({'ID':IDs1, 'refs':index_refs1}).set_index('ID'),
                                  pd.DataFrame({'code': code}) )
        IDs2 = [3,4,5]
        index_refs2 = [[0,2,4,5,6,7], [0,3,7,8], []]
        table2 = SparseIndexTable( pd.DataFrame({'ID':IDs2, 'refs':index_refs2}).set_index('ID'),
                                  pd.DataFrame({'code': code}) )

        table3 = table1.concatenate([table2])

        exp_data = np.array([[0,0,0,0,1,1,0,1,0],
                             [1,0,0,1,0,0,0,1,1],
                             [0,1,1,1,0,1,1,0,1],
                             [1,0,1,0,1,1,1,1,0],
                             [1,0,0,1,0,0,0,1,1],
                             [0,0,0,0,0,0,0,0,0]])

        mat = table3.as_matrix().toarray()
        np.testing.assert_array_equal(mat, exp_data)

    def test_sparse_indexed_valued_table_values(self):
        code = [i for i in range(9)]
        IDs = [0,1,2]
        index_refs = [[4,5,7], [0,3,7,8], [1,2,3,5,6,8]]
        values_refs = [[2,4,2], [8,9,6,8], [8,3,1,2,4,2]]
        table = SparseIndexedValuesTable( pd.DataFrame({'ID':IDs, 'refs':index_refs, 'values_refs':values_refs}).set_index('ID'),
                                          pd.DataFrame({'code': list(range(len(code)))}, index=code) )
        exp_data  = [[0,0,0,0,2,4,0,2,0],[8,0,0,9,0,0,0,6,8],[0,8,3,1,0,2,4,0,2]]
        mat = table.as_matrix().toarray()
        np.testing.assert_array_equal(mat, exp_data)

class TestModel(model.Model):
    def _model(self):
        self.suppress_warnings()
        pass

class FeedTests(unittest.TestCase):
    def setUp(self):
        tf.reset_default_graph()

        TensorQueue.DEFAULT_TIMEOUT=1000
        self._test_model = TestModel()
        self._test_tower = self._test_model._tower
        self._test_builder = build_tower( self._test_tower )
        self._test_builder.__enter__()
        tflon.logging.set_test_mode()

    def tearDown(self):
        self._test_builder.__exit__()
        tflon.logging.clear_test_logs()

    def test_index_queue_always_unique_when_out_of_phase(self):
        IDs = list(range(121))

        IQ = IndexQueue(IDs, shuffle=True)
        for i in range(1000):
            dq = IQ.dequeue(24)
            assert len(set(dq)) == 24, "Failed on batch %d" % (i)

    def test_index_queue_with_single_pass_shuffle(self):
        Ids = list(range(200))

        collected = []
        IQ = IndexQueue(Ids, epoch_limit=1, shuffle=True)
        while not IQ.empty():
            B = IQ.dequeue(10)
            collected.append(B)

        collected = sum(collected, [])
        self.assertEqual(len(Ids), len(collected))
        self.assertEqual(set(Ids), set(collected))
        self.assertNotEqual(Ids, collected)

    def test_index_queue_with_multi_pass_shuffle(self):
        Ids = list(range(200))
        Idcounts = {}

        IQ = IndexQueue(Ids, shuffle=True)
        while IQ.epochs < 2:
            B = IQ.dequeue(12)
            for ID in B:
                Idcounts[ID] = Idcounts.get(ID, 0) + 1

        counts = [Idcounts[v] for v in Ids]
        self.assertEqual(192, len([c for c in counts if c == 2]))
        self.assertEqual(8, len([c for c in counts if c == 3]))

    def test_index_queue_with_multi_pass_no_shuffle(self):
        Ids = list(range(200))
        Idcounts = [0]*200

        IQ = IndexQueue(Ids, shuffle=False)
        while IQ.epochs < 2:
            B = IQ.dequeue(12)
            for ID in B:
                Idcounts[ID] += 1

        self.assertTrue(all([v==2 for v in Idcounts[8:]]))
        self.assertTrue(all([v==3 for v in Idcounts[:8]]))

    @with_tempfile
    def test_write_table_feed_to_archive(self, tfn):
        tsv_reader = lambda fpath: pd.read_csv(fpath, sep='\t', dtype={'ID':str}).set_index('ID')
        schema = tflon.data.Schema({'desc': tflon.data.SchemaEntry('desc', tflon.data.Table),
                                    'targ': tflon.data.SchemaEntry('targ', tflon.data.Table)})
        schema = schema.map(desc=('descriptors.tsv', tsv_reader), targ=('targets.tsv', tsv_reader))
        feed = tflon.data.make_table_feed( resource_filename('tflon_test.data', 'distributed'), schema, master_table='desc', shard_format='directory' )
        tflon.data.archive_shard(feed, tfn)

        with tarfile.open(tfn, 'r:gz') as tfile:
            self.assertEqual(set(tfile.getnames()), {'index', 'desc.pq', 'targ.pq'})

    @with_tempdir
    def test_write_table_feed_to_dir(self, tdir):
        tsv_reader = lambda fpath: pd.read_csv(fpath, sep='\t', dtype={'ID':str}).set_index('ID')
        schema = tflon.data.Schema({'desc': tflon.data.SchemaEntry('desc', tflon.data.Table),
                                    'targ': tflon.data.SchemaEntry('targ', tflon.data.Table)})
        schema = schema.map(desc=('descriptors.tsv', tsv_reader), targ=('targets.tsv', tsv_reader))
        feed = tflon.data.make_table_feed( resource_filename('tflon_test.data', 'distributed'), schema, master_table='desc', shard_format='directory' )
        tflon.data.write_shard(feed, tdir)

        self.assertEqual(set(os.listdir(tdir)), {'desc.pq', 'targ.pq', 'index'})

    @with_tempdir
    def test_write_shard(self, tdir):
        index = np.arange(8)
        table1 = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
        table2 = pd.DataFrame(np.array([[10, 20, 30], [40, 50, 60], [70, 80, 90]]))

        t1 = StringyTable(table1)
        t2 = Table(table2)

        feed = TableFeed({'t1':t1, 't2':t2})

        tflon.data.write_shard(feed, tdir)
        t1_shard = tflon.data.read_parquet(tdir + '/t1.pq')
        t2_shard = tflon.data.read_parquet(tdir + '/t2.pq')

        np.testing.assert_array_equal(t1_shard.values, np.array([['1', '2', '3'], ['4', '5', '6'], ['7', '8', '9']]))
        np.testing.assert_array_equal(t2_shard.values, table2.values)

    def test_make_table_feed_loads_all_shards(self):
        tsv_reader = lambda fpath: pd.read_csv(fpath, sep='\t', dtype={'ID':str}).set_index('ID')
        schema = tflon.data.Schema({'desc': tflon.data.SchemaEntry('desc', tflon.data.Table),
                                    'targ': tflon.data.SchemaEntry('targ', tflon.data.Table)})
        schema = schema.map(desc=('descriptors.tsv', tsv_reader), targ=('targets.tsv', tsv_reader))
        feed = tflon.data.make_table_feed( resource_filename('tflon_test.data', 'distributed'), schema, master_table='desc', shard_format='directory' )

        self.assertEqual(sorted(feed.keys()), ['_index', 'desc', 'targ'])
        self.assertEqual(feed['desc'].shape, (8375+999,210))
        self.assertEqual(feed['targ'].shape, (8375+999,1))

    def test_make_table_feed_loads_all_shards_from_parallel_archives(self):
        tsv_reader = lambda fpath: pd.read_csv(fpath, sep='\t', dtype={'ID':str}).set_index('ID')
        schema = tflon.data.Schema({'desc': tflon.data.SchemaEntry('desc', tflon.data.Table),
                                    'targ': tflon.data.SchemaEntry('targ', tflon.data.Table)})
        schema = schema.map(desc=('descriptors.tsv', tsv_reader), targ=('targets.tsv', tsv_reader))
        l1 = os.path.join(resource_filename('tflon_test.data', 'multiple'), 'loc1')
        l2 = os.path.join(resource_filename('tflon_test.data', 'multiple'), 'loc2')
        feed = tflon.data.make_table_feed( [l1, l2], schema, master_table='desc', shard_format='directory' )

        self.assertEqual(sorted(feed.keys()), ['_index', 'desc', 'targ'])
        self.assertEqual(feed['desc'].shape, (8375+999,210))
        self.assertEqual(feed['targ'].shape, (8375+999,1))


    def test_table_feed_converts_sparse_data(self):
        index = np.arange(8)
        t1 = Table( pd.DataFrame(np.arange(32).reshape(8,4), index=index) )
        col_data = _make_nested_column_data([3,6,4,2,10,9,6,0])
        t2 = PaddedNestedTable( pd.DataFrame({'col':col_data}, index=index) )

        feed = TableFeed({'t1':t1, 't2':t2})
        batch = {k: convert_to_feedable(v) for k, v in feed.batch([2,5,6]).items()}

        self.assertEqual(batch['t1'].shape, (3, 4))
        self.assertEqual(type(batch['t2']), tf.SparseTensorValue)
        self.assertEqual(batch['t2'].dense_shape, (3, 10))

    def test_tablefeed_holdout_ignores_missing(self):
        index = np.arange(8)
        t1 = Table( pd.DataFrame(np.arange(32).reshape(8,4), index=index) )
        col_data = _make_nested_column_data([3,6,4,2,10,9,6,0])
        t2 = PaddedNestedTable( pd.DataFrame({'col':col_data}, index=index) )

        feed = TableFeed({'t1':t1, 't2':t2})
        nfeed = feed.holdout(set([0,3,6,9]))

        np.testing.assert_array_equal( feed.index, [1,2,4,5,7] )
        np.testing.assert_array_equal( nfeed.index, [0,3,6] )
        for k in feed:
            self.assertEqual( feed[k].shape[0], 5 )
            self.assertEqual( nfeed[k].shape[0], 3 )

    def test_tablefeed_xval_from_groups(self):
        index = np.arange(8)
        t1 = Table( pd.DataFrame(np.arange(32).reshape(8,4), index=index) )
        col_data = _make_nested_column_data([3,6,4,2,10,9,6,0])
        t2 = PaddedNestedTable( pd.DataFrame({'col':col_data}, index=index) )

        feed = TableFeed({'t1':t1, 't2':t2})

        holdouts = pd.DataFrame({'GROUPS': [0,3,2,1,2,1,0,0]}, index=index)

        expected_groups = {0: {0,6,7}, 1: {3, 5}, 2: {2, 4}, 3: {1}}

        for i, (test, train) in enumerate(feed.xval(4, holdouts)):
            self.assertEqual(set(test.index), expected_groups[i])
            self.assertEqual(set(train.index), set(index)-expected_groups[i])


    def test_tablefeed_xval(self):
        index = np.arange(8)
        t1 = Table( pd.DataFrame(np.arange(32).reshape(8,4), index=index) )
        col_data = _make_nested_column_data([3,6,4,2,10,9,6,0])
        t2 = PaddedNestedTable( pd.DataFrame({'col':col_data}, index=index) )

        feed = TableFeed({'t1':t1, 't2':t2})

        test_counts, train_counts = {}, {}
        i = 0
        for test,train in feed.xval():
            self.assertEqual(len(test.index), 1)
            for idx in test.index:
                test_counts[idx] = test_counts.get(idx, 0)+1
            self.assertEqual(len(train.index), 7)
            for idx in train.index:
                train_counts[idx] = train_counts.get(idx, 0)+1
            i+=1

        self.assertEqual(8, i)
        self.assertEqual(list(test_counts.values()), [1]*8)
        self.assertEqual(list(train_counts.values()), [7]*8)

        test_sizes, train_sizes = [], []
        test_counts, train_counts = {}, {}
        i = 0
        for test,train in feed.xval(3):
            test_sizes.append(len(test.index))
            for idx in test.index:
                test_counts[idx] = test_counts.get(idx, 0)+1
            train_sizes.append(len(train.index))
            for idx in train.index:
                train_counts[idx] = train_counts.get(idx, 0)+1
            i+=1

        self.assertEqual(3, i)
        self.assertEqual(test_sizes, [3,3,2])
        self.assertEqual(train_sizes, [5,5,6])
        self.assertEqual(list(test_counts.values()), [1]*8)
        self.assertEqual(list(train_counts.values()), [2]*8)

    def test_tablefeed_drop(self):
        index = np.arange(8)
        t1 = Table( pd.DataFrame(np.arange(32).reshape(8,4), index=index) )
        col_data = _make_nested_column_data([3,6,4,2,10,9,6,0])
        t2 = PaddedNestedTable( pd.DataFrame({'col':col_data}, index=index) )

        feed = TableFeed({'t1':t1, 't2':t2})
        feed.drop({0,3,6,9})

        np.testing.assert_array_equal( feed.index, [1,2,4,5,7] )
        for k in feed:
            self.assertEqual( feed[k].shape[0], 5 )

    def test_tablefeed_sampler(self):
        index = np.arange(8)
        t1 = Table( pd.DataFrame(np.arange(32).reshape(8,4), index=index) )
        col_data = _make_nested_column_data([3,6,4,2,10,9,6,0])
        t2 = PaddedNestedTable( pd.DataFrame({'col':col_data}, index=index) )

        feed = TableFeed({'t1':t1, 't2':t2})

        sampler = feed.sample()

        for i in range(10):
            batch = sampler(i+1)
            self.assertEqual(len(batch.index), i+1)

class CoordinatorTests(unittest.TestCase):
    def setUp(self):
        tf.reset_default_graph()

        TensorQueue.DEFAULT_TIMEOUT=1000
        self._test_model = TestModel()
        self._test_tower = self._test_model._tower
        self._test_builder = build_tower( self._test_tower )
        self._test_builder.__enter__()
        tflon.logging.set_test_mode()

    def test_queue_can_create_persistent_tensors_for_data_subsets(self):
        inp = self._test_tower.add_input(name="image", shape=[None, 5, 5], dtype=tf.float32)
        tar = self._test_tower.add_target(name="label", shape=[None, 10], dtype=tf.float32)

        with tf.Session() as S:
            TQ = TensorQueue(self._test_tower, capacity=1)

            inp_tensor = np.random.random(size=(10,5,5))
            TQ.enqueue( { inp: inp_tensor } )
            FD = TQ.fetch()
            v = S.run( inp, feed_dict=FD )
            np.testing.assert_array_almost_equal( inp_tensor, v )

            tar_tensor = np.random.random(size=(10,10))
            TQ.enqueue( { tar: tar_tensor } )
            FD = TQ.fetch()
            v = S.run( tar, feed_dict=FD )
            np.testing.assert_array_almost_equal( tar_tensor, v )

            try:
                v = S.run( inp, feed_dict=FD )
                raise AssertionError( "Should have raised exception!" )
            except:
                pass

    @unittest.skipIf(True, "Skip test for unimplemented feature")
    def test_allow_feeding_persistent_tensors_to_bottleneck_ops(self):
        inp = self._test_tower.add_input(name="image", shape=[None, 5, 5], dtype=tf.float32)
        v = inp * 2
        self._test_tower.add_bottleneck('mul_op', v)
        final = v + 4

        with tf.Session() as S:
            TL = TensorLoader(self._test_tower)

            inp_tensor = np.random.random(size=(10, 5, 5))
            mul_result = inp_tensor*2
            final_result = mul_result+4

            alt_mul = np.random.random(size=(10,5,5))
            alt_final = alt_mul+4

            TL.load( {'image': inp_tensor} )
            res = S.run( final, feed_dict=TL.fetch() )
            np.testing.assert_array_almost_equal( final_result, res )

            TL.load( {'mul_op': mul_result} )
            res = S.run( final, feed_dict=TL.fetch() )
            np.testing.assert_array_almost_equal( final_result, res )

            TL.load( {'mul_op': alt_mul} )
            res = S.run( final, feed_dict=TL.fetch() )
            np.testing.assert_array_almost_equal( alt_final, res )

    def test_tensor_queue_persists_dequeued_tensors(self):
        inp = self._test_tower.add_input(name="image", shape=[None, 5, 5], dtype=tf.float32)
        mu = self._test_tower.add_input(name="offset", shape=[None], dtype=tf.float32)
        sigma = self._test_tower.add_input(name="range", shape=[None], dtype=tf.float32)
        normed = (inp - tf.reshape(mu, (-1, 1, 1))) / tf.reshape(sigma, (-1, 1, 1))

        Q = TensorQueue(self._test_tower)
        G = iter( RandomGenerator( shapes={'image': (10, 5, 5), 'offset': (10,), 'range': (10,)} ) )

        with tf.Session() as S:
            for i in range(10):
                B = next(G)
                FD = self._test_tower.feed(B)
                Q.enqueue(FD)
            self.assertEqual(10, Q.size())

            FD = Q.fetch()
            A = S.run( normed, feed_dict=FD )
            B = S.run( normed, feed_dict=FD )
            np.testing.assert_array_almost_equal(A, B)

            FD = Q.fetch()
            C = S.run( normed, feed_dict=FD )
            self.assertFalse( np.any(A==C) )

            Q.empty()

    def test_tensor_queue_with_just_one_input(self):
        inp = self._test_tower.add_input(name="image", shape=[None, 5, 5], dtype=tf.float32)

        Q = TensorQueue(self._test_tower)
        RG = RandomGenerator( shapes={'image': (10, 5, 5)} )
        G = iter( RG )

        with tf.Session() as S:
            for i in range(10):
                B = next(G)
                FD = self._test_tower.feed(B)
                Q.enqueue(FD)
            self.assertEqual(10, Q.size())

            for i in range(10):
                val = S.run(inp, feed_dict=Q.fetch())
                np.testing.assert_array_almost_equal(val, RG.history[i]['image'])

    def test_queue_coordinator_starts_and_stops_queue_runners(self):
        inp = self._test_tower.add_input(name="image", shape=[None,25], dtype=tf.float32)
        mu = self._test_tower.add_input(name="offset", shape=[None,1], dtype=tf.float32)
        sigma = self._test_tower.add_input(name="range", shape=[None,1], dtype=tf.float32)
        normed = (inp - tf.reshape(mu, (-1, 1, 1))) / tf.reshape(sigma, (-1, 1, 1))

        shapes={'image': (10, 25), 'offset': (10,1), 'range': (10,1)}
        dfs = { name: pd.DataFrame(np.random.random(size=shapes[name]), index=range(10)) for name in shapes }
        data = { name: Table(dfs[name]) for name in dfs }
        feed = TableFeed(data)

        with tf.Session() as S:
            Q = TensorQueue( self._test_tower )
            with QueueCoordinator( self._test_model, feed.shuffle(batch_size=5), Q, processes=2 ):
                FD = Q.fetch()
                A = S.run( normed, feed_dict=FD )
                B = S.run( normed, feed_dict=FD )
                np.testing.assert_array_almost_equal(A, B)

                FD = Q.fetch()
                C = S.run( normed, feed_dict=FD )
                self.assertFalse( np.any(A==C) )

    def test_thread_coordinator_FIFO_ordering(self):
        inp = self._test_tower.add_input(name="image", shape=[None, 25], dtype=tf.float32)
        mu = self._test_tower.add_input(name="offset", shape=[None,1], dtype=tf.float32)
        sigma = self._test_tower.add_input(name="range", shape=[None,1], dtype=tf.float32)

        shapes={'image': (10, 25), 'offset': (10,1), 'range': (10,1)}
        dfs = { name: pd.DataFrame(np.random.random(size=shapes[name]), index=range(10)) for name in shapes }
        data = { name: Table(dfs[name]) for name in dfs }
        feed = TableFeed(data)
        batches = []

        def record_iterator():
            it = feed.shuffle(batch_size=1)
            while 1:
                B = next(it)
                batches.append(B)
                yield B

        with tf.Session() as S:
            Q = TensorQueue(self._test_tower)

            with ThreadCoordinator( self._test_model, record_iterator(), Q, processes=1 ):
                for i in range(10):
                    FD = S.run([inp, mu, sigma], feed_dict=Q.fetch())
                    FD = {'image':FD[0], 'offset':FD[1], 'range':FD[2]}
                    for name in FD:
                        np.testing.assert_array_almost_equal(FD[name], batches[i][name].data.values, err_msg='Mismatch on batch %d, table %s' % (i, name))

    def test_queue_coordinator_FIFO_ordering(self):
        inp = self._test_tower.add_input(name="image", shape=[None, 25], dtype=tf.float32)
        mu = self._test_tower.add_input(name="offset", shape=[None,1], dtype=tf.float32)
        sigma = self._test_tower.add_input(name="range", shape=[None,1], dtype=tf.float32)

        shapes={'image': (10, 25), 'offset': (10,1), 'range': (10,1)}
        dfs = { name: pd.DataFrame(np.random.random(size=shapes[name]), index=range(10)) for name in shapes }
        data = { name: Table(dfs[name]) for name in dfs }
        feed = TableFeed(data)
        batches = []

        def record_iterator():
            it = feed.shuffle(batch_size=1)
            while 1:
                B = next(it)
                batches.append(B)
                yield B

        with tf.Session() as S:
            Q = TensorQueue(self._test_tower)

            with QueueCoordinator( self._test_model, record_iterator(), Q, processes=1 ):
                for i in range(10):
                    FD = S.run([inp, mu, sigma], feed_dict=Q.fetch())
                    FD = {'image':FD[0], 'offset':FD[1], 'range':FD[2]}
                    for name in FD:
                        np.testing.assert_array_almost_equal(FD[name], batches[i][name].data.values, err_msg='Mismatch on batch %d, table %s' % (i, name))

    @unittest.skipIf( tuple(int(v) for v in tf.__version__.split('.')) < (1, 3), "Skipping buffer test, tensorflow version < 1.3" )
    def test_queue_coordinator_with_buffering(self):
        inp = self._test_tower.add_input(name="image", shape=[None, 25], dtype=tf.float32)
        mu = self._test_tower.add_input(name="offset", shape=[None, 1], dtype=tf.float32)
        sigma = self._test_tower.add_input(name="range", shape=[None, 1], dtype=tf.float32)

        shapes={'image': (20, 25), 'offset': (20,1), 'range': (20,1)}
        dfs = { name: pd.DataFrame(np.random.random(size=shapes[name]), index=range(20)) for name in shapes }
        data = { name: Table(dfs[name]) for name in dfs }
        feed = TableFeed(data)

        all_seen = []
        with tf.Session() as S:
            Q = TensorQueue(self._test_tower, staging=True)

            with QueueCoordinator( self._test_model, feed.shuffle(batch_size=1), Q, processes=2):
                for i in range(10):
                    data = Q.fetch()
                    FD = S.run( [inp, mu, sigma], feed_dict=data )
                    all_seen.append({k:FD[i] for i,k in enumerate(['image', 'offset', 'range'])})

        def find(fD):
            for i in range(20):
                if all(np.allclose(fD[k], dfs[k].values[i], atol=1e-4, rtol=1e-4) for k in ['image', 'offset', 'range']):
                    return i, True
            return None, False

        recovered = set()
        for fD in all_seen:
            idx, found = find(fD)
            assert found
            assert idx not in recovered
            recovered.add(idx)

        assert len(recovered)==10

        staging_ops=0
        for op in tf.get_default_graph().get_operations():
            if op.name.startswith("TensorQueue/TensorQueue/StagingArea"):
                staging_ops += 1
        assert staging_ops > 0

    def test_pyro_coordinator_starts_and_stops_featurizers(self):
        inp = self._test_tower.add_input(name="image", shape=[None,25], dtype=tf.float32)
        mu = self._test_tower.add_input(name="offset", shape=[None,1], dtype=tf.float32)
        sigma = self._test_tower.add_input(name="range", shape=[None,1], dtype=tf.float32)
        normed = (inp - tf.reshape(mu, (-1, 1, 1))) / tf.reshape(sigma, (-1, 1, 1))

        shapes={'image': (10, 25), 'offset': (10,1), 'range': (10,1)}
        dfs = { name: pd.DataFrame(np.random.random(size=shapes[name]), index=range(10)) for name in shapes }
        data = { name: Table(dfs[name]) for name in dfs }
        feed = TableFeed(data)

        with tf.Session() as S:
            Q = TensorQueue( self._test_tower )
            with PyroCoordinator( self._test_model, feed.shuffle(batch_size=5), Q, processes=2 ):
                FD = Q.fetch()
                A = S.run( normed, feed_dict=FD )
                B = S.run( normed, feed_dict=FD )
                np.testing.assert_array_almost_equal(A, B)

                FD = Q.fetch()
                C = S.run( normed, feed_dict=FD )
                self.assertFalse( np.any(A==C) )

    def test_pyro_coordinator_restart_featurizers_on_failure(self):
        desc = self._test_tower.add_input(name='desc', shape=[None, 50], ttype=tflon.data.Table, dtype=tf.float32)
        targ = self._test_tower.add_input(name='targ', shape=[None, 1], ttype=tflon.data.Table, dtype=tf.float32)
        self._test_tower.add_preprocessor(NFailurePreprocessor())
        tsv_reader = lambda fpath: pd.read_csv(fpath, sep='\t', dtype={'ID':str}).set_index('ID')
        schema = self._test_model.schema.map(desc=('descriptors.tsv', tsv_reader), targ=('targets.tsv', tsv_reader))
        feed = tflon.data.make_table_feed( resource_filename('tflon_test.data', 'distributed'), schema, master_table='desc', shard_format='directory' )

        with tf.Session() as S:
            Q = TensorQueue( self._test_tower, timeout=5000 )
            with PyroCoordinator( self._test_model, feed.shuffle(batch_size=25), Q, processes=2, timeout=1 ):
                for i in range(100):
                    FD = Q.fetch()
                    D, T = S.run( [desc, targ], feed_dict=FD )
                    self.assertEqual(D.shape, (25, 50))
                    self.assertEqual(T.shape, (25, 1))

        restart_messages = []
        for level, message, args, kwargs in tflon.logging.get_test_logs():
            if level=='W' and 'Batch queuer' in message and 'restart service' in message:
                restart_messages.append(message)
        assert len(restart_messages) >= 2

class NFailurePreprocessor(tflon.model.Preprocessor):
    def __init__(self):
        super(NFailurePreprocessor, self).__init__(schema={'desc':tflon.data.Table})
        self.N = np.random.randint(5, 45)
        self.i = 0

    def _preprocess(self, batch):
        import time
        self.i+=1
        if self.i==self.N:
            time.sleep(5)
        sliced = batch['desc'].data.iloc[:,:50]
        return {'desc': sliced}
