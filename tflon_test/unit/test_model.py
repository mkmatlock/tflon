from builtins import range
import sys
import unittest
import tflon
import scipy as sp
import pandas as pd
import numpy as np
import dill as pickle
import tensorflow as tf
from pkg_resources import resource_filename
from tflon_test.utils import RandomGenerator, with_tempfile, TflonTestCase, capture_stderr

class NeuralNetDescNorm(tflon.model.Model):
    def _data(self, batch):
        batch['desc']=batch['desc'].as_matrix()/2
        return batch

    def _model(self):
        I = self.add_input('desc', shape=[None, 10])
        T = self.add_target('targ', shape=[None, 1])
        net = tflon.toolkit.Dense(5, activation=tf.tanh) | tflon.toolkit.Dense(1)
        L = net(I)
        self.add_metric( 'auc', tflon.toolkit.auc( T, L ) )

class NeuralNet(tflon.model.Model):
    def _model(self, width=5):
        I = self.add_input('desc', shape=[None, 10])
        T = self.add_target('targ', shape=[None, self.num_targets])

        width = self.get_parameter( 'width', width )
        net = tflon.toolkit.Dense(width, activation=tf.tanh) | tflon.toolkit.Dense(self.num_targets)
        L = net(I)

        self.add_output( "pred", tf.nn.sigmoid(L) )
        self.add_loss( "xent", tf.losses.sigmoid_cross_entropy(T, L) )

        L2coeff = self.get_parameter('L2_coefficient')
        self.add_loss( "l2", L2coeff * tf.add_n( [tf.nn.l2_loss( W ) for W in self.weights] ) )
        self.add_metric( 'auc', tflon.toolkit.auc( T, L ) )

    def _parameters(self):
        return {'L2_coefficient': 0.1, 'num_targets': 1}

class SparseSum(tflon.model.Model):
    def _model(self):
        I = self.add_input('sparse', shape=[None, 10000], sparse=True)

        out = tf.sparse_reduce_sum(I, axis=1)
        self.add_output('sum', out)

class RaggedProduct(tflon.model.Model):
    def _model(self):
        I = self.add_input('ragged', shape=[None, 4], ragged=True)

        out = tf.stack([tf.reduce_prod(I.slice(i)+1) for i in range(4)])
        self.add_output('prod', out)

class SchemaTest(tflon.model.Model):
    def _model(self):
        molecules = self.add_table('molecules', ttype=tflon.chem.MoleculeTable)
        atom = self.add_input('atoms', shape=[None, 22], ttype=tflon.data.DenseNestedTable)
        react = self.add_input('react', shape=[None, 4], ttype=tflon.data.Table)
        targets = self.add_target('targets', shape=[None, 14], ttype=tflon.data.Table)

        out = tf.reduce_sum(atom) + tf.reduce_sum(react) + tf.reduce_sum(targets)
        self.add_output('out', out)

class DenseTransformTest(tflon.model.Model):
    def _model(self):
        atoms = self.add_input('atoms', shape=[None, 22], ttype=tflon.data.DenseNestedTable)
        out = tflon.toolkit.Dense(10, activation=tf.nn.elu)(atoms)
        self.add_output('out', out, tflon.data.DenseToNested('atoms'))

class DenseGraphTransformTest(tflon.model.Model):
    def _model(self):
        graphs = self.add_table('molecules', ttype=tflon.chem.MoleculeTable)
        atoms = self.add_input('atoms', shape=[None, 22], ttype=tflon.data.DenseNestedTable)
        out = tflon.toolkit.Dense(10, activation=tf.nn.elu)(atoms)
        self.add_output('out', out, tflon.graph.NodesToNested(graphs))

class FeaturizeTestModel(tflon.model.Model):
    def _model(self):
        atom = self.add_input('atoms', shape=[None, 22], ttype=tflon.data.DenseNestedTable)
        targets = self.add_target('targets', shape=[None, 14], ttype=tflon.data.Table)

        net = tflon.toolkit.SegmentReduce('atoms') | tflon.toolkit.Dense(14)
        logits = net(atom)

        self.add_output('out', tf.nn.sigmoid(logits))
        self.add_loss('xent', tf.losses.sigmoid_cross_entropy(targets, logits))

class ModelTests(TflonTestCase):
    def test_model_featurize(self):
        M = FeaturizeTestModel()
        shard = M.schema.map(atoms='atom.pq').load( resource_filename('tflon_test.data', 'sample_shard') )
        M.featurize(shard)
        assert np.all(shard['SegmentReduce_0/reduce'].index == shard['atoms'].index)
        self.assertEqual(shard['SegmentReduce_0/reduce'].as_matrix().shape, (17723,))

    def test_model_featurize_by_batch(self):
        M = FeaturizeTestModel()
        shard = M.schema.map(atoms='atom.pq').load( resource_filename('tflon_test.data', 'sample_shard') )
        M.featurize(shard, batch_size=100)
        assert np.all(shard['SegmentReduce_0/reduce'].index == shard['atoms'].index)

        loglines = tflon.logging.get_test_logs()
        assert len([l for l in loglines if 'Featurizing' in l[1]]) == 10, '\n'.join(loglines)
        self.assertEqual(shard['SegmentReduce_0/reduce'].as_matrix().shape, (17723,))

    def test_model_deserialize_graphs_before_inference(self):
        M = DenseGraphTransformTest()
        shard = M.schema.map(atoms='atom.pq').load( resource_filename('tflon_test.data', 'sample_shard') )

        with tf.Session() as S:
            M.initialize()
            for output in M.infer(shard.iterate(batch_size=100), query='out'):
                assert output.shape==(100,10)

    def test_model_evaluate_with_transform(self):
        index = list(range(100))
        feed = tflon.data.TableFeed({ 'desc': tflon.data.Table(pd.DataFrame(data=np.random.random((100,10)), index=index)),\
                                      'targ': tflon.data.Table(pd.DataFrame(data=np.random.randint(0,2,size=(100,1)).astype(np.float32), index=index)) })

        M = NeuralNetDescNorm()

        with tf.Session() as S:
            M.initialize()

            auc = M.evaluate( feed.iterate(10), query='auc' )
            assert not np.isnan(auc)

    def test_model_multiple_evaluations(self):
        index = list(range(100))
        feed = tflon.data.TableFeed({ 'desc': tflon.data.Table(pd.DataFrame(data=np.random.random((100,10)), index=index)),\
                                      'targ': tflon.data.Table(pd.DataFrame(data=np.random.randint(0,2,size=(100,1)).astype(np.float32), index=index)) })

        M = NeuralNet()

        with tf.Session() as S:
            M.initialize()

            auc = M.evaluate( feed.iterate(10), query='auc' )
            assert not np.isnan(auc)

            auc = M.evaluate( feed.iterate(10), query='auc' )
            assert not np.isnan(auc)

    def test_model_output_reduce(self):
        M = DenseTransformTest()
        shard = M.schema.map(atoms='atom.pq').load( resource_filename('tflon_test.data', 'sample_shard') )

        with tf.Session() as S:
            M.initialize()
            output = M.infer(shard)['out']

            assert isinstance(output, pd.DataFrame)
            self.assertEqual(output.shape, (1000, 10))
            data = shard['atoms'].data

            for idx, row in data.iterrows():
                for v in output.loc[idx]:
                    assert len(v) == len(row[data.columns[0]])

    def test_model_schema_filter_targets(self):
        M = SchemaTest()

        shard = M.schema.map(atoms='atom.pq', react='reactivity.pq').required.load( resource_filename('tflon_test.data', 'sample_shard') )
        self.assertEqual(set(shard.keys()), {'_index', 'atoms', 'molecules', 'react'})

    def test_model_schema_can_load_shards(self):
        M = SchemaTest()
        shard = M.schema.map(atoms='atom.pq', react='reactivity.pq').load( resource_filename('tflon_test.data', 'sample_shard') )

        self.assertEqual(set(shard.keys()), {'_index', 'atoms', 'molecules', 'react', 'targets'})
        self.assertEqual(len(shard['molecules'].index), 1000)
        self.assertEqual(len(shard['atoms'].index), 1000)
        self.assertEqual(shard['atoms'].shape[1], 22)
        self.assertEqual(shard['react'].shape, (1000, 4))
        self.assertEqual(shard['targets'].shape, (1000, 14))
        with tf.Session() as S:
            output = S.run( M.outputs['out'], feed_dict=M.feed( M._preprocess_batch(shard) ) )
            assert np.isnan(output)

    def test_model_schema_can_load_shards_from_tarfile(self):
        M = SchemaTest()
        shard = M.schema.map(atoms='atom.pq', react='reactivity.pq').load( resource_filename('tflon_test.data', 'sample_shard.tar.gz') )

        self.assertEqual(set(shard.keys()), {'_index', 'atoms', 'molecules', 'react', 'targets'})
        self.assertEqual(len(shard['molecules'].index), 1000)
        self.assertEqual(len(shard['atoms'].index), 1000)
        self.assertEqual(shard['atoms'].shape[1], 22)
        self.assertEqual(shard['react'].shape, (1000, 4))
        self.assertEqual(shard['targets'].shape, (1000, 14))
        with tf.Session() as S:
            output = S.run( M.outputs['out'], feed_dict=M.feed( M._preprocess_batch(shard) ) )
            assert np.isnan(output)

    def test_model_with_sparse_inputs(self):
        M = SparseSum()

        val_rng = np.arange(100)
        ind_rng = np.arange(10000)
        vals = np.random.choice(val_rng, size=(5000,))
        indices = np.array([sorted(np.random.choice(ind_rng, size=(5,), replace=False)) for _ in range(1000)]).reshape((-1,))
        indptr = np.arange(1001)*5

        sp_inp = sp.sparse.csr_matrix((vals, indices, indptr), shape=(1000,10000))

        with tf.Session() as S:
            output = M.infer(source={'sparse':sp_inp})
            result = np.sum(sp_inp, axis=1)

            np.testing.assert_array_almost_equal(output['sum'].reshape((1000,1)), result)

            batch = M._tower.feed({'sparse':tflon.data.convert_to_feedable(sp_inp)})

            queue = M.get_queue()
            queue.enqueue(batch)
            output = S.run(M.outputs['sum'], feed_dict=queue.fetch())
            np.testing.assert_array_almost_equal(output.reshape((1000,1)), result)

    def test_model_with_ragged_inputs(self):
        row_data = np.arange(13, dtype=np.int32).tolist()
        row2_data = np.arange(13,27, dtype=np.int32).tolist()
        nest_data = [3,6,4]
        nest2_data = [3,5,3,3]
        nested_data = pd.DataFrame(columns=['ID', 'data', 'lengths']).set_index('ID')
        nested_data.loc[0] = [row_data, nest_data]
        nested_data.loc[1] = [row2_data, nest2_data]
        table = tflon.data.RaggedTable(nested_data, dtype=np.int32)


        M = RaggedProduct()
        with tf.Session() as S:
            output = M.infer(source={'ragged':table})

            rshapes=[6,11,7,3]
            rvectors=[[0,1,2,13,14,15], [3,4,5,6,7,8,16,17,18,19,20], [9,10,11,12,21,22,23], [24,25,26]]

            result = np.array([np.prod(np.array(v, dtype=np.float32)+1) for v in rvectors])
            np.testing.assert_array_almost_equal(output['prod'], result)
            batch = M._tower.feed({'ragged':tflon.data.convert_to_feedable(table.as_matrix())})

            queue = M.get_queue()
            queue.enqueue(batch)
            output = S.run(M.outputs['prod'], feed_dict=queue.fetch())
            np.testing.assert_array_almost_equal(output, result)

    def test_model_inference_with_one_batch_specific_output(self):
        M = NeuralNet()

        with tf.Session() as S:
            M.initialize()

            g = RandomGenerator(shapes={'desc': (10, 10), 'targ':(10, 1)})
            batch = next(iter(g))
            output = M.infer( batch, query='pred' )

            _, W1, b1, W2, b2 = S.run( M.variables )
            l1 = np.tanh( np.dot( batch['desc'], W1 ) + b1 )
            s1 = 1. / (1. + np.exp( -(np.dot( l1, W2 ) + b2) ))

            np.testing.assert_array_almost_equal( output, s1 )

    def test_model_get_weight_values(self):
        M = NeuralNet()

        with tf.Session() as S:
            M.initialize()
            outvars = M.fetch('weights')
            self.assertEqual(len(outvars.keys()), 2)
            self.assertEqual(outvars['NeuralNet/Dense_0/weights_0:0'].shape, (10,5))
            self.assertEqual(outvars['NeuralNet/Dense_1/weights_0:0'].shape, (5,1))

            outvars = M.fetch(['weights'])
            self.assertEqual(len(outvars.keys()), 2)
            self.assertEqual(outvars['NeuralNet/Dense_0/weights_0:0'].shape, (10,5))
            self.assertEqual(outvars['NeuralNet/Dense_1/weights_0:0'].shape, (5,1))

    def test_model_inference_with_batch_iterator(self):
        M = NeuralNet()

        with tf.Session() as S:
            M.initialize()
            _, W1, b1, W2, b2 = S.run( M.variables )

            g = RandomGenerator(shapes={'desc': (10, 10), 'targ':(10, 1)}, limit=10)
            for output in M.infer( iter(g) ):
                batch = g.history[-1]
                l1 = np.tanh( np.dot( batch['desc'], W1 ) + b1 )
                s1 = 1. / (1. + np.exp( -(np.dot( l1, W2 ) + b2) ))

                np.testing.assert_array_almost_equal( output['pred'], s1 )

            self.assertEqual(10, len(g.history))

    def test_model_inference_with_tablefeed_iterator(self):
        M = NeuralNet()

        index = list(range(100))
        feed = tflon.data.TableFeed({'desc': tflon.data.Table(pd.DataFrame(data=np.random.random((100,10)), index=index))})
        with tf.Session() as S:
            M.initialize()
            _, W1, b1, W2, b2 = S.run( M.variables )

            for batch in feed.iterate(batch_size=20):
                output = M.infer( batch )
                l1 = np.tanh( np.dot( batch['desc'].as_matrix(), W1 ) + b1 )
                s1 = 1. / (1. + np.exp( -(np.dot( l1, W2 ) + b2) ))
                np.testing.assert_array_almost_equal( output['pred'], s1 )

    @with_tempfile
    def test_model_save_load(self, tmpfile):
        M = NeuralNet()

        with tf.Session():
            M.initialize()
            g = RandomGenerator(shapes={'desc': (10, 10), 'targ':(10, 1)})
            batch = next(iter(g))
            output = M.infer( batch )

            M.save(tmpfile)

        tf.reset_default_graph()

        M = tflon.model.Model.load(tmpfile)
        with tf.Session():
            M.initialize()
            output2 = M.infer( batch )

            np.testing.assert_array_almost_equal(output['pred'], output2['pred'])


    @with_tempfile
    def test_model_save_writes_model_submodule_and_default_parameters(self, tmpfile):
        M = NeuralNet(L2_coefficient=10.)

        with tf.Session():
            M.initialize()
            M.save(tmpfile)

        with open(tmpfile, 'rb') as pfile:
            class_, params, values = pickle.load(pfile)

            required = {'weight_prior', 'weight_initializer', 'bias_prior', 'bias_initializer', 'num_targets',
                         'Dense_0/activation', 'Dense_1/activation', 'L2_coefficient'}
            actual = set(params.keys())

            self.assertEqual(actual, required)

    @with_tempfile
    def test_model_save_load_override_params(self, tmpfile):
        M = NeuralNet()

        with tf.Session():
            M.initialize()
            g = RandomGenerator(shapes={'desc': (10, 10), 'targ':(10, 1)})
            batch = next(iter(g))
            output = M.infer( batch )

            M.save(tmpfile)

        self.assertEqual(M.get_parameter('L2_coefficient'), 0.1)

        tf.reset_default_graph()

        M = tflon.model.Model.load(tmpfile, params={'L2_coefficient':1.0})
        with tf.Session():
            M.initialize()
            output2 = M.infer( batch )

            np.testing.assert_array_almost_equal(output['pred'], output2['pred'])

        self.assertEqual(M.get_parameter('L2_coefficient'), 1.0)

        tf.reset_default_graph()

        M = tflon.model.Model.load(tmpfile, L2_coefficient=2.0)
        with tf.Session():
            M.initialize()
            output2 = M.infer( batch )

            np.testing.assert_array_almost_equal(output['pred'], output2['pred'])

        self.assertEqual(M.get_parameter('L2_coefficient'), 2.0)

    @with_tempfile
    def test_model_save_does_not_save_training_variables(self, tmpfile):
        index = list(range(100))
        F = tflon.data.TableFeed({ 'desc': tflon.data.Table(pd.DataFrame(data=np.random.random((100,10)), index=index)),\
                                   'targ': tflon.data.Table(pd.DataFrame(data=np.random.randint(0,2,size=(100,1)).astype(np.float32), index=index)) })
        M = NeuralNet()
        T = tflon.train.TFTrainer(tf.train.AdamOptimizer(1e-3), iterations=10)

        with tf.Session():
            M.fit(F, T)
            M.save(tmpfile)

        with open(tmpfile,'rb') as pfile:
            _, _, values = pickle.load(pfile)

        expected_values = {'NeuralNet/phase:0',
                           'NeuralNet/Dense_0/weights_0:0', 'NeuralNet/Dense_0/biases:0',
                           'NeuralNet/Dense_1/weights_0:0', 'NeuralNet/Dense_1/biases:0'}
        self.assertEqual(set(values.keys()), expected_values)

    def test_model_unused_parameters_should_warn(self):
        M1 = NeuralNet(width= 10, unused="Something")
        self.assertEqual(tflon.logging.get_test_logs()[0], ('W', "Parameter setting %s was not used by any module", ('unused',), {}))

    def test_model_should_parameterize(self):
        M1 = NeuralNet()
        shapes = [tuple(dim.value for dim in W.get_shape()) for W in M1.weights + M1.biases]
        self.assertEqual(shapes, [(10,5), (5,1), (5,), (1,)])

        tf.reset_default_graph()
        M2 = NeuralNet(width=10)
        shapes = [tuple(dim.value for dim in W.get_shape()) for W in M2.weights + M2.biases]
        self.assertEqual(shapes, [(10,10), (10,1), (10,), (1,)])

        tf.reset_default_graph()
        M2 = NeuralNet(params={'width':10})
        shapes = [tuple(dim.value for dim in W.get_shape()) for W in M2.weights + M2.biases]
        self.assertEqual(shapes, [(10,10), (10,1), (10,), (1,)])

        tf.reset_default_graph()
        try:
            NeuralNet(width=None)
        except KeyError as e:
            self.assertTrue( 'Key not set and no default: width' in str(e) )
            pass
        else:
            assert False, "Expected KeyError"

    def test_model_can_take_kwargs(self):
        M1 = NeuralNet()
        shapes = [tuple(dim.value for dim in W.get_shape()) for W in M1.weights + M1.biases]
        self.assertEqual(shapes, [(10,5), (5,1), (5,), (1,)])

        tf.reset_default_graph()
        M2 = NeuralNet(width=10)
        shapes = [tuple(dim.value for dim in W.get_shape()) for W in M2.weights + M2.biases]
        self.assertEqual(shapes, [(10,10), (10,1), (10,), (1,)])

        tf.reset_default_graph()
        try:
            NeuralNet(width=None)
        except KeyError as e:
            self.assertTrue( 'Key not set and no default: width' in str(e) )
            pass
        else:
            assert False, "Expected KeyError"

    def test_model_should_report_len(self):
        M1 = NeuralNet()
        self.assertEqual( len(M1), 50+5+5+1 )

    def test_model_dir_should_include_tower_methods(self):
        M1 = NeuralNet()
        M1dir = dir(M1)
        assert all(item in M1dir for item in dir(M1._tower))
