from builtins import range
from tflon_test.utils import TflonTestCase, TflonOpTestCase, ragged, hot, convert_feed
import tensorflow as tf
import pandas as pd
import numpy as np
import dill as pickle
import networkx as nx
import tflon
import unittest

def gen_images(batch_size, min_size, max_size, channels):
    dims = np.random.randint(min_size, max_size+1, size=(batch_size, 2))

    data = np.full((batch_size, channels+2), None, np.object_)

    for i in range(batch_size):
        w,h = dims[i,:]
        for j in range(channels):
            data[i, j] = np.random.random((w,h))
        data[i, -2] = w
        data[i, -1] = h

    columns = ['channel_%d' % (i) for i in range(channels)] + ['width', 'height']
    return pd.DataFrame(data=data, columns=columns)

class ImageDataTests(TflonTestCase):
    def test_image_table_pad_to_shape(self):
        image_data = gen_images(100, 5, 10, 3)
        w,h = image_data[image_data.columns[-2:]].max()

        imtable = tflon.image.ImageTable(image_data, adjustment='pad')
        immat = imtable.as_matrix()

        self.assertEqual(immat.shape, (100, w, h, 3))

    def test_image_table_clip_to_shape(self):
        image_data = gen_images(100, 5, 10, 3)
        w,h = image_data[image_data.columns[-2:]].min()

        imtable = tflon.image.ImageTable(image_data, adjustment='clip')
        immat = imtable.as_matrix()

        self.assertEqual(immat.shape, (100, w, h, 3))

    def test_image_table_adjust_to_shape(self):
        image_data = gen_images(100, 5, 10, 3)
        w,h = 7,7

        imtable = tflon.image.ImageTable(image_data, size=(w,h))
        immat = imtable.as_matrix()

        self.assertEqual(immat.shape, (100, w, h, 3))

    def test_image_table_fixed_size(self):
        image_data = gen_images(100, 19, 19, 5)

        imtable = tflon.image.ImageTable(image_data)
        immat = imtable.as_matrix()

        self.assertEqual(immat.shape, (100, 19, 19, 5))

"""Note: if i need to change any of these, run graph_object_test.py out of scripts folder in image_wave.
That will give the outputs I can then copy and paste into here. If I make it 0 indexed, that's what I'll need to do."""
class ImageGraph2DTests(TflonTestCase):
    # def setUp(self):
    #     tflon.data.TensorQueue.DEFAULT_TIMEOUT=1000
    #     tflon.logging.clear_test_logs()
    #     tflon.logging.set_test_mode()
    #     tflon.system.reset()

    def test_basic_attributes(self):
        size = 4
        graph = tflon.image.ImageGraph2D((size,size))
        self.assertEqual(graph._components, list(range(1,size**2+1)))
        self.assertEqual(graph.nodes, list(range(1,size**2+1)))
        self.assertEqual(graph.nodes.index,{1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 7: 6, 8: 7, 9: 8, 10: 9, 11: 10, 12: 11, 13: 12, 14: 13, 15: 14, 16: 15})
        self.assertEqual(graph.edges, [(1, 2), (1, 5), (2, 3), (2, 6), (3, 4), (3, 7), (4, 8), (5, 6), (5, 9), (6, 7), (6, 10), (7, 8), (7, 11), (8, 12), (9, 10), (9, 13), (10, 11), (10, 14), (11, 12), (11, 15), (12, 16), (13, 14), (14, 15), (15, 16)])
        self.assertEqual(graph.edges.index, {(5, 9): 8, (10, 11): 16, (8, 7): 11, (4, 8): 6, (5, 6): 7, (10, 6): 10, (14, 13): 21, (6, 2): 3, (12, 16): 20, (7, 11): 12, (14, 10): 17, (1, 2): 0, (11, 10): 16, (9, 5): 8, (6, 7): 9, (15, 11): 19, (7, 6): 9, (6, 10): 10, (1, 5): 1, (16, 15): 23, (14, 15): 22, (7, 3): 5, (10, 9): 14, (3, 2): 2, (2, 6): 3, (11, 7): 12, (15, 16): 23, (9, 10): 14, (2, 3): 2, (2, 1): 0, (12, 11): 18, (13, 9): 15, (15, 14): 22, (11, 12): 18, (6, 5): 7, (16, 12): 20, (9, 13): 15, (3, 7): 5, (8, 12): 13, (13, 14): 21, (3, 4): 4, (10, 14): 17, (4, 3): 4, (5, 1): 1, (11, 15): 19, (12, 8): 13, (7, 8): 11, (8, 4): 6})

    def test_basic_methods(self):
        size = 4
        I = np.array(list(range(1,size**2+1))).reshape(size,size)
        graph = tflon.image.ImageGraph2D((size,size))
        self.assertEqual(graph.neighborhood(5,2), {1: 1, 2: 2, 5: 0, 6: 1, 7: 2, 9: 1, 10: 2, 13: 2})
        self.assertEqual(next(graph.get_edges(1,5)),(1,5))

        c_coords = graph.centroids()
        self.assertEqual(c_coords,(2,2))
        blocks, ancestors, types = graph.bfs(c_coords)
        self.assertEqual(blocks, [[11], [7, 10, 12, 15], [3, 6, 8, 9, 14, 16], [2, 4, 5, 13], [1]])
        self.assertEqual(ancestors, {1: [2, 5], 2: [3, 6], 3: [7], 4: [3, 8], 5: [6, 9], 6: [7, 10], 7: [11], 8: [7, 12], 9: [10], 10: [11], 11: [], 12: [11], 13: [9, 14], 14: [10, 15], 15: [11], 16: [12, 15]})
        self.assertEqual(types, {})

class ImageGraph2DGRNNTests(TflonOpTestCase):
    def test_eval_image_graph_with_grnn(self):
        batch = {'image_graphs': tflon.data.Table(pd.DataFrame(data={'Structures':[tflon.image.ImageGraph2D((19,19)), tflon.image.ImageGraph2D((10,5))]}, index=[0,1]))}

        forward_cell = tflon.graph.MiniGRUCell(10)
        backward_cell = tflon.graph.MiniGRUCell(10)
        pixels = tf.placeholder(name='pixels', dtype=tf.float32, shape=[None, 2])
        G = tflon.graph.GRNN('image_graphs', rooter=tflon.image.ImageGraph2D.centroids, dagger=tflon.image.ImageGraph2D.bfs)

        resize = tflon.toolkit.Dense(10)(pixels)
        result = G(resize, forward_cell, backward_cell)

        with tf.Session() as S:
            S.run(tf.variables_initializer(self._test_tower.variables))

            fd = self._test_tower.feed(convert_feed(G.featurizer(batch)))
            pixels_val = np.random.random((19*19+10*5, 2))
            fd[pixels] = pixels_val

            output = S.run(result, feed_dict=fd)
            self.assertEqual((19*19+10*5, 10), output.shape)
