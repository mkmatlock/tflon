from builtins import range
import tensorflow as tf
import tflon
import pandas as pd
import numpy as np
from tflon_test.utils import TflonTestCase, NeuralNet, with_tempdir
from pkg_resources import resource_filename

def make_table_feed():
    fn = resource_filename('tflon_test.data', 'cyp.tsv')
    data = pd.read_csv(fn, sep='\t', dtype={'ID':str}).set_index('ID')
    return tflon.data.TableFeed({ 'desc': tflon.data.Table(data.iloc[:,:-1]), 'targ': tflon.data.Table(data.iloc[:,-1:]) })

def generate_random_tensors(shape):
    idx = 0
    while 1:
        yield tflon.data.TableFeed({'input':
                                    tflon.data.Table( pd.DataFrame(data=np.random.random(shape), index=list(range(idx,idx+shape[0]))) )})
        idx += shape[0]

class TrainerTests(TflonTestCase):
    def test_trainer_updates_view(self):
        data = make_table_feed()
        trainer = tflon.train.TFTrainer(tf.train.AdamOptimizer(1e-3), iterations=10)
        model = NeuralNet()
        v = trainer.add_view('intermediate')

        visited = []
        def check_update(index, value):
            assert np.isnan(value).sum()==0
            assert value.shape==(20,10)
            assert index.shape==(20,)
            visited.append(index.tolist())

        v.listen(check_update)

        with tf.Session() as S:
            model.fit(data.shuffle(20), trainer)
        self.assertEqual(len(set(sum(visited, []))), 200)

    @with_tempdir
    def test_restarts_preserves_iteration_counter(self, tdir):
        data = make_table_feed()
        summary_hook = tflon.train.SummaryHook(tdir, frequency=5)
        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(0.01), iterations=15, hooks=[summary_hook] )
        model = NeuralNet()

        with tf.Session() as S:
            model.fit(data.shuffle(20), trainer, restarts=3)

        logfile = tflon.summary.get_latest_file(tdir)
        steps, _ = tflon.summary.search_summary(logfile, "losses/" )

        self.assertEqual(steps, [5,10,15,20,25,30,35,40,45])



class SamplerTests(TflonTestCase):
    def test_class_resampling_selects_rare_examples_often(self):
        BS = 5
        N = 1000
        C = 4
        index = np.arange(N)
        prates = [0.5, 0.25, 0.1, 0.01]
        tsamples = [1, 2, 5, 10]
        targets = np.zeros((N,C))
        for i in range(C):
            targets[np.random.choice(index, size=(int(N*prates[i]),), replace=False),i] = 1

        nanrowrates = 0.005
        nanrates = 0.001
        zeros = np.argwhere(targets==0)
        nanpos = np.random.choice(np.arange(zeros.shape[0], dtype=np.int32), size=int(nanrates*zeros.shape[0]), replace=False)
        for i in nanpos:
            targets[zeros[i]] = np.nan
        zerorows = np.argwhere(np.nansum(targets,axis=1)==0)
        nanrows = np.random.choice(np.arange(zerorows.shape[0], dtype=np.int32), size=int(nanrowrates*zerorows.shape[0]), replace=False)
        for i in nanrows:
            targets[i,:] = np.nan


        feed = tflon.data.TableFeed({'targets': tflon.data.Table(pd.DataFrame(targets, index=index))})
        sampler = tflon.train.ClassResampling(feed, 'targets')

        epochs=4
        counts = np.maximum(np.nanmax(targets*np.array(tsamples), axis=1), 1)
        counts[np.isnan(counts)] = 0
        np.testing.assert_array_equal(counts, sampler._counts)
        expected = counts * epochs
        observed = np.zeros(expected.shape)
        limit = (np.sum(counts)/BS).astype(np.int32)+1

        it = sampler.shuffle(batch_size=BS)
        for i in range(epochs*limit):
            B = next(it)
            observed[np.isin(index, B.index)]+=1

        self.assertEqual(sampler.epoch, epochs)
        self.assertTrue(np.sum(np.abs(expected-observed))<50)
        self.assertTrue(np.sum(observed==expected)>950)

    def test_boost_selects_high_loss_examples(self):
        data = make_table_feed()
        v = tflon.train.View('raw_losses')
        model = NeuralNet()
        op = v.setup(model)
        boost = tflon.train.BoostGradient(data, v)

        visited = set()
        with tf.Session() as S:
            model.initialize()
            G = boost.shuffle(batch_size=100)
            for i in range(100):
                B = next(G)
                I = B.index.astype(np.string_)
                V = S.run(op, feed_dict=model.feed(B))
                v.update(I, V)

                visited |= set(I)

        self.assertEqual(data.index.shape[0], len(visited))
        assert np.all(boost._weights<tflon.train.BoostGradient.INITIAL_VALUE)

    def test_merge_feeds_exhausts_groups_before_reshuffle(self):
        index = np.arange(225)

        pos = np.random.choice(index, size=25, replace=False)
        labels = np.zeros((225,1))
        labels[pos] = 1
        t1 = tflon.data.Table( pd.DataFrame(labels, index=index) )

        desc = np.random.random((225,10))
        t2 = tflon.data.Table( pd.DataFrame(desc, index=index) )

        feed = tflon.data.TableFeed({'targ': t1, 'desc': t2})

        i0 = index[labels[:,0]==0]
        g0 = feed.batch(i0)
        i1 = index[labels[:,0]==1]
        g1 = feed.batch(i1)

        iterator = tflon.train.merge_feeds(g0.shuffle(20), g1.shuffle(5))

        seen0 = set()
        seen1 = set()
        for _ in range(5):
            batch = next(iterator)
            bidx = set(batch.index)
            self.assertEqual(len(bidx), 25)

            b0 = bidx & set(i0)
            b1 = bidx & set(i1)
            self.assertEqual(len(b0), 20)
            self.assertEqual(len(b1), 5)
            self.assertEqual(seen0 & b0, set())
            self.assertEqual(seen1 & b1, set())
            seen0 |= b0
            seen1 |= b1

        for _ in range(5):
            batch = next(iterator)
            bidx = set(batch.index)
            self.assertEqual(len(bidx), 25)

            b0 = bidx & set(i0)
            b1 = bidx & set(i1)
            self.assertEqual(len(b0), 20)
            self.assertEqual(len(b1), 5)
            self.assertEqual(seen0 & b0, set())
            self.assertEqual(seen1 & b1, b1)
            seen0 |= b0
            seen1 |= b1

    def test_fixed_interval_curriculum_updates_levels(self):
        level1 = generate_random_tensors(shape=(100,10))
        level2 = generate_random_tensors(shape=(100,20))
        fic = tflon.train.FixedIntervalCurriculum([level1, level2], frequency=50)

        with tf.Session() as S:
            S.run(tf.variables_initializer(tf.global_variables()))
            step = tf.placeholder(shape=[], dtype=tf.int32)
            condition_op = fic(step, 1e-1)
            iterator = fic.iterate()

            collection = []
            for i in range(1, 51):
                collection.append(next(iterator)['input'])
                S.run(condition_op, feed_dict={step: i})

            is_level1 = [c.shape==(100,10) for c in collection]
            self.assertTrue( all(is_level1) )

            collection = []
            for i in range(51, 101):
                collection.append(next(iterator)['input'])
                S.run(condition_op, feed_dict={step: i})

            is_level2 = [c.shape==(100,20) for c in collection]
            self.assertTrue( 5 < np.sum(is_level2) < 45 )

    def test_fixed_interval_curriculum_multiple_samples_per_batch(self):
        level1 = generate_random_tensors(shape=(50,10))
        level2 = generate_random_tensors(shape=(100,10))
        fic = tflon.train.FixedIntervalCurriculum([level1, level2], frequency=25)

        with tf.Session() as S:
            S.run(tf.variables_initializer(tf.global_variables()))
            step = tf.placeholder(shape=[], dtype=tf.int32)
            condition_op = fic(step, 1e-1)
            iterator = fic.iterate(samples_per_batch=3)

            collection = []
            shape_hist = {}
            for i in range(100):
                inp = next(iterator)['input']
                shape_hist[inp.shape] = shape_hist.get(inp.shape, 0) + 1
                collection.append(inp)
                S.run(condition_op, feed_dict={step: i})

            self.assertEqual([(150, 10), (200, 10), (250, 10), (300, 10)], sorted(shape_hist.keys()))

