import os
import unittest
import subprocess
from tflon.distributed.distributed import has_distributed_capability
from tflon_test.utils import rm_if

class TestExamples(unittest.TestCase):
    @classmethod
    def tearDownClass(self):
        rm_if('wdbc.data')
        rm_if('saved_lr.p')
        rm_if('model.p')
        rm_if('tflon/examples/tox21/tox21.csv.gz')
        rm_if('tflon/examples/tox21/tox21_molecules.pq')
        rm_if('tflon/examples/tox21/tox21_targets.pq')

    def test_basic_neural_network(self):
        subprocess.check_call(['python', '-m', 'tflon.examples.logistic_regression'])

    @unittest.skipIf(not has_distributed_capability(), "Please install mpi4py, horovod and MPI")
    def test_distributed_neural_network(self):
        subprocess.check_call(['mpirun', '-np', '2', 'python', '-m', 'tflon.examples.distributed'])

    def test_neighborhood_network(self):
        subprocess.check_call(['python', '-m', 'tflon.examples.tox21.neighborhood'])

    def test_wave_network(self):
        subprocess.check_call(['python', '-m', 'tflon.examples.tox21.wave'])

    def test_weave(self):
        subprocess.check_call(['python', '-m', 'tflon.examples.tox21.weave'])

    def test_maze(self):
        subprocess.check_call(['python', '-m', 'tflon.examples.maze.train', '-i', '100', '-b', '25', 'model.p'])
        self.assertTrue(os.path.exists('model.p'))
