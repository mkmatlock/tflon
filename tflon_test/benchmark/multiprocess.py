from builtins import range
import tflon
import tensorflow as tf
from pkg_resources import resource_filename
import contextlib
import shutil
import tempfile
import time
import os
import numpy as np
import pandas as pd
import click

@contextlib.contextmanager
def tempdir():
    dirpath = tempfile.mkdtemp()
    def cleanup():
        try:
            shutil.rmtree(dirpath)
        except:
            pass
    try:
        yield dirpath
    finally:
        cleanup()

class Wave(tflon.model.Model):
    def _schema(self):
        # Create a shard schema with three parquet files
        return { 'targets': ('targets.pq', tflon.data.Table),
                 'molecules': ('molecules_3d.pq', tflon.chem.MoleculeTable),
                 'atoms': ('atom.pq', tflon.data.DenseNestedTable) }

    def _model(self):
        atoms = self.add_input('atoms', shape=[None, self.atom_features])
        if self.bond_features>0: bonds = self.add_input('bonds', shape=[None, self.bond_features])
        else: bonds = None
        targets = self.add_target('targets', shape=[None, self.targets])

        graph_conversion = tflon.chem.GraphConverter('molecules', gtype='delaunay', preserve_bonds=True)
        edge_lengths = tflon.chem.BondLength()()
        edge_properties = graph_conversion(bonds)
        edge_properties = tf.concat([edge_properties, edge_lengths], axis=1)

        # Apply one pass of wave with state_size of 10
        resize = tflon.toolkit.Dense(10, activation=tf.nn.elu)(atoms)

        graph_recursion = tflon.graph.GRNN(edge_properties=self.bond_features>0, dagger=tflon.graph.ViGraph.bfs, rooter=tflon.graph.ViGraph.centroids, num_ports=4)
        forward_cell = graph_recursion.wrap_cell( tf.nn.rnn_cell.GRUCell(10) )
        backward_cell = graph_recursion.wrap_cell( tf.nn.rnn_cell.GRUCell(10) )
        output = graph_recursion(resize, forward_cell, backward_cell, edge_properties=edge_properties)

        # Reduce wave output to molecule features and classify
        net = tflon.graph.Reduce() | tflon.toolkit.Dense(self.targets)
        logits = net(output)

        # self.add_loss('logloss', log_regression_loss(targets, logits, weights=chem_acc, nans=True))
        self.add_output('outputs', logits)
        self.add_loss('xent', tflon.toolkit.xent_uniform_sum(targets, logits, nans=True))
        self.add_loss('l2', tflon.toolkit.l2_penalty(self.weights))
        self.add_metric('auc', tflon.toolkit.auc(targets, logits, axis=0, nans=True))

    def _parameters(self):
        return {'graph_table':'graphs',
                'atom_features': None,
                'bond_features': 0,
                'targets': None}

def parse_log(logfile, query, runtime, writepath):
    iters, data = tflon.summary.search_summary(logfile, *query)

    df_idx = sorted(data.keys())
    df_data = []
    for k in df_idx:
        values = [data[k][j] for j in iters]
        df_data.append([np.percentile(values, 25*j) for j in range(0,5)])
    df_idx = [k.split('/')[-1].strip('_') for k in df_idx]
    df_data.append([runtime, np.nan, np.nan, np.nan, np.nan])
    pd.DataFrame(data=df_data, columns=['Min', '1st-quartile', 'Median', '3rd-quartile', 'Max'], index=df_idx+['Time (seconds)']).round(1).to_csv(writepath, sep='\t')

@click.command()
@click.option("-p", "--processes", default=4)
@click.option("-q", "--query", multiple=True)
@click.option("-i", "--iterations", default=2500)
@click.argument("outfile")
def main(processes, query, iterations, outfile):
    if len(query) == 0:
        query = ['GPU', 'CPU', 'Memory used', 'Queue utilization']

    with tempdir() as dirpath:
        tflon.data.TensorQueue.DEFAULT_TIMEOUT=5000

        model = Wave(atom_features=22, targets=14)
        data = model.schema.load(resource_filename('tflon_test.data', 'sample_shard'))
        feed = tflon.data.TableFeed(data)

        trainer = tflon.train.TFTrainer( tf.train.AdamOptimizer(1e-3), iterations=iterations )
        hook = tflon.train.SummaryHook( dirpath, frequency=10 )
        trainer.add_hook( hook )

        with tf.Session():
            start_time = time.time()
            model.fit( feed.shuffle(batch_size=100), trainer, restarts=1, processes=processes )

        runtime = time.time() - start_time
        filename, = os.listdir(dirpath)
        parse_log(os.path.join(dirpath, filename), query, runtime, outfile)

if __name__=='__main__':
    main()
